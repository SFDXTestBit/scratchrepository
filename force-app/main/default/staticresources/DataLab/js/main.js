$(function(){
	
	//Trigger hover from Submenu
	$('dl.circle dd a').hover(function(e) {
		$(this).parent('dd').prev('dt').find('a').addClass('hover');
	}, function(){
		$(this).parent('dd').prev('dt').find('a').removeClass('hover');
	});
	
	//Animate Background from Submenu
	var fadeTime = 250;
	
	$('dl.circle.orange dd a').add('dl.circle.orange dt a').hover(function() {
		$('#teaser').stop().animate({backgroundColor: '#ed9c33' },fadeTime);
		$('.transform-white').find('*').stop().animate({color: '#fff' },fadeTime);
	},function(){
		$('#teaser').stop().animate({backgroundColor: '#DCDDDE' },fadeTime);
		$('.transform-white').find('*').stop().animate({color: '#788386' },fadeTime);
	});
	
	$('dl.circle.blue dd a').add('dl.circle.blue dt a').hover(function() {
		$('#teaser').stop().animate({backgroundColor: '#5896be' },fadeTime);
		$('.transform-white').find('*').stop().animate({color: '#fff' },fadeTime);
	},function(){
		$('#teaser').stop().animate({backgroundColor: '#DCDDDE' },fadeTime);
		$('.transform-white').find('*').stop().animate({color: '#788386' },fadeTime);
	});
	
	$('dl.circle.green dd a').add('dl.circle.green dt a').hover(function() {
		$('#teaser').stop().animate({backgroundColor: '#89b283' },fadeTime);
		$('.transform-white').find('*').stop().animate({color: '#fff' },fadeTime);
	},function(){
		$('#teaser').stop().animate({backgroundColor: '#DCDDDE' },fadeTime);
		$('.transform-white').find('*').stop().animate({color: '#788386' },fadeTime);
	});
	
	
	  /* Calculate Height Menu, because of scrolling */
	  
	  var viewportHeight = $(window).height();
	  $('.scrollResponsive').css({'height': (viewportHeight-105)+'px'});
	  
	  $(window).resize(function() {
	  	var viewportHeight = $(window).height();
	  	$('.scrollResponsive').css({'height': (viewportHeight-105)+'px'});
	  });
	  
	  /* Scroll to top */
	 	 
	$(".scrollTop a").click(function() {
	  $("html, body").animate({ scrollTop: 0 });
	  return false;
	});
	 
});


