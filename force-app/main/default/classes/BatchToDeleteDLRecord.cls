/*------------------------------------------------------------------
/* Created By : Dev Team Phiz
 * Created date : 12/07/2017 07:11
 * Last Modified by:: Dev Team Phiz ,  26/09/2017 11:22
 * Description: Batch class to delete records
------------------------------------------------------------------*/
global class BatchToDeleteDLRecord Implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful {
    //Initialization
    global List<String> orderList; 
    global String objName;
   
    
    public BatchToDeleteDLRecord (List<String> orderList){        
        this.orderList = orderList;
        this.objName = orderList.get(0);                  
    }
    
    //return all objects
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id'+' FROM '+objName;
        return Database.getQueryLocator(query);
    }
    
    //method to delete the records in List 
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        try{
            List<sObject> objList = new List<sObject>();
            for(sObject s : scope){                   
                objList.add(s);
            } 
            if(!objList.isEmpty()){
                
                Database.delete(objList,false);//Database method to delete the records in List               
            }
        } catch(Exception e){
            
            
        }       
    } 
    
    
    global void finish(Database.BatchableContext BC){
            orderList.remove(0);
            if(!orderList.isEmpty() && test.isrunningtest()==false){
                Database.executeBatch(new BatchToDeleteDLRecord (orderList));
            }
        
    }
    
}