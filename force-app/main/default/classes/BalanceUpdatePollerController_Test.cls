@isTest
public class BalanceUpdatePollerController_Test {
    //Test method to test Success scenario
    public static testmethod void TestBalanceUpdatePollerController(){
        Test.startTest();
        try{
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            insert oBedspace;
          
            Account acc = new Account(Name='test', SP_contact_telephone_number__c = '01403 567890'); 
            insert acc;
            
            Contact con = new Contact(FirstName='Test',LastName='Contact',AccountId=acc.id);
            insert con;
            Id devRecordTypeId = Schema.SObjectType.In_Form__Tenancy__c.getRecordTypeInfosByName().get('Tenancy').getRecordTypeId();
            
            
            Tenancy__c tenancy = new Tenancy__c();
            tenancy.recordTypeid = devRecordTypeId;
            tenancy.Client__c = con.Id;
            tenancy.Start_date__c=Date.Today();
            tenancy.Status__c=null;
            tenancy.Bedspace__c=oBedspace.Id;
            insert tenancy;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(tenancy);
            BalanceUpdatePollerController ctrl = new BalanceUpdatePollerController(sc);
            ctrl.refreshStatus();
            
            System.assertEquals(tenancy.Balance_are_up_to_Date__c, false);
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    }
}