//Modified for Security Review
@isTest
public class UpdateNightTest{
     
    
    //Test method to Update Nights
    static testmethod void TestUpdateNight(){
        Test.startTest();
        try{
            Transaction_Type__c dailytype = new Transaction_Type__c(Daily_Amount__c = true,Is_Active__c=true,Type__c = 'Daily',Name = 'DailyType');
            INSERT dailyType;
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            Map<String,Id> rtMap = new Map<String,Id>();
            if(rtList!= null && rtList.size()>0){
                for(RecordType rt: rtList)
                rtMap.put(rt.Name,rt.Id);
            }
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            INSERT oBedspace;
            
            Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
            INSERT con;
            
            Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Bedspace__c =oBedspace.Id,Total_Rent_Charge__c=100,Start_date__c=Date.Today().addDays(-10),End_date__c=Date.Today().addDays(10),In_Form__Is_default__c=true);
            INSERT oRentSchedule;
            
            Rent_Stream_Charge__c oRentCharge = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=50);
            INSERT oRentCharge;
            
            Rent_Stream_Charge__c oRentCharge1 = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=100);
            INSERT oRentCharge1;
            
            Tenancy__c ten1 = new Tenancy__c();
            ten1.recordTypeid = rtMap.get('Void');
            ten1.Client__c = con.Id;
            ten1.Start_date__c=Date.Today().addDays(-5);
            ten1.Status__c='Current';
            ten1.Bedspace__c=oBedspace.Id;        
            INSERT ten1;
            Night__c nights = new Night__c();
            nights.Date__c = Date.Today().addDays(-2);
            nights.Tenancy__c=ten1.Id;
            INSERT nights; 
            ApexPages.standardController stdCont = new ApexPages.standardController(ten1);
            Night__c informNight =new Night__c();
            informNight.Void_reason__c = 'Waiting for referral';
            informNight.Void_status__c= 'Available to let';
            UpdateNights updateNight = new UpdateNights(stdCont);   
            updateNight.save();
            
            System.assertNotEquals(nights.id, null);
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    }
     
}