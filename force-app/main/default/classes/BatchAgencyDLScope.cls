/*------------------------------------------------------------------
 * Created By : Saurabh Gupta 
 * Created date : 23/03/2017 13:13
 * Last Modified by:: Saurabh Gupta ,  26/09/2017 04:59
 * Description: Batch class to execute BatchToCreateDLRecordHelper
 history : v1.0 - 31/10/2018 - Changes For Security Review
------------------------------------------------------------------*/
global with sharing class BatchAgencyDLScope implements Database.Batchable<sObject>{   
    //Initialization
    global Id batchDLId;
    
    //Constructor
    global BatchAgencyDLScope(Id batchId){
        this.batchDLId = batchId;
    } 
    
    //Return Agency records 
    global Database.QueryLocator start(Database.BatchableContext BC){
        //List<String> fieldList = new List<String>{'Id','In_Form__DL_Scope__c'};
        
        //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Agency__c',fieldList)){
          return Database.getQueryLocator('SELECT Id,In_Form__DL_Scope__c FROM In_Form__Agency__c');
        //}
        //else
        //{
        //    return null;
       // }
    }

    //Execute method to update timeLine Event data.
    global void execute(Database.BatchableContext BC, List<sObject> scope){   
    
        set<Id> agencyIds = new set<Id>();  
        Map<Id,In_Form__Agency__c> agencyMap = new Map<Id,In_Form__Agency__c>();
        
        
            for(In_Form__Agency__c agency : (List<In_Form__Agency__c>)Scope){       
                agency.In_Form__DL_Scope__c = False;
                agencyIds.add(agency.Id);
                agencyMap.put(agency.Id,agency);    
            }
            
            //List<String> fieldList = new List<String>{'Id','In_Form__Agency_referred_via__c','In_Form__Client__c','In_Form__DL_Scope__c'};
            
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Timeline_Event__c',fieldList)){
                //Iterate timeLine Event whose dl Scope is true and agancy referred via
                for(In_Form__Timeline_Event__c obj : [SELECT Id,In_Form__Agency_referred_via__c,In_Form__Client__c,In_Form__DL_Scope__c 
                                                        FROM In_Form__Timeline_Event__c 
                                                        WHERE In_Form__DL_Scope__c = true AND In_Form__Agency_referred_via__c IN:agencyIds]){
                
                    In_Form__Agency__c agency = agencyMap.get(obj.In_Form__Agency_referred_via__c);
                    agency.In_Form__DL_Scope__c = true;
                    agencyMap.put(agency.Id,agency);
                }
            //}
            
            //fieldList.clear();
            
            //fieldList = new List<String>{'In_Form__DL_Scope__c'};
            
           //&& InForm_CRUDFLS_Util.getUpdateAccessCheck('In_Form__Agency__c',fieldList)
            if(!agencyMap.isEmpty() ){
                
                List<Database.SaveResult> resultsList = Database.update(agencyMap.values(), false);
                for (Database.SaveResult result : resultsList ) {
                  if (!result.isSuccess()){ for (Database.Error err : result.getErrors()){
                       System.debug('Error :'+err.getMessage());
                    }
                  }
                }
           }        
    }   
   
    //finish method to Execute BatchToCreateDLRecordHelper class
    global void finish(Database.BatchableContext BC){
       BatchToCreateDLRecordHelper.callBatch(batchDLId);        
    }   
}