/*-----------------------------------------------------------------_
/* Created By : 
* Created date : 18/8/2015
* Description: Batch class to create transaction on creation of tenancy
* History: v1.1 - Sneha U. - 26/10/2018 - Enforcing CRUD/FLS
* History: v1.2 - Dinesh D. - 31/10/2018 - Enforcing FLS, Additional Null check & Try-Catch block
           v1.3 - Ashish S. - 24/4/2019 - To debug the transaction duplication issue.
------------------------------------------------------------------*/
global with sharing class TransactionCreationBatch implements Database.Batchable<sObject>, Database.Stateful{

    List<Id> tenancyListBatch = new List<Id>();
    Map<String,List<Rent_Stream_Charge__c>> rsChargeMap;
    Set<String> typeList;
    Map<Id,Tenancy__c> tenancies;
    boolean weeklyAmount;
    global Map<String,Double> amtTypeMap;
    global TransactionCreationBatch(List<Id> listTenancy){
        tenancyListBatch = listTenancy;
        rsChargeMap = new Map<String,List<Rent_Stream_Charge__c>>();
        amtTypeMap = new Map<String,Double>();
        typeList = new Set<String>();
        weeklyAmount= false;
    }

    global List<Transaction__c> start(Database.BatchableContext BC){
       // try{
            //List of fields assigned for Tenancy__c Object read access
            /*List<String> fieldList = new List<String>{
               'Id',
               'In_Form__Bedspace__r.In_Form__No_financial_data__c',
               'Name',    
               'In_Form__Start_date__c',
               'In_Form__End_date__c',                     
               'In_Form__Status__c',
               'In_Form__Bedspace__c'    
            };*/ 
            
            //Check read access for Tenancy__c Object
            //InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',fieldList) &&
            if(!tenancyListBatch.isEmpty()){    
                tenancies = new Map<Id,In_Form__Tenancy__c>([SELECT Id, In_Form__Bedspace__r.In_Form__No_financial_data__c,Name,In_Form__Start_date__c,In_Form__End_date__c,In_Form__Status__c,In_Form__Bedspace__c FROM In_Form__Tenancy__c 
                                                WHERE Id IN: tenancyListBatch]);
                                                
                List<Id> bedspaceIds = new List<Id>();
                map<id,boolean> mapBedspace=new map<id,boolean>(); // Added by Ajay on 06/05/2018   
                
                List<In_Form__Transaction__c> transactions = new List<In_Form__Transaction__c>();
                for(In_Form__Tenancy__c tenancy : tenancies.values()){
                    if(tenancy.In_Form__Bedspace__c != null){
                        bedspaceIds.add(tenancy.In_Form__Bedspace__c);
                        mapBedspace.put(tenancy.id,tenancy.In_Form__Bedspace__r.In_Form__No_financial_data__c); // Added by Ajay on 06/05/2018
                    }
                }
                
                //Call Helper class to get rent Charges 
                rsChargeMap=transactionUtility.getRentCharges(bedspaceIds,typeList,false);
                
                //Call Helper class to get list of Transactions
                transactions=transactionUtility.createTransaction(tenancies.values(),mapBedspace,weeklyAmount);
                
                
                return transactions;
            }
            //else{
             //   return null;
            //}
        /*} catch(Exception e){
            System.debug('Exception:'+e.getMessage());
            return null;
        }*/
        return null;  
    }

    global void execute(Database.BatchableContext BC, List<Transaction__c> scope){
        List<In_Form__Transaction__c> transactions = new List<In_Form__Transaction__c>();
        //Call helper class method to update the Transactions Amount
         
            transactions=transactionUtility.updateTransaction(scope,typeList,rsChargeMap,tenancies,weeklyAmount);
             
            if(!transactions.isEmpty()){
                systemBatchHelper.isBatchRunning=true;
                insert transactions;
            }
        
    }
    
    global void finish(Database.BatchableContext BC){
        try{
            Date startDate = Date.today();
            //List of fields assigned for Transaction__c Object read access
            /*List<String> fieldList = new List<String>{
             'In_Form__Transaction_Date__c'
            };*/
            
            //Check read access for Transaction__c Object
            //InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction__c',fieldList) &&
            if( !tenancyListBatch.isEmpty()){    
                for(Transaction__c trans : [SELECT Transaction_Date__c FROM Transaction__c WHERE Tenancy__c IN: tenancyListBatch ORDER BY Transaction_Date__c LIMIT 1]){
                    startDate = trans.Transaction_date__c.date();
                }
                Set<Id> idSet = new Set<Id>(tenancyListBatch);
                Database.executeBatch(new TransactionTotalUpdationBatch(idSet, startDate),5);
            }
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        } 
    }
}