/*------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 08/06/2017 11:58
 * Last Modified by:: Saurabh Gupta,  25/09/2017 05:37
 * Description: Batch class to create connection with other org.
 * History: v1.1 - Sneha U. - 26/10/2018 - Enforcing CRUD/FLS
 * History: v1.2 - Sneha U. - 31/10/2018 - Added create access check for error logs,Removed commented code
------------------------------------------------------------------*/
global class BatchToCreateConnectionRecordsNew Implements Database.Batchable<Id> {
    //Initialization
    global Set<Id> sharingIds;
    global Id networkId;
    global String fields;
    global String objName;
    global String dlscopeName;    
    
    public BatchToCreateConnectionRecordsNew(Set<Id> sharingIds){
        this.sharingIds = sharingIds;    
    }
    
    //Return sharing ids
    global Iterable<Id> start(Database.BatchableContext BC){
        
        List<Id> shareIds = new List<Id>();
        shareIds.addAll(sharingIds);
        return shareIds;
    
    }
    
    //Execute Method to create connection record
    global void execute(Database.BatchableContext BC, List<Id> scope){

        
        List<Error_Logs__c> errorLogList = new List<Error_Logs__c>();
        List<Error_Logs__c> errorLogListNew = new List<Error_Logs__c>();
        
        //List of fields assigned for Error_Logs__c Object create access
        /*List<String> fieldList = new List<String>{
            'In_Form__Description__c'
        };*/ 

        //Check create access for Error_Logs__c and its fields 
        //if(InForm_CRUDFLS_Util.getCreateAccessCheck(String.valueOf(Schema.SObjectType.Error_Logs__c.getSobjectType()),fieldList)){

            for(id obj: scope){
            
                Error_Logs__c error1 = new Error_Logs__c(Description__c = obj);     
                errorLogList.add(error1);
                
            }
        //}
                
        if(!errorLogList.isEmpty()) {
            insert errorLogList;
        }
                    
        List<PartnerNetworkRecordConnection> connectionsList =  new  List<PartnerNetworkRecordConnection>();
        networkId = BatchToCreateDLRecordHelper.getConnectionId(System.Label.ConnectionName);
        
        //List of fields assigned for PartnerNetworkRecordConnection Object create access
        //fieldList = new List<String>{'ConnectionId','LocalRecordId','SendClosedTasks','SendOpenTasks','SendEmails'}; 

        //Check create access for PartnerNetworkRecordConnection and its fields 
        //if(InForm_CRUDFLS_Util.getCreateAccessCheck('PartnerNetworkRecordConnection',fieldList)){        
            for(id obj: scope){
                    
                PartnerNetworkRecordConnection newConnection = new PartnerNetworkRecordConnection(ConnectionId = networkId,LocalRecordId = obj,SendClosedTasks = false,SendOpenTasks = false,SendEmails = false);
                
                connectionsList.add(newConnection);
            }
        //}
                
        if(!connectionsList.isEmpty()){
             
            List<Database.SaveResult> connectionsListresult = Database.insert(connectionsList,false);
            for (Database.SaveResult sr : connectionsListresult ) {
                    
                if (sr.isSuccess()) {
                    // This condition will be executed for successful records and will fetch the ids of successful records
                    System.debug('Successfully updated . Record ID is : ' + sr.getId());
                }
                else {
                    errorLogList = new List<Error_Logs__c>();

                    //List of fields assigned for Error_Logs__c Object create access
                    /*fieldList = new List<String>{
                        'In_Form__Description__c'
                    };*/ 

                    //Check create access for Error_Logs__c and its fields 
                    //if(InForm_CRUDFLS_Util.getCreateAccessCheck(String.valueOf(Schema.SObjectType.Error_Logs__c.getSobjectType()),fieldList)){

                        // This condition will be executed for failed records
                        for(Database.Error objErr : sr.getErrors()) {
                            Error_Logs__c error = new Error_Logs__c(Description__c = objErr.getMessage());
                                            
                            errorLogList.add(error);         
                        }     
                    //}     
                }
            }
            if(!errorLogList.isEmpty()){
                insert errorLogListNew;
            }             
        }
  
        
    }
    
    global void finish(Database.BatchableContext BC){}

}