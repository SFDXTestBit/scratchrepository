@isTest
public class SplitRentChargeLightning_Test {
    //Test data setup
    static testmethod void splitRentChargeData(){
        Transaction_Type__c dailytype = new Transaction_Type__c(Daily_Amount__c = false,Is_Active__c=true,Type__c = 'Daily',Name = 'DailyType');
        INSERT dailyType;
              
        Id rId = Schema.SObjectType.Tenancy__c.getRecordTypeInfosByName().get('Tenancy').getRecordTypeId();

        Id transId = Schema.SObjectType.Transaction__c.getRecordTypeInfosByName().get('Rent charge').getRecordTypeId();

        Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace',No_financial_data__c=true);
        INSERT oBedspace;
        
        
        Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
        INSERT con;
        
        Transaction_Type__c trantype = new Transaction_Type__c();
        trantype.Name='Daily Transaction Calculation';
        trantype.Is_Active__c =false; 
        trantype.Type__c='Daily';
        trantype.Switch_off_all_financial_data__c=false;
        insert trantype;
        
        Tenancy__c ten1 = new Tenancy__c();
        ten1.recordTypeid = rId;
        ten1.Client__c = con.Id;
        ten1.Start_date__c=Date.Today().addDays(-364);
        ten1.End_date__c=Date.Today();
        ten1.Status__c='Current';
        ten1.Balance_are_up_to_Date__c=true;
        ten1.Bedspace__c=oBedspace.Id;
        INSERT ten1;
        
        Rent_Schedule__c rentSch = new Rent_Schedule__c(Tenancy__c = ten1.Id,Start_date__c=Date.Today().addDays(-364),End_date__c=Date.Today(),Bedspace__c= oBedspace.Id,Total_rent_charge__c=300);
                
        
        List<Transaction__c> tranList = new List<Transaction__c>();
        Transaction__c tran1 = new Transaction__c(Tenancy__c = ten1.Id,RecordTypeId = transId,Amount__c=-100,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-1));
        tranList.add(tran1);
        Transaction__c tran2 = new Transaction__c(Tenancy__c = ten1.Id,RecordTypeId = transId,Amount__c=-50,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-2));
        tranList.add(tran2);
        //INSERT tranList;
        List<Transaction__c> rsList = [SELECT Id,Name FROM Transaction__c WHERE Tenancy__c=:ten1.Id];
        System.assertEquals(0, rsList.size(), '');
        
        List<amountTypeWrapper> atw = new List<amountTypeWrapper>();
        amountTypeWrapper amtwrap = new amountTypeWrapper();
        amtwrap.type='Test';
        amtwrap.amount=100;
        amountTypeWrapper amountwrap = new amountTypeWrapper();
        amountwrap.type='Test Data';
        amountwrap.amount=200;
        atw.add(amtwrap);
        atw.add(amountwrap);
        String amountJSON = JSON.serialize(atw);
                
        SplitRentChargeLightning.checkfinancialData(ten1.id);
        SplitRentChargeLightning.getBedSpaceid(ten1.id);
        SplitRentChargeLightning.splitRent(ten1.id);
        SplitRentChargeLightning.getTypes();
        SplitRentChargeLightning.save(rentSch,amountJSON,ten1.id);
        SplitRentChargeLightning.getFiscalLastDate();
        
    }
        static testmethod void splitRentCharge(){
        
        Id rId = Schema.SObjectType.Tenancy__c.getRecordTypeInfosByName().get('Tenancy').getRecordTypeId();

        Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace',No_financial_data__c=true);
        INSERT oBedspace;
                
        Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
        INSERT con;

        Tenancy__c ten1 = new Tenancy__c();
        ten1.recordTypeid = rId;
        ten1.Client__c = con.Id;
        ten1.Start_date__c=Date.Today().addDays(-364);
        ten1.End_date__c=Date.Today();
        ten1.Status__c='Current';
        ten1.Balance_are_up_to_Date__c=true;
        ten1.Bedspace__c=oBedspace.Id;
        INSERT ten1;


        List<amountTypeWrapper> atw = new List<amountTypeWrapper>();
        amountTypeWrapper amtwrap = new amountTypeWrapper();
        amtwrap.type='Test';
        amtwrap.amount=300;
        amountTypeWrapper amountwrap = new amountTypeWrapper();
        amountwrap.type='Test Data';
        amountwrap.amount=400;
        atw.add(amtwrap);
        atw.add(amountwrap);
        String amountJSON = JSON.serialize(atw);
        
        Rent_Schedule__c rentSch = new Rent_Schedule__c(Tenancy__c = ten1.Id,Start_date__c=Date.Today().addDays(-364),End_date__c=Date.Today(),Bedspace__c= oBedspace.Id,Total_rent_charge__c=300);
        INSERT rentSch;
        System.assertNotEquals(null, rentSch.Id);
        SplitRentChargeLightning.splitRent(ten1.id);
        SplitRentChargeLightning.save(rentSch,amountJSON,ten1.id);
        SplitRentChargeLightning.getFiscalLastDate();

        }

        static testmethod void splitRentChar(){
        
        Id rId = Schema.SObjectType.Tenancy__c.getRecordTypeInfosByName().get('Tenancy').getRecordTypeId();

        Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace',No_financial_data__c=true);
        INSERT oBedspace;
                
        Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
        INSERT con;

        Tenancy__c ten1 = new Tenancy__c();
        ten1.recordTypeid = rId;
        ten1.Client__c = con.Id;
        ten1.Start_date__c=Date.Today().addDays(-364);
        ten1.End_date__c=Date.Today();
        ten1.Status__c='Current';
        ten1.Balance_are_up_to_Date__c=true;
        ten1.Bedspace__c=oBedspace.Id;
        INSERT ten1;


        List<amountTypeWrapper> atw = new List<amountTypeWrapper>();
        amountTypeWrapper amtwrap = new amountTypeWrapper();
        amtwrap.type='Test';
        amtwrap.amount=300;
        amountTypeWrapper amountwrap = new amountTypeWrapper();
        amountwrap.type='Test Data';
        amountwrap.amount=400;
        atw.add(amtwrap);
        atw.add(amountwrap);
        String amountJSON = JSON.serialize(atw);
        
        Rent_Schedule__c rentSch = new Rent_Schedule__c(Tenancy__c = ten1.Id,Start_date__c=Date.Today().addDays(-364),End_date__c=Date.Today(),Bedspace__c= oBedspace.Id,Total_rent_charge__c=700);
        INSERT rentSch;
        System.assertNotEquals(null, rentSch.Id);
        SplitRentChargeLightning.splitRent(ten1.id);
        SplitRentChargeLightning.save(rentSch,amountJSON,ten1.id);
        SplitRentChargeLightning.getFiscalLastDate();

        }

        static testmethod void splitRentCh(){
        
        Id rId = Schema.SObjectType.Tenancy__c.getRecordTypeInfosByName().get('Tenancy').getRecordTypeId();

        Bedspace__c oBedspace = new Bedspace__c(Name='TestBedSpace',No_financial_data__c=true);
        INSERT oBedspace;
                
        Contact con = new Contact(Lastname='Test',FirstName='TestFname');
        INSERT con;

        Tenancy__c ten1 = new Tenancy__c();
        ten1.recordTypeid = rId;
        ten1.Client__c = con.Id;
        ten1.Start_date__c=Date.Today().addDays(-364);
        ten1.End_date__c=Date.Today();
        ten1.Status__c=' ';
        ten1.Balance_are_up_to_Date__c=true;
        ten1.Bedspace__c=oBedspace.Id;
        INSERT ten1;
        System.assertNotEquals(null, ten1.Id);

        List<amountTypeWrapper> atw = new List<amountTypeWrapper>();
        amountTypeWrapper amtwrap = new amountTypeWrapper();
        amtwrap.type='';
        amtwrap.amount=300;
        amountTypeWrapper amountwrap = new amountTypeWrapper();
        amountwrap.type='';
        amountwrap.amount=400;
        atw.add(amtwrap);
        atw.add(amountwrap);
        String amountJSON = JSON.serialize(atw);
        
        Rent_Schedule__c rentSch = new Rent_Schedule__c(Tenancy__c = null,Start_date__c=Date.Today().addDays(+3),End_date__c=Date.Today(),Bedspace__c= oBedspace.Id,Total_rent_charge__c=700);
        
        SplitRentChargeLightning.save(rentSch,amountJSON,ten1.id);
        SplitRentChargeLightning.getBedSpaceid(null);
        
        }
    static testmethod void splitRen(){
        
        Id rId = Schema.SObjectType.Tenancy__c.getRecordTypeInfosByName().get('Tenancy').getRecordTypeId();

        Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace',No_financial_data__c=true);
        INSERT oBedspace;
                
        Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
        INSERT con;

        Tenancy__c ten1 = new Tenancy__c();
        ten1.recordTypeid = rId;
        ten1.Client__c = con.Id;
        ten1.Start_date__c=Date.Today().addDays(-474);
        ten1.End_date__c=Date.Today();
        ten1.Status__c='Current';
        ten1.Balance_are_up_to_Date__c=true;
        ten1.Bedspace__c=oBedspace.Id;
        INSERT ten1;


        List<amountTypeWrapper> atw = new List<amountTypeWrapper>();
        amountTypeWrapper amtwrap = new amountTypeWrapper();
        amtwrap.type='Test';
        amountTypeWrapper amountwrap = new amountTypeWrapper();
        amountwrap.type='Test Data';
        atw.add(amtwrap);
        atw.add(amountwrap);
        String amountJSON = JSON.serialize(atw);
        
        Rent_Schedule__c rentSch = new Rent_Schedule__c(Tenancy__c = ten1.Id,Start_date__c=Date.Today().addDays(-474),End_date__c=Date.Today(),Bedspace__c= oBedspace.Id);
        INSERT rentSch;
        
        SplitRentChargeLightning.save(rentSch,amountJSON,ten1.id);
        SplitRentChargeLightning.getBedSpaceid(ten1.id);
        
        }

       
}