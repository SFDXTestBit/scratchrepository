/*------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 01/02/2017 07:50
 * Last Modified by:: Saurabh Gupta,  27/09/2017 12:01
 * Description: Rest  class to insert batch completion date and batch id.
  history : v1.0 - 31/10/2018 - Changes For Security Review
------------------------------------------------------------------*/
@RestResource(urlMapping='/dlBatch/*')
global with sharing class RestClassfordlBatch {  
   
    @HttpPost
    global static String doPost(String batchId) {

        try{
            //List<String> fieldList=new List<String>{'In_Form__Batch_Completion_Date__c','In_Form__Batch_Status__c','In_Form__Batch_Id__c'};
            
            //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__DL_Batch__c',fieldList)){
                In_Form__DL_Batch__c dlBatch = new In_Form__DL_Batch__c();
                dlBatch.In_Form__Batch_Completion_Date__c = System.today();
                dlBatch.In_Form__Batch_Status__c = 'Scheduled'; 
                dlBatch.Batch_Id__c = batchId;
                
                insert dlBatch;
                return dlBatch.Id;
            //}
            //else
            //{
               //return 'Error Occured';
            //}   
        }
        catch(Exception e){
            return 'Error Occured';
        }

    } 

}