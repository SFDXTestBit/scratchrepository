/*------------------------------------------------------------------
/* Created By : 
 * Created date : 19/8/2015
 * Description: Test class to generate rent schedule and rent stream.
 * History : 30-10-2018 Modified By Bhargav Shah For Security Review
------------------------------------------------------------------*/
@isTest
public class MassUpload_Test {
    //Test method to test Success scenario
    public static testmethod void TestMassUpload(){
        try{
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            INSERT oBedspace;
            Test.startTest();
            String csvString = 'Bedspace Name,Start Date,End Date,Total Rent Charged,Amount-Rent Stream Charge,Type -Rent Stream Charge,Is Default\n'+
                'testBedSpace,1/3/2015,2/4/2016,100,100,Housing benefit,TRUE\n'+
                'testBedSpace,1/6/2015,2/7/2016,100,100,Resident Contribution,false\n'+
                'testBedSpace,,,100,100,Housing benefit,TRUE\n'+
                'testBpace7248,1/1/2013,2/4/2017,100,100,Housing benefit,false\n';
            
            blob bString = Blob.valueOf(csvString);
            Document doc = new Document();
            doc.Body=bString;
            doc.Name='RentStream.csv';
            MassUploadController massUpload = new MassUploadController();
            massUpload.inputFile = doc;
            massUpload.createRecord();
            massUpload.createAuditCSV();
            List<Rent_Schedule__c> rsList = [SELECT ID,Name FROM Rent_Schedule__c WHERE Bedspace__c =: oBedspace.Id]; 
            System.assertNotEquals(null, oBedspace.Id, 'SUCCESS');
            Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }
    }
    
    //Test method to test when there is no file attached
    public static testmethod void TestMassUploadNoFile(){
        try{
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            INSERT oBedspace;
            Test.startTest();
                String csvString = 'Bedspace Name,Start Date,End Date,Total Rent Charged,Amount-Rent Stream Charge,Type -Rent Stream Charge\n'+
                                    'testBedSpace,1/3/2015,2/4/2016,100,100,Housing benefit\n'+
                                    'testBedSpace,1/3/2015,2/4/2016,100,100,Resident Contribution\n'+
                                    'testBedSpace,,,100,100,Housing benefit\n'+
                                    'testBpace7248,1/1/2013,2/4/2017,100,100,Housing benefit\n';
                                    
                blob bString = Blob.valueOf(csvString);
                Document doc = new Document();
                doc.Body=bString;
                MassUploadController massUpload = new MassUploadController();
                massUpload.inputFile = doc;
                massUpload.createRecord();
                List<Rent_Schedule__c> rsList = [SELECT ID,Name FROM Rent_Schedule__c WHERE Bedspace__c =: oBedspace.Id];
                System.assertEquals(0, rsList.SIze(), 'No File');
            Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }            
    }
    
    //Test method to test when there is file with wrong type
    public static testmethod void TestMassUploadWrongFile(){
        try{
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            INSERT oBedspace;
            Test.startTest();
                String csvString = 'Bedspace Name,Start Date,End Date,Total Rent Charged,Amount-Rent Stream Charge,Type -Rent Stream Charge\n'+
                                    'testBedSpace,1/3/2015,2/4/2016,100,100,Housing benefit\n'+
                                    'testBedSpace,1/3/2015,2/4/2016,0,-100,Resident Contribution\n'+
                                    'testBedSpace,,,100,100,Housing benefit\n'+
                                    'testBpace7248,1/1/2013,2/4/2017,100,100,Housing benefit\n';
                                    
                blob bString = Blob.valueOf(csvString);
                Document doc = new Document();
                doc.Body=bString;
                doc.Name='RentStream.xls';
                MassUploadController massUpload = new MassUploadController();
                massUpload.inputFile = doc;
                massUpload.createRecord();
                List<Rent_Schedule__c> rsList = [SELECT ID,Name FROM Rent_Schedule__c WHERE Bedspace__c =: oBedspace.Id];
                System.assertEquals(0, rsList.SIze(), 'No File');
            Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }     
    }
    
    //Test method to test exception generation
    public static testmethod void TestMassUploadException(){
        try{
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            INSERT oBedspace;
            Test.startTest();
                String csvString = 'Bedspace Name,Start Date,End Date,Total Rent Charged,Amount-Rent Stream Charge,Type -Rent Stream Charge,Is Default\n'+
                                    'testBedSpace,1/3/2015,"2/4/2016,100",100,"Housing benefit",true\n'+
                                    'testBedSpace,1/3/2015,2/4/2016,100,109,Resident Contribution,false\n'+
                                    'testBedSpace,,,100,100,Housing benefit,true\n'+
                                    'testBpace7248,1/1/2013,2/4/2017,100,100,Housing benefit,false\n';
                                    
                blob bString = Blob.valueOf(csvString);
                Document doc = new Document();
                doc.Body=bString;
                doc.Name='RentStream.csv';
                MassUploadController massUpload = new MassUploadController();
                massUpload.inputFile = doc;
                massUpload.createRecord();
                List<Rent_Schedule__c> rsList = [SELECT ID,Name FROM Rent_Schedule__c WHERE Bedspace__c=:oBedspace.Id];
                System.assertEquals(0, rsList.SIze(), 'No File');
            Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    }

}