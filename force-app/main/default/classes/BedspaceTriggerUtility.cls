/*------------------------------------------------------------------
/* Created By : 
 * Created date : 15/9/2015
 * Description: Utility class for Bedspace Trigger
 * History: v1.1 - Sneha U. - 31/10/2018 - Added with sharing keyword,Removed debug statements,Added Try/Catch
------------------------------------------------------------------*/ 
// Method to update Bedspace record if Status is not selected
public with sharing class BedspaceTriggerUtility {
    public static void updateStatus(List<Bedspace__c> bedList){
   
          for(Bedspace__c bed : bedList){
              if(bed.Status__c == null ){
                  bed.Status__c = 'Void - available To Let';
              }
          }
     
    }
}