/*------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 13/01/2016 05:41
 * Last Modified by: Bhargav Shah. - 31/10/2018 - Enforcing CRUD/FLS
 * Description: Batch class to create transaction on creation of tenancy
------------------------------------------------------------------*/
global with sharing class RentStreamUpdateTransactionsBatch implements Database.Batchable<sObject>,Database.Stateful{
    
    global List<Rent_Stream_Charge__c> rSCs;
    global Set<Id> tenancyIds;
    boolean weeklyAmount;
    //Constructor Initialization
    global RentStreamUpdateTransactionsBatch(List<Rent_Stream_Charge__c> rSCs){
        this.tenancyIds = new Set<Id>();
        this.rSCs = rSCs;
        weeklyAmount = false;
    }
    
    //return transaction records of Rent charge recordtype
    global Database.QueryLocator start(Database.BatchableContext BC){
        
            tenancyIds = new Set<Id>();
            
            String rentChargeRecordTypeId = Schema.SObjectType.Transaction__c.getRecordTypeInfosByName().get('Rent charge').getRecordTypeId();
            //List<String> rentStreamCharge = new List<String>{'In_Form__Rent_Schedule__r.In_Form__Start_Date__c','In_Form__Rent_Schedule__r.In_Form__End_Date__c','In_Form__Rent_Schedule__r.In_Form__Bedspace__c','In_Form__Rent_Schedule__r.In_Form__Tenancy__c','In_Form__In_Form__Amount__c','In_Form__Type__c'};
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Rent_Stream_Charge__c',rentStreamCharge)){
                rSCs = [SELECT Id, Rent_Schedule__r.Start_Date__c,Rent_Schedule__r.End_Date__c,Rent_Schedule__r.Bedspace__c, Rent_Schedule__r.Tenancy__c,Amount__c,Type__c 
                        FROM Rent_Stream_Charge__c WHERE Id IN: rSCs ORDER BY Rent_Schedule__r.CreatedDate DESC];
            
                for(Rent_Stream_Charge__c rSC : rSCs){
                    tenancyIds.add(rSC.Rent_Schedule__r.Tenancy__c); 
                }
                //List<String> transactionsFieldsList = new List<String>{'In_Form__Tenancy__r.In_Form__Bedspace__c','In_Form__Transaction_Date__c','In_Form__Amount__c','In_Form__Type__c','In_Form__Balance__c','In_Form__Tenancy__c','In_Form__Tenancy__r.In_Form__Bedspace__r.In_Form__No_financial_data__c'};
                //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction__c',transactionsFieldsList)){
                    return Database.getQueryLocator('SELECT Id, Tenancy__r.Bedspace__c,Transaction_Date__c,Amount__c, Type__c,Balance__c,createdDate, Tenancy__c, Tenancy__r.Bedspace__r.No_financial_data__c FROM Transaction__c WHERE Tenancy__c IN: tenancyIds AND RecordTypeId =: rentChargeRecordTypeId');
                //}
            //}
        
        return null;
    }
    
    //Execute method to update Transaction 
    global void execute(Database.BatchableContext BC, List<Transaction__c> scope){
        try{
            Boolean switchoffFinancial=false;
            scope=transactionUtility.createRentScheduleTransaction(scope,rSCs,weeklyAmount);
            systemBatchHelper.isBatchRunning=true;
            UPDATE scope;
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    //Finish method to execute TransactionTotalUpdationBatch class
    global void finish(Database.BatchableContext BC){
        try{
            Date startDate = Date.today();
            //List<String> transactionsFieldsList = new List<String>{'In_Form__Transaction_Date__c','In_Form__Tenancy__c'};
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction__c',transactionsFieldsList)){
                for(Transaction__c trans : [SELECT Transaction_Date__c FROM Transaction__c WHERE Tenancy__c IN: tenancyIds ORDER BY Transaction_Date__c LIMIT 1]){
                    startDate = trans.Transaction_date__c.date();
                }
                
                Database.executeBatch(new TransactionTotalUpdationBatch(tenancyIds, startDate),5);
            //}
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
}