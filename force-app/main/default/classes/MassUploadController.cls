/*------------------------------------------------------------------
/* Created By : Saurabh Gupta ,  
 * Created date : 13/01/2016 05:41
 * Last Modified By Saurabh Gupta ,  27/09/2017 11:56
 * Description: Controller class for Mass upload
 * History: v1.1 - Sneha U. - 26/10/2018 - Enforcing CRUD/FLS
------------------------------------------------------------------*/
public with Sharing Class MassUploadController{
    public Document inputFile{get;set;}
    public List<Rent_Schedule__c> rentSList;
    public List<Rent_Stream_Charge__c> rentChargeList;
    Map<Rent_Schedule__c,String> successErrorScheduleMap;
    Map<Rent_Stream_Charge__c,String> successErrorStreamMap;
    List<mapWrapper> mapping;
    public List<CsvWrapper> csvWrapperList {get;set;}
    Map<Id,String> bedSpaceMap;
    Map<Rent_Schedule__c,List<Rent_Stream_Charge__c>> rentScheduleChargeMap;
    
    public MassUploadController(){
        inputFile= new Document();
        rentSList = new List<Rent_Schedule__c>();
        rentChargeList = new List<Rent_Stream_Charge__c>();
        successErrorScheduleMap = new Map<Rent_Schedule__c,String>();
        successErrorStreamMap = new Map<Rent_Stream_Charge__c,String>();
        mapping = new List<mapWrapper>();
        rentScheduleChargeMap = new Map<Rent_Schedule__c,List<Rent_Stream_Charge__c>>();
        csvWrapperList = new List<CsvWrapper>();
        bedSpaceMap = new Map<Id,String>();
    }
    
    //Method to create record from Csv
    public PageReference createRecord(){
        
        try{
            if(inputFile.name !=null){
                if(inputFile.name.endsWith('.csv')){
                    List<List<String>> csvParse = CsvParser.parseCSV(inputFile.Body.ToString(),true);
                    List<Rent_Stream_Charge__c> rentStreamChargeUpdateList = new List<Rent_Stream_Charge__c>();
                    createRentRecords(csvParse);
                    List<Rent_Schedule__c> rentScheduleUpdateList = new List<Rent_Schedule__c>(rentScheduleChargeMap.keySet());
                    Map<Rent_Schedule__c,Id> idMap = new Map<Rent_Schedule__c,Id>();
                    //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Rent_Schedule__c',null)){
                        Database.SaveResult[] srScheduleList = Database.INSERT(rentScheduleUpdateList,false);
                    for(integer i=0;i<srScheduleList.size(); i++){
                        if(srScheduleList[i].IsSuccess()){
                            successErrorScheduleMap.put(rentScheduleUpdateList[i], 'SUCCESS');
                            idMap.put(rentScheduleUpdateList[i],srScheduleList[i].getId());
                        }
                        else{
                            String errorMessage = 'FAILURE : ';
                            List<Database.Error> errorList = srScheduleList[i].getErrors();
                            for(Database.Error err : errorList){
                                errorMessage += err.getMessage();
                            } 
                            successErrorScheduleMap.put(rentScheduleUpdateList[i], errorMessage);
                        }
                    } 
                    
                    //}                    
                    for(Rent_Schedule__c rs : rentScheduleChargeMap.Keyset()){
                        for(MapWrapper mpWrap : mapping){
                            if(rs!=null  && rs.Bedspace__c !=null && rs.Total_Rent_Charge__c != null){
                                String str = rs.Bedspace__c +'~'+ rs.Start_date__c +'~'+ rs.End_Date__c +'~'+ rs.Total_Rent_Charge__c +'~'+ rs.Is_default__c;
                                if(mpWrap.rentScheduleString == str){ 
                                    List<Rent_Stream_Charge__c> rentStream = mpWrap.rentCharge;
                                    if(rentStream != null && rentStream.size()>0){
                                        for(Rent_Stream_Charge__c rsc : rentStream ){

                                            //Check access for In_Form__Rent_Stream_Charge__c object and field
                                            // && InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Rent_Stream_Charge__c',new List<String>{'In_Form__Rent_Schedule__c'})
                                            if(idMap.containsKey(rs)){
                                                rsc.Rent_Schedule__c = rs.Id;
                                                rentStreamChargeUpdateList.add(rsc);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Database.SaveResult[] srStreamList = Database.INSERT(rentStreamChargeUpdateList,false);
                    for(integer i=0;i<srStreamList.size(); i++){
                        if(srStreamList[i].IsSuccess()){
                            successErrorStreamMap.put(rentStreamChargeUpdateList[i],'SUCCESS');
                        }
                        else{
                            String errorMessage = 'FAILURE : ';
                            List<Database.Error> errorList = srStreamList[i].getErrors();
                            for(Database.Error err : errorList){
                                errorMessage += err.getMessage();
                            } 
                            successErrorStreamMap.put(rentStreamChargeUpdateList[i], errorMessage);
                        }
                    }
                    PageReference editList = Page.AuditCsv;
                    editList.setRedirect(false);
                    return editList;
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Please Upload csv files only.'));
                    return null;
                }
               
            }
            else{   
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Please select a file to upload.'));
                return null;
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Corrupted file or data found, Please correct the data and upload again.'));
            return null;
        }
    }
    
    //Mapping of Rent scchedule with rent stream
    private void createRentRecords(List<List<String>> csvString){
        
            Set<String> bedspaceNameSet = new set<String>();
            Map<String,Id> bedspaceNameIDMap = new Map<String,Id>();
            for(List<String> rowString : csvString){
                if(String.IsNotBlank(rowString.get(0))){
                    bedspaceNameSet.add(rowString.get(0));
                }
            }
            
            List<Bedspace__c> bedspaceList = [SELECT Id,Name FROM Bedspace__c WHERE Name IN : bedspaceNameSet];
            for(Bedspace__c oBed : bedspaceList){
                bedspaceNameIDMap.put(oBed.Name,oBed.Id);
                bedSpaceMap.put(oBed.Id,oBed.Name);
            }

            //List of fields assigned for In_Form__Rent_Stream_Charge__c Object create access
            /*List<String> fieldList = new List<String>{
                'In_Form__Amount__c',
                'In_Form__Type__c'
            };*/ 

            //Check create access for In_Form__Rent_Stream_Charge__c and its fields 
            //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Rent_Stream_Charge__c',fieldList)){   

                for(List<String> rowString : csvString){  
                    Rent_Schedule__c oRentSchdule = new Rent_Schedule__c();
                    Rent_Stream_Charge__c oRentStream = new Rent_Stream_Charge__c();
                    if(bedspaceNameIDMap.containsKey(rowString.get(0)))
                        oRentSchdule.Bedspace__c = bedspaceNameIDMap.get(rowString.get(0));
                    if(String.IsNotBlank(rowString.get(1)))
                        oRentSchdule.Start_Date__c = Date.Parse(rowString.get(1));
                    if(String.IsNotBlank(rowString.get(2)))
                        oRentSchdule.End_Date__c = Date.Parse(rowString.get(2));
                    if(String.IsNotBlank(rowString.get(3)))
                        oRentSchdule.Total_Rent_Charge__c = Double.ValueOf(rowString.get(3));
                    if(String.IsNotBlank(rowString.get(4)))
                        oRentStream.Amount__c = Double.ValueOf(rowString.get(4));
                    if(String.IsNotBlank(rowString.get(5)))
                        oRentStream.Type__c = rowString.get(5);
                    if(String.IsNotBlank(rowString.get(6))){
                        oRentSchdule.Is_default__c = rowString.get(6).contains('TRUE');
                    }
                    
                    if(rentScheduleChargeMap.containsKey(oRentSchdule)){
                        List<Rent_Stream_Charge__c> rentSCList = rentScheduleChargeMap.get(oRentSchdule);
                        rentSCList.add(oRentStream);
                        rentScheduleChargeMap.put(oRentSchdule,rentSCList);
                    }
                    else{
                        List<Rent_Stream_Charge__c> rentSCList = new List<Rent_Stream_Charge__c>();
                        rentSCList.add(oRentStream);
                        rentScheduleChargeMap.put(oRentSchdule,rentSCList);
                    }
                    
                }
            //}
            for(Rent_Schedule__c oRes : rentScheduleChargeMap.keyset()){
                MapWrapper mapWrapper = new MapWrapper();
                mapWrapper.rentScheduleString = oRes.Bedspace__c +'~'+ oRes.Start_date__c +'~'+ oRes.End_Date__c +'~'+ oRes.Total_Rent_Charge__c + '~' + oRes.Is_Default__c;
                mapWrapper.rentCharge =rentScheduleChargeMap.get(oRes);
                mapWrapper.rentSchedule = oRes;
                mapping.add(mapWrapper);
            }
       
    }
    
    //Row details capture 
    public class MapWrapper{
        String rentScheduleString{get;set;}
        List<Rent_Stream_Charge__c> rentCharge {get;set;}
        Rent_Schedule__c rentSchedule {get;set;}
    }
    
    //Output csv creation
    public class CsvWrapper{
       public String bedspaceName{get;set;}
        public String startDate{get;set;}
        public String endDate{get;set;}
        public String totalRent{get;set;}
        public String amount{get;set;}
        public String type{get;set;}
        public String isDefault{get;set;}
        public String error{get;set;}
        
    }
    
    //Action method for Audit
    public void createAuditCSV(){
        try{
            for(MapWrapper mapWrap : mapping){
                for(Rent_Stream_Charge__c rsc : mapWrap.rentCharge){
                    for(Rent_Schedule__c rentS : successErrorScheduleMap.keyset()){
                        String str = rentS.Bedspace__c +'~'+ rentS.Start_date__c +'~'+ rentS.End_Date__c +'~'+ rentS.Total_Rent_Charge__c + '~' + rentS.Is_Default__c;
                        if(mapWrap.rentScheduleString == str){
                            CsvWrapper csvWrap = new CsvWrapper();
                            if(bedSpaceMap.containsKey(rentS.Bedspace__c))
                                csvWrap.bedspaceName = bedSpaceMap.get(rentS.Bedspace__c);
                            csvWrap.startDate = String.Valueof(rentS.Start_Date__c);
                            if(rsc.Type__c != null){
                            string typeStr = rsc.Type__c;
                            typeStr = typeStr.replace(',','');
                            typeStr = typeStr.replace('\r\n', '');
                            typeStr = typeStr.replace('\n', '');
                            typeStr = typeStr.replace('\r', '');
                            csvWrap.type =  typeStr;
                            }
                            csvWrap.enddate = String.Valueof(rentS.End_Date__c);
                            csvWrap.totalRent = String.Valueof(rentS.Total_Rent_Charge__c);
                            csvWrap.amount = String.Valueof(rsc.Amount__c);
                            csvWrap.isDefault = String.valueOf(rentS.Is_Default__c);
                            csvWrap.error = successErrorScheduleMap.get(rentS);
                            for(Rent_Stream_Charge__c rentSC : successErrorStreamMap.keyset()){
                                if(rentSC.Rent_Schedule__c == rentS.Id){
                                    csvWrap.error = successErrorStreamMap.get(rentSC);
                                }
                            }
                            csvWrapperList.add(csvWrap);
                        }
                    }
                }
            }
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,e.getMessage()));
        }
    }
}