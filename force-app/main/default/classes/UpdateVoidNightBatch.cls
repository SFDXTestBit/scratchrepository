/*------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 26/02/2016 13:09
 * Last Modified by:: Saurabh Gupta , 27/09/2016 12:06
 * Description: Batch class to update Night object records
 * History: v1.1 - Sneha U. - 31/10/2018 - Added Read access check, Added update access check ,Added Try/Catch block
------------------------------------------------------------------*/
global with sharing class UpdateVoidNightBatch implements Database.Batchable<sObject>{
    //Initialization
    In_Form__Night__c night;
    In_Form__Tenancy__c tenancy;
    
    //Constructor
    public UpdateVoidNightBatch(In_Form__Tenancy__c tenancy,In_Form__Night__c night){
        this.tenancy = tenancy;
        this.night = night;
        
    }
    
    //Return night object records whose date is between tenancy start date and end date
    global Database.QueryLocator start(Database.BatchableContext BC){
        String tenancyId = tenancy.Id;
        Date startDate = tenancy.In_Form__Start_date__c;
        Date endDate = tenancy.In_Form__End_date__c;

        //List of fields assigned for In_Form__Night__c Object read access
        /*List<String> fieldList = new List<String>{
        'In_Form__Date__c',
        'In_Form__Tenancy__c',
        'In_Form__Void_reason__c',
        'In_Form__Void_status__c'
        };*/

        //Check read access for In_Form__Night__c Object
        //if(InForm_CRUDFLS_Util.getReadAccessCheck(String.valueOf(Schema.SObjectType.Night__c.getSobjectType()),fieldList) ){
            String str = String.escapeSingleQuotes(tenancyId);
            String query = 'SELECT Id,In_Form__Date__c,In_Form__Tenancy__c,In_Form__Void_reason__c,In_Form__Void_status__c FROM In_Form__Night__c WHERE In_Form__Tenancy__c =: str AND Date__c >=: startDate AND Date__c <=:endDate';
            return Database.getQueryLocator(query);
        //}
        //else{
            //return null;
        //}
        
    }
    
    //Update night object reason and status with void reason and void status
    global void execute(Database.BatchableContext BC,List<In_Form__Night__c> objList){
        try{
            //List of fields assigned for In_Form__Night__c Object read access
            /*List<String> fieldList = new List<String>{
            'In_Form__Void_status__c',
            'In_Form__Void_reason__c'
            };*/

            //Check update access for In_Form__Night__c Object
            //if(InForm_CRUDFLS_Util.getUpdateAccessCheck(String.valueOf(Schema.SObjectType.Night__c.getSobjectType()),fieldList)){
                for (Night__c obj : objList) {
                    obj.In_Form__Void_reason__c = night.In_Form__Void_reason__c;
                    obj.In_Form__Void_status__c = night.In_Form__Void_status__c;
                }
                update objList;
            //}
        }catch(Exception e){
            system.debug('Exception:' + e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC){
    
    }
}