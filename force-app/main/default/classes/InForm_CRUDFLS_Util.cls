/*
Class  : InForm_CRUDFLS_Util
Author : Extentia
Details: Util Class for CRUD/FLS check
History: v1.0 - Sneha U. - 10/26/2018 - Initial Creation
*/
public with sharing class InForm_CRUDFLS_Util{

    private static Map<String, Schema.SObjectType> mapObjects = Schema.getGlobalDescribe();

    /********************************************************************************************************************************
         Method    : getUpdateAccessCheck 
         Parameter : ObjectName, list of Fields to check access
    ********************************************************************************************************************************/
    public static boolean getUpdateAccessCheck(string objectName, List<string> FieldsToUpdate) {

        Boolean permission = true;
        try {
            if(String.isNotBlank(objectName) && getObjectUpdateAccessCheck(objectName)){
                Map<String,Schema.SObjectField> m = mapObjects.get( objectName ).getDescribe().fields.getMap();
                if(FieldsToUpdate!=null){
                    for (String fieldToCheck : FieldsToUpdate) {
                        if ((!m.get(fieldToCheck).getDescribe().isAccessible()) && (!m.get(fieldToCheck).getDescribe().isUpdateable())) {
                            permission  = false;
                            break;
                        }
                    }
                }
            }
        }
        catch(Exception e) {
            System.debug('Exception :'+e.getMessage());
            return false;
        }
        return permission;
    }

    /********************************************************************************************************************************
         Method    : getObjectUpdateAccessCheck
         Parameter : ObjectName
    ********************************************************************************************************************************/
    private static boolean getObjectUpdateAccessCheck(string objectName){

        try {
            Schema.DescribeSobjectResult drSObj = mapObjects.get(objectName) != null ? mapObjects.get(objectName).getDescribe() : null;
            if(drSObj != null && drSObj.isAccessible() && drSObj.isUpdateable()){
                return true;
            }else{
                return false;
            }
        }catch(Exception e) {
            System.debug('Exception :'+e.getMessage());
            return false;
        }
        
    }
    
    /******************************************************************************************************************************** 
         Method    : getCreateAccessCheck 
         Parameter : ObjectName, Fields to check access
    *********************************************************************************************************************************/
    public static boolean getCreateAccessCheck(string objectName, List<string> FieldsToInsert) {

        Boolean permission = false;
        try {
            if(String.isNotBlank(objectName)&& getObjectCreateAccessCheck(objectName)){
                Map<String,Schema.SObjectField> m = mapObjects.get( objectName ).getDescribe().fields.getMap();
                permission = true;
                if(FieldsToInsert!=null){
                    for (String fieldToCheck : FieldsToInsert) {
                        if ((!m.get(fieldToCheck).getDescribe().isAccessible()) && (!m.get(fieldToCheck).getDescribe().isCreateable())) {
                            permission  = false;
                            break;
                        }
                    }
                }
            }
            return permission;
      }
      catch(Exception e) {
        System.debug('Exception :'+e.getMessage());
        return false;
      }
    }
    /************************************************************************************************************************ 
         Method    : getObjectCreateAccessCheck
         Parameter : ObjectName
    *************************************************************************************************************************/
    private static boolean getObjectCreateAccessCheck(String objectName){
        
        Schema.DescribeSobjectResult drSObj = mapObjects.get(objectName) != null ? mapObjects.get(objectName).getDescribe() : null;
        if(drSObj != null && drSObj.isAccessible() && drSObj.isCreateable()){
            return true;
        }else{
            return false;
        }
    }

    /******************************************************************************************************************************** 
         Method    : getReadAccessCheck 
         Parameter : ObjectName, Fields to check access
    *********************************************************************************************************************************/
    public static boolean getReadAccessCheck(string objectName, List<string> FieldsToUpdate) {
        try {

            //Map for reference Fields
            Map<String,List<String>> mapReferenceObject = new Map<String,List<String>>();

            if(String.isNotBlank(objectName)&& getObjectReadAccessCheck(objectName)){  
                Map<String,Schema.SObjectField> fieldMap = mapObjects.get( objectName ).getDescribe().fields.getMap();
                map<String, string> relReferenceMap = new map<String, string>();
                for (String fieldName: fieldMap.keySet()) {
                    Schema.DescribeFieldResult fieldDescribe = fieldMap.get(fieldName).getDescribe();
                    String type = string.valueOf(fieldDescribe.getRelationshipName());
                    if(String.isNotBlank(type) && fieldDescribe.getReferenceTo().size() > 0){
                        relReferenceMap.put(type, string.valueOf(fieldDescribe.getReferenceTo()[0].getDescribe().getName()));
                    } 
                }           
                Boolean permission = true;
                if(FieldsToUpdate!=null){
                    for (String  fieldToCheck : FieldsToUpdate) {         
                        if(fieldToCheck.contains('.')) {            
                            List<string> field = fieldToCheck.split('\\.');      

                            //Add to the list of fields for reference object 
                            if(mapReferenceObject.containsKey(relReferenceMap.get(field[0]))){
                                mapReferenceObject.get(relReferenceMap.get(field[0])).add(field[1]);
                            }
                            else{
                                mapReferenceObject.put(relReferenceMap.get(field[0]), new List<String> {field[1]} );
                            }

                        }
                        else {
                            if (fieldMap.containsKey(fieldToCheck) && !fieldMap.get(fieldToCheck).getDescribe().isAccessible()){
                                permission = false;
                                return permission;
                            }
                        }
                    }
                }

                //For Reference Object fields
                for(String referenceObject : mapReferenceObject.keyset()){
                    permission  = getReadAccessCheck(referenceObject, mapReferenceObject.get(referenceObject));
                    
                    //Return if no access
                    if(!permission){
                        return permission;
                    }
                }

            return permission;
            }else{
                return false;
            }
            
        }
        catch(Exception e) {
            System.debug('Exception :'+e.getMessage());
            return false;
        }
    }

    /************************************************************************************************************************ 
         Method    : getObjectCreateAccessCheck
         Parameter : ObjectName
    *************************************************************************************************************************/
    private static boolean getObjectReadAccessCheck(String objectName){
        try{
            Schema.DescribeSobjectResult drSObj = mapObjects.get(objectName) != null ? mapObjects.get(objectName).getDescribe() : null;
            if(drSObj != null && drSObj.isAccessible()){
                return true;
            }else{
                return false;
            }
        }
        catch(Exception e) { System.debug('Exception :'+e.getMessage()); return false;
        }
    }

    /***********************************************************************************************
        Method Name : getDeleteRecordsAccessCheck
        Description : Delete database records
    ************************************************************************************************/
    public static boolean getDeleteRecordsAccessCheck(List<Sobject> listToDelete) {
        Boolean permission = false;
        if(listToDelete != null && listToDelete.size() > 0){
            try{
                permission = (listToDelete[0].getSobjectType()).getDescribe().isDeletable();
            }catch(Exception e) { System.debug('Exception :'+e.getMessage()); return false;
            }
        }
        return permission;
    }
}