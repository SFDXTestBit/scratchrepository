/***************************************************************************************************
Class   : createActionCtrl
Author  : AKohakade
Details : Its controller for Create Action for Multiple client page. It contains functionality to fetch Projects, Sessions, Timeline events, create action.
Coverage Classes:  TestCoverage_createActionCtrl (94%)
History : v1.0 - 21/02/2018 - Created
          v1.1 - 31/10/2018 - Changes For Security Review
          v1.2 - 17/12/2018 - Added check for blank values of client first and last name
****************************************************************************************************/

global with sharing class createActionCtrl {
    
    public createActionCtrl(ApexPages.StandardController stdController) { }
    
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : getProjectList method return the list of project in which Show in Session in true
    Version : v1.0 - 21/02/2018 - Created
    -----------------------------------------------------------------------------------------------*/  
    @RemoteAction
    public static Map<String, Object> getProjectList(){        
        Map<String, Object> mapOfResult = new Map<String, Object>(); 
        Map<String,Object> mapOfReadAccessCheck = getReadAccessCheck('Account', new List<String>{'Name','Show_in_session__c'});
        
        if(mapOfReadAccessCheck.get('Status') == true){
           List<Object> lstOfProj = [SELECT Id,Name,Show_in_session__c FROM Account WHERE Show_in_session__c = true ORDER BY Name ASC];
           mapOfResult.put('Status', 'true');
           mapOfResult.put('Result', lstOfProj);            
           
        }else{
           mapOfResult.put('Status', 'false');
           mapOfResult.put('Error', mapOfReadAccessCheck.get('Output'));           
        }              
        return mapOfResult;
    }
    
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : getSessionList method return the list of sessions belonging to project whose id is taken as parameter to method. 
    Version : v1.0 - 21/02/2018 - Created
    -----------------------------------------------------------------------------------------------*/      
    @RemoteAction
    public static Map<String, Object> getSessionList(String projId){
        Map<String, Object> mapOfResult = new Map<String, Object>();
            Map<String,Object> mapOfReadAccessCheck = getReadAccessCheck('Session__c', new List<String>{'Project__c','Start_date_time__c','Record_type_name__c'});
            
            List<sObject> lstSession = new List<sObject>();
            
            if(String.isNotBlank(projId)){          
                
                if(mapOfReadAccessCheck.get('Status') == true){
                    lstSession = [SELECT Id,Project__c,Start_date_time__c,Record_type_name__c FROM Session__c WHERE Project__c =: String.escapeSingleQuotes(projId) ORDER BY Start_date_time__c DESC LIMIT 100];
                    
                    mapOfResult.put('Status', 'true');
                    mapOfResult.put('Result', lstSession);
                
                }else{          
                    mapOfResult.put('Status', 'false'); 
                    mapOfResult.put('Error', mapOfReadAccessCheck.get('Output'));           
                }
            }else{
                mapOfResult.put('Status', 'false');
                mapOfResult.put('Error', 'No project is selected');
            }
        return mapOfResult;
    }
    
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : getTimeLineEventList method return the list of timeline event which comes under selected project
    Version : v1.0 - 21/02/2018 - Created
    -----------------------------------------------------------------------------------------------*/    
    @RemoteAction
    public static Map<String, Object> getTimeLineEventList(String projId, String currentSession, String selectedSession, String FieldAPI, String eventStatus, List<String> lstColumnFields){        
        Map<String, Object> mapOfResult = new Map<String, Object>();
        
        
            List<Timeline_Event__c> lstTimelineEventAtProj = new List<Timeline_Event__c>();
            Set<Timeline_Event__c> setTimelineEventListDeDuped = new Set<Timeline_Event__c>();       
            Set<String> setClientAtAction = new Set<String>();
            Set<Timeline_Event__c> setTimelineEventListDeDupedActioned = new Set<Timeline_Event__c> ();
            Set<Action__c> setofAcionAtSelectedSession = new Set<Action__c>();
            Set<Timeline_Event__c> setTimelineEventDeDupActionedAtSession = new Set<Timeline_Event__c> ();
            Map<Id, Action__c> mapOfAction = new Map<Id, Action__c>();
            List<String> lstOfFields = new List<String>();
            boolean flag = false;
            
            String qryString;
            
            List<String> lstStatus = new List<String>();
            if(String.isNotBlank(eventStatus))
                lstStatus = eventStatus.split(',');
            
            Map<String,String> mapObjFields = getObjFieldList('Timeline_Event__c').get('MapOfName');
            String nameSpace = getNameSpace('createActionCtrl');
            
            //1. Get all timeline event related to Selected Project            
            lstOfFields = new List<String>{'Client_first_name__c','Client_last_name__c','Project_Lookup__r.Name','Client__c'};                    
            
            if(lstColumnFields != null && lstColumnFields.size() > 0){                
                for(String field: lstColumnFields){                    
                    if(mapObjFields.containsKey(field))
                        lstOfFields.add(field);
                }
            }
            
            Map<String,Object> mapOfReadAccessCheck_TE = getReadAccessCheck('Timeline_Event__c', lstOfFields);
            
            if(String.isNotBlank(projId)){
                if(mapOfReadAccessCheck_TE.get('Status') == true){                    
                    qryString = 'SELECT Id,'+nameSpace+'Client_first_name__c,'+nameSpace+'Client_last_name__c,'+nameSpace+'Client__c,'+nameSpace+'Project_Lookup__r.Name';
                    //Add fields which are being added from configuration setting
                    if(lstColumnFields != null && lstColumnFields.size() > 0){
                        for(String field: lstColumnFields){
                            if(mapObjFields.containsKey(field) && !qryString.contains(field))
                                qryString += ', ' + field ;
                        }
                    }
                    
                    
                    if(!qryString.contains(nameSpace+'Project_Lookup__c'))
                        qryString += ', '+nameSpace+'Project_Lookup__c';
                    
                    qryString +=' FROM '+nameSpace+'Timeline_Event__c WHERE ';

                    if(mapObjFields.containsKey(FieldAPI))
                        qryString += FieldAPI + ' IN:lstStatus AND ';                                        
                    
                    String projectId = String.escapeSingleQuotes(projId);
                    qryString += nameSpace+'Project_Lookup__c =:projectId AND '+nameSpace+'Project_Lookup__r.Show_in_session__c = true ORDER BY '+nameSpace+'Client_name__c ASC, CreatedDate DESC';
                              
                    lstTimelineEventAtProj = Database.query(qryString);
                }else{          
                    mapOfResult.put('Status', 'false'); 
                    mapOfResult.put('Error', mapOfReadAccessCheck_TE.get('Output'));

                    return  mapOfResult;
                }
            }else{
                mapOfResult.put('Status', 'false');
                mapOfResult.put('Error', 'No project is selected');
                
                return  mapOfResult;
            }               
                
            //2. Remove duplicate clients from the list of current timeline events
            Id currentInClient;
            for(Timeline_Event__c event:lstTimelineEventAtProj){
                if (currentInClient != event.Client__c) {
                  setTimelineEventListDeDuped.add(event);
                }
                currentInClient = event.Client__c;
            }
            
            //3. Remove client having Action at this session            
            if(String.isNotBlank(currentSession)){
                Map<String,Object> mapOfReadAccessCheck_Act = getReadAccessCheck('Action__c', new List<String>{'Client__c', 'Session__c', 'Timeline_Event__c', 'Timeline_Event__r.Project_Lookup__r.id'});
                
                if(mapOfReadAccessCheck_Act.get('Status') == true){
                    for(Action__c action:[SELECT Id, Client__c, Session__c, Timeline_Event__c, Timeline_Event__r.Project_Lookup__r.id FROM Action__c WHERE Session__c =: String.escapeSingleQuotes(currentSession) LIMIT 100]){                                         
                        setClientAtAction.add(action.Client__c);                        
                        mapOfAction.put(action.id, action);
                    }
                }else{          
                    mapOfResult.put('Status', 'false'); 
                    mapOfResult.put('Error', mapOfReadAccessCheck_Act.get('Output'));

                    return  mapOfResult;                
                }
            }else{
                mapOfResult.put('Status', 'false');
                mapOfResult.put('Error', 'Current session is not selected');
                
                return  mapOfResult;
            }
            
            //4. Clients who do not have action at current session  
            for(Timeline_Event__c event: setTimelineEventListDeDuped){                  
                if(!setClientAtAction.contains(event.Client__c)){                               
                    setTimelineEventListDeDupedActioned.add(event);
                }else{  
                    flag = false;
                    for(Action__c act : mapOfAction.values()){
                        if(act.Timeline_Event__r.Project_Lookup__r.id == event.Project_Lookup__c && event.Client__c == act.Client__c)
                            flag = true;        
                    }   
                    if(!flag)
                        setTimelineEventListDeDupedActioned.add(event);                 
                }           
                    
            }
                
            if(selectedSession == 'showAll'){                
                mapOfResult.put('Status', 'true');
                mapOfResult.put('Result', setTimelineEventListDeDupedActioned);
                
            }else{
                if(String.isNotBlank(selectedSession)){
                    for(Action__c action:[SELECT Id, Client__c, Session__c, Timeline_Event__c FROM Action__c WHERE Session__c =: String.escapeSingleQuotes(selectedSession) LIMIT 100]){
                        setofAcionAtSelectedSession.add(action);
                    }
                }
                
                for(Action__c action: setofAcionAtSelectedSession){
                    for(Timeline_Event__c event :setTimelineEventListDeDupedActioned){
                        if(event.Client__c == action.Client__c){
                            setTimelineEventDeDupActionedAtSession.add(event);
                        }
                    }
                }
                
                mapOfResult.put('Status', 'true');
                mapOfResult.put('Result', setTimelineEventDeDupActionedAtSession);
            }
                
        return mapOfResult;
    } 
    
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : getObjFieldList method return the map of field and field data types of object whose name is taken as parameter to method
    Version : v1.0 - 21/02/2018 - Created
    -----------------------------------------------------------------------------------------------*/ 
    @RemoteAction
    public static Map<String,Map<String,String>> getObjFieldList(String ObjectName){      
        Map<String,Map<String,String>> mapOfAll =  new Map<String,Map<String,String>>();
    
        
            Map<String,String> mapFieldToApiName = new Map<String,String>();
            Map<String,String> mapFieldToType = new Map<String,String>();
            
            String nameSpacePrefix;
            
            nameSpacePrefix = getNameSpace('createActionCtrl');
            ObjectName = nameSpacePrefix+ObjectName;
            
            if(String.isNotBlank(ObjectName)){      
                Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                Map <String, Schema.SObjectField> fieldMap = schemaMap.get(ObjectName).getDescribe().fields.getMap();
                for(Schema.SObjectField sfield : fieldMap.Values()){
                    mapFieldToApiName.put(sfield.getDescribe().getname(), sfield.getDescribe().getLabel());
                    mapFieldToType.put(sfield.getDescribe().getname(), String.ValueOf(sfield.getDescribe().getType()));
                }
            }
            
            mapOfAll.put('MapOfName',mapFieldToApiName);
            mapOfAll.put('MapOfType',mapFieldToType);
        
        return  mapOfAll;    
    }  
  
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : createAction method creates Action records for list of timeline events as session which will be taken as parameter to method
    Version : v1.0 - 21/02/2018 - Created
    -----------------------------------------------------------------------------------------------*/
    @RemoteAction
    public static Map<String, Object> createAction(List<Timeline_Event__c> lstEvents, String sessionId, String recordTypeName){
        
        Map<String, Object> mapOfcreateActionResult = new Map<String, Object>();
        
        List<Action__c> lstAction = new List<Action__c>();
        Session__c session = new Session__c();
        
        Map<String,String> mapSuccessfullyAdded = new Map<String,String>();
        Map<String,String> mapOfFailed = new Map<String,String>();
        Map<String,String> mapOfClient = new Map<String,String>();
        Map<String, Id> mapOfRecType = new Map<String, Id>();
        
        try{            
            if(String.isNotBlank(sessionId) && String.isNotBlank(recordTypeName)){
                Map<String,Object> mapOfReadAccessCheck_Session = getReadAccessCheck('Session__c', new List<String>{'Start_date_time__c', 'End_date_time__c', 'Record_type_name__c'});
                if(mapOfReadAccessCheck_Session.get('Status') == true){
                    session = [SELECT Start_date_time__c, End_date_time__c, Record_type_name__c FROM Session__c WHERE id=: String.escapeSingleQuotes(sessionId) LIMIT 1];
                }else{          
                    mapOfcreateActionResult.put('Status', 'false'); 
                    mapOfcreateActionResult.put('Error', mapOfReadAccessCheck_Session.get('Output'));
                }
                
                String nameSpace = getNameSpace('createActionCtrl');        
                List<Schema.RecordTypeInfo> lstRecTypesInfo = new List<Schema.RecordTypeInfo>();               
                
                lstRecTypesInfo = Schema.getGlobalDescribe().get(nameSpace+'Action__c').getDescribe().getRecordTypeInfos();
                
                for(Schema.RecordTypeInfo rec : lstRecTypesInfo){
                    if(rec.isActive())
                        mapOfRecType.put(rec.getName(), rec.getRecordTypeId());
                }
                
                if(mapOfRecType.containsKey(recordTypeName)){
                    Id RecTypeId = mapOfRecType.get(recordTypeName);   
            
                    for(Timeline_Event__c event:lstEvents){                 
                        Action__c action = new Action__c();
                        action.RecordTypeId = RecTypeId; 
                        action.Client__c = event.Client__c; 
                        action.Session__c = sessionId;
                        action.Timeline_Event__c = event.id;
                        
                        if(session != null){
                            action.Start_date_time__c = session.Start_date_time__c;      
                            action.End_date_time__c = session.End_date_time__c;
                        }
                        lstAction.add(action);
                        
                        //v1.2 Starts
                        if(event.Client_first_name__c != null && event.Client_last_name__c != null)
                            mapOfClient.put(event.Client__c,event.Client_first_name__c +' '+event.Client_last_name__c);
                        else if(event.Client_first_name__c == null && event.Client_last_name__c != null)
                                mapOfClient.put(event.Client__c,event.Client_last_name__c);
                            else if(event.Client_first_name__c != null && event.Client_last_name__c == null)
                                    mapOfClient.put(event.Client__c,event.Client_first_name__c);
                                else
                                    mapOfClient.put(event.Client__c,'');
                                    
                       //v1.2 Ends             
                    }
                }
            }else{
                mapOfcreateActionResult.put('Status', 'false');
                mapOfcreateActionResult.put('Error', 'Current session is not valid');                
            }
            
            Map<String,Object> mapOfCreateAccessCheck_Act = getCreateAccessCheck('Action__c', new List<String>{'Client__c', 'Session__c', 'Timeline_Event__c', 'Start_date_time__c', 'End_date_time__c'});
            
            if(mapOfCreateAccessCheck_Act.get('Status') == true){           
                if(lstAction.size()>0){
                    Database.SaveResult[] actionList = Database.insert(lstAction, false);
                    integer count = 0;
                    
                    for (Database.SaveResult sr : actionList) {
                        if (sr.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed                    
                            mapSuccessfullyAdded.put(lstAction[count].Client__c+'-'+lstAction[count].Timeline_Event__c,sr.getId());
                        }
                        else {
                            // Operation failed, so get all errors                
                            String errorMessage = '';
                            integer errorCnt = 0;
                            for(Database.Error err : sr.getErrors()) {                        
                                errorMessage += err.getMessage();
                                if(errorCnt < sr.getErrors().size() - 1)
                                    errorMessage += ', ';
                                errorCnt ++;
                            }
                            mapOfFailed.put(lstAction[count].Client__c+'-'+lstAction[count].Timeline_Event__c,errorMessage);
                        }
                        count++;
                    }                
                    mapOfcreateActionResult.put('Status', 'true');
                    mapOfcreateActionResult.put('Successful',mapSuccessfullyAdded);
                    mapOfcreateActionResult.put('Failed',mapOfFailed);
                    mapOfcreateActionResult.put('ClientDetails',mapOfClient);                        
                }
            }else{          
                mapOfcreateActionResult.put('Status', 'false'); 
                mapOfcreateActionResult.put('Error', mapOfCreateAccessCheck_Act.get('Output'));         
            }               
        }catch(Exception e){
            mapOfcreateActionResult.put('Status', 'false'); 
            mapOfcreateActionResult.put('Error', 'Error occured while Action creation.');            
        }       
        return mapOfcreateActionResult;
    }
    
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : checkRecordType method checkes whether given record type is present on Action object or not and return boolean result
    Version : v1.0 - 21/02/2018 - Created
    -----------------------------------------------------------------------------------------------*/
    @RemoteAction
    public static boolean checkRecordType(String recTypeDevName){
        Set<String> setRecTypes = new Set<String> ();
            List<Schema.RecordTypeInfo> lstRecTypesInfo = new List<Schema.RecordTypeInfo>();
            if(String.isNotBlank(recTypeDevName)){           
                String nameSpacePrefix = getNameSpace('createActionCtrl');
                
                lstRecTypesInfo = Schema.getGlobalDescribe().get(nameSpacePrefix+'Action__c').getDescribe().getRecordTypeInfos();
                
                for(Schema.RecordTypeInfo rec : lstRecTypesInfo){
                    if(rec.isActive())
                        setRecTypes.add(rec.getName());
                }                   
                return setRecTypes.contains(recTypeDevName); 
            }else{
                return false;
            }
        
    }
        
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : getReadAccessCheck method checks user has read access to object, fields
    Version : v1.0 - 10/03/2018 - Created
    -----------------------------------------------------------------------------------------------*/ 
    public static Map<String,Object> getReadAccessCheck(string objectNameToCheck, List < string > lstOfFieldsTocreate) {
        
        Map<String,Object> mapOfResult = new Map<String,Object>();
        Boolean flag = true;
        String result;
        String nameSpacePrefix;
        string objectName;
        List < string > FieldsToRead = new List < string >();
        
        nameSpacePrefix = getNameSpace('createActionCtrl');
             
        for(string field: lstOfFieldsTocreate){  
            FieldsToRead.add(nameSpacePrefix+field);
        }
        objectName = nameSpacePrefix+objectNameToCheck;            
       
        try {           
            Map < String, Schema.SObjectField > m = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            map < String, string > relReferenceMap = new map < String, string > ();
            
            
            for (String fieldName: m.keySet()) {
                Schema.DescribeFieldResult fieldDescribe = m.get(fieldName).getDescribe();
                String type = string.valueOf(fieldDescribe.getRelationshipName());
                if (type != null && fieldDescribe.getReferenceTo().size() > 0) {
                    relReferenceMap.put(type, string.valueOf(fieldDescribe.getReferenceTo()[0].getDescribe().getName()));
                }
            }
            for (String fieldToCheck: FieldsToRead) {
                if (fieldToCheck.contains('.')) // to check relationship query
                {
                    List < string > field = fieldToCheck.split('\\.', 2);
                    String parentFieldName = field.remove(0);
                    Schema.DescribeSObjectResult parentObj = Schema.getGlobalDescribe().get(relReferenceMap.get(parentFieldName)).getDescribe();
                    mapOfResult = getReadAccessCheck(parentObj.getName(), new List < string > {field[0]});
                    
                } else {
                    
                    if (m.get(fieldToCheck) != null) {
                        if (!m.get(fieldToCheck).getDescribe().isAccessible()) {
                            flag = false;
                            result = 'No Read access to ' + m.get(fieldToCheck).getDescribe().getLabel() + ' field for ' +  Schema.getGlobalDescribe().get(objectName).getDescribe().getLabel() + ' object.';
                            break;
                        }
                    }
                }
            }
            
        } catch (Exception e) {            
            flag = false; result = 'Exception : ' + e.getMessage();
        }
        
        mapOfResult.put('Status',flag);
        mapOfResult.put('Output',result);
        return mapOfResult;
    }
    
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : getCreateAccessCheck method checks user has create access to object, fields
    Version : v1.0 - 10/03/2018 - Created
    -----------------------------------------------------------------------------------------------*/ 
    public static Map<String,Object> getCreateAccessCheck(string objectNameToCheck, List < string > lstOfFieldsTocreate) {
        Map<String,Object> mapOfResult = new Map<String,Object>();
        
            Boolean flag = true;
            String result = '';
            List < string > FieldsTocreate = new List < string >();
            string objectName;
            String nameSpacePrefix;
            
            nameSpacePrefix = getNameSpace('createActionCtrl');
                  
            for(string field: lstOfFieldsTocreate){
                if(!field.contains(nameSpacePrefix))
                    FieldsTocreate.add(nameSpacePrefix+field);
            }
            objectName = nameSpacePrefix+objectNameToCheck;            
            
            
            Map < String, Schema.SObjectField > m = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            
            for (String fieldToCheck: FieldsTocreate) {
                if (m.get(fieldToCheck) != null) {
                    
                    if (!m.get(fieldToCheck).getDescribe().isCreateable() || !m.get(fieldToCheck).getDescribe().isAccessible()) {
                        flag = false;
                        result += (String.isBlank(result) ? ('No Create access to following fields for ' +  Schema.getGlobalDescribe().get(objectName).getDescribe().getLabel() + ' object: ') : '') + m.get(fieldToCheck).getDescribe().getLabel() +',';
                    }
                }
            }
            if (String.isNotBlank(result)) {
                result = result.removeEnd(',');
            }
            mapOfResult.put('Status',flag);
            mapOfResult.put('Output',result);
        
        return mapOfResult;
    }
    
    /*---------------------------------------------------------------------------------------------- 
    Author  : AKohakade
    Details : getNameSpace method return the namespace used for organization
    Version : v1.0 - 21/02/2018 - Created
    -----------------------------------------------------------------------------------------------*/ 
    public static String getNameSpace(String className){
        String orgNamespace='';
        ApexClass cs = [select NamespacePrefix from ApexClass where Name =: String.escapeSingleQuotes(className)];        
        if(String.isNotBlank(cs.NamespacePrefix))
            orgNamespace = cs.NamespacePrefix+'__';
        return orgNamespace;        
    }
}