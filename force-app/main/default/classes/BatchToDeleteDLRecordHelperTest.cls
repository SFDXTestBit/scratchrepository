@istest
Public class BatchToDeleteDLRecordHelperTest{ 
    Static Testmethod void HelperTest(){
        Test.startTest();
        
        
        BatchToDeleteDLRecordHelper b = new BatchToDeleteDLRecordHelper();
        database.executeBatch(b);
      
        Test.stopTest();
        
    }
}