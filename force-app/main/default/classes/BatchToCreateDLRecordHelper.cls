/*---------------------------------------------------------------------
 * Created By : Saurabh Gupta 
 * Created date : 02/02/2017 10:14
 * Last Modified by:: Saurabh Gupta ,  26/09/2017 06:23
 * Description: This class is the helper class of BatchToCreateDLRecord
   history : v1.0 - 31/10/2018 - Changes For Security Review
----------------------------------------------------------------------*/
public with sharing class BatchToCreateDLRecordHelper{
    public static void callBatch(Id recordId){
        
            //Initialization
            List<String> orderList = new List<String>();
            List<String> destinationList = new List<String>();
            
            //Iterate custom metadata records
            for(In_Form__DataBatchCallOrder__mdt obj : [Select DeveloperName,MasterLabel,In_Form__Orders__c     
                                                        FROM In_Form__DataBatchCallOrder__mdt ORDER BY In_Form__Orders__c  LIMIT 2000]){                                             
                orderList.add(obj.MasterLabel);                                                                
            }       
            
            if(!orderList.isEmpty()){           
               set<Id> ids = new set<Id>();
               if(!Test.isRunningTest()){
                   Database.executeBatch(new BatchToCreateDLRecord(orderList,recordId,'Processing',ids),1);   
               }         
            }
             
    }    
    
    //Return the connection name whose connectionStatus is accepted.
    public static Id getConnectionId(String connectionName) {
         //List<String> fieldList=new List<String>{'id'};
            
         //if(InForm_CRUDFLS_Util.getReadAccessCheck('PartnerNetworkConnection',fieldList)){
                List<PartnerNetworkConnection> partnerNetConList = [SELECT id From PartnerNetworkConnection WHERE connectionStatus = 'Accepted' AND connectionName =: String.escapeSingleQuotes(connectionName)];
                
                if ( partnerNetConList.size()!= 0 ) {
                    return partnerNetConList.get(0).Id;
                }
         //}          
         return null;
    }
}