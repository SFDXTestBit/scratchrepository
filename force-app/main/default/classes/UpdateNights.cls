/*------------------------------------------------------------------
/* Created By : 
 * Created date : 13/8/2015
 * Description: UpdateNights
------------------------------------------------------------------*/
public with sharing class UpdateNights{ 
    
    public Tenancy__c tenancy{get;set;}
    public Night__c night{get;set;}
    public Boolean isSuccess{get;set;}
    public UpdateNights(ApexPages.StandardController sc){
        tenancy = new Tenancy__c(Id=(ID)sc.getId());
        tenancy.End_date__c = date.today();
        night = new Night__c();
    }
   
    //Method to update Tenancy
    public PageReference save(){
        try{
            if(tenancy.Start_date__c>Date.today()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Start_Date_Error_Msg));
                return null;
            }
            if(tenancy.End_date__c > Date.today()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.End_Date_Error_Msg));
                return null;
            }
            if(tenancy.End_date__c < tenancy.Start_date__c){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.End_Date_Error_Msg1));
            }
            //Mayur Patil[26/10/2018]:v1.1 - Extentia - Enforcing CRUD/FLS
            //List<String> fieldList=new List<String>{'In_Form__Void_Reason__c','In_Form__Void_Status__c'};
            //&& InForm_CRUDFLS_Util.getUpdateAccessCheck('In_Form__Tenancy__c', fieldList)
            if(tenancy.End_date__c == Date.Today() ){
                Tenancy__c ten = new Tenancy__c(Id = tenancy.Id);
                ten.Void_Reason__c = night.Void_Reason__c;
                ten.Void_Status__c = night.Void_Status__c;
                update ten;
                tenancy.End_date__c = Date.Today()-1;
            }
            isSuccess = true; 
            Database.executeBatch(new UpdateVoidNightBatch(tenancy,night),1); 
              
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
            isSuccess = false; 
        }
    return null;
    }
}