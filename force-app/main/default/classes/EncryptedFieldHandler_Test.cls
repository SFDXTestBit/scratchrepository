@isTest 
private class EncryptedFieldHandler_Test  {
    //Mayur Patil - 30/10/2018: Securtiy Review
    static testMethod void encryptedFieldHandlerTest() {
        Test.startTest();
        try{
            In_Form__DL_Batch__c dl = new In_Form__DL_Batch__c(); 
            insert dl;
            
            System.AssertNotEquals(dl.Id,null);
            
            In_Form__DL_Client__c dlclient = new  In_Form__DL_Client__c ();
            dlclient.In_Form__DL_Batch_Name__c = dl.Id;
            
            dlclient.In_Form__Birth_Date_Encrypted__c ='Test birthdate';
            dlclient.In_Form__Full_Name_Encrypted__c ='TestName';
            dlclient.In_Form__NI_Number_Encrypted__c = 'Test Number';
            
            insert dlclient;
            
            System.AssertNotEquals(dlclient.Id,null);
            
            
            dlclient.In_Form__Birth_Date_Encrypted__c ='Test birth';
            dlclient.In_Form__Full_Name_Encrypted__c ='Test fname';
            dlclient.In_Form__NI_Number_Encrypted__c = 'Test no';
            
            update dlclient; 
            System.AssertEquals(dlclient.In_Form__Birth_Date_Encrypted__c,'Test birth');
            
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    }
    //Mayur Patil - 30/10/2018: Securtiy Review
    static testMethod void encryptedFieldTest(){
        Test.startTest();
        try{
            In_Form__DL_Batch__c dl = new In_Form__DL_Batch__c(); 
            insert dl;
            
            System.AssertNotEquals(dl.Id,null);
            
            In_Form__DL_Client__c dlclient = new  In_Form__DL_Client__c ();   
            dlclient.In_Form__DL_Batch_Name__c = dl.Id;     
            dlclient.In_Form__Birth_Date_Encrypted__c ='Test birthdate';
            dlclient.In_Form__Full_Name_Encrypted__c ='TestName';
            dlclient.In_Form__NI_Number_Encrypted__c = 'Test Number';      
            insert dlclient;
            
            System.AssertNotEquals(dlclient.Id,null);
            
            In_Form__DL_Client__c dlclient1 = new  In_Form__DL_Client__c ();
            dlclient1.In_Form__DL_Batch_Name__c = dl.Id;
            dlclient1.In_Form__Birth_Date_Encrypted__c ='Test birthdate1';
            dlclient1.In_Form__Full_Name_Encrypted__c ='TestName1';
            dlclient1.In_Form__NI_Number_Encrypted__c = 'Test Number1';        
            insert dlclient1;
            
            System.AssertEquals(dlclient1 .In_Form__Birth_Date_Encrypted__c,'Test birthdate1');
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    } 
}