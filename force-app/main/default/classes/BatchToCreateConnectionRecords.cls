/*------------------------------------------------------------------
 * Created By : Saurabh Gupta ,
 * Created date : 25/05/2017 13:44
 * Last Modified by:: Saurabh Gupta , 25/09/2017 05:26
 * Description: Batch class to create connection with other org.
 * History: v1.1 - Sneha U. - 26/10/2018 - Enforcing CRUD/FLS
 * History: v1.2 - Dinesh D. - 31/10/2018 - Added Try-Catch & Removed commmented code 
------------------------------------------------------------------*/
global class BatchToCreateConnectionRecords Implements Database.Batchable<Id> {
    
    global Set<Id> sharingIds;
    global Id networkId;
    global String fields;
    global String objName;
    global String dlscopeName;
    
    
    public BatchToCreateConnectionRecords(Set<Id> sharingIds){
        this.sharingIds = sharingIds;    
    }
    
    //Return sharing ids
    global List<Id> start(Database.BatchableContext BC){
        
        List<Id> shareIds = new List<Id>();
        shareIds.addAll(sharingIds);        
        return shareIds;
    
    }
    
    //Execute Method to create connection record
    global void execute(Database.BatchableContext BC, List<Id> scope){
        
        List<Error_Logs__c> errorLogList = new List<Error_Logs__c>();
          for(id obj: scope){
                Error_Logs__c error1 = new Error_Logs__c(Description__c = obj);     
                errorLogList.add(error1);
            }
                    
            if(!errorLogList.isEmpty()) {
                insert errorLogList;
            }
                        
            List<PartnerNetworkRecordConnection> connectionsList =  new  List<PartnerNetworkRecordConnection>();
            
            //List of fields assigned for PartnerNetworkRecordConnection Object create access
            /*List<String> fieldList = new List<String>{
                'ConnectionId',
                'LocalRecordId',
                'SendClosedTasks',
                'SendOpenTasks',
                'SendEmails'
            };*/ 
            
            //Check create access for PartnerNetworkRecordConnection and its fields 
            //if(InForm_CRUDFLS_Util.getCreateAccessCheck('PartnerNetworkRecordConnection',fieldList)){

                for(id obj: scope){
                    
                    PartnerNetworkRecordConnection newConnection =
                        new PartnerNetworkRecordConnection(
                            ConnectionId = networkId,
                            LocalRecordId = obj,
                            SendClosedTasks = false,
                            SendOpenTasks = false,
                            SendEmails = false);
                    
                    connectionsList.add(newConnection);
                }
            //}
                    
            if(!connectionsList.isEmpty()){
                 
                List<Database.SaveResult> connectionsListresult = Database.insert(connectionsList,false);
                for (Database.SaveResult sr : connectionsListresult ) {
                        
                    if (sr.isSuccess()) {
                        // This condition will be executed for successful records and will fetch the ids of successful records
                        System.debug('Successfully updated . Record ID is : ' + sr.getId());
                    }
                    else {
                        errorLogList = new List<Error_Logs__c>();
                        // This condition will be executed for failed records
                        for(Database.Error objErr : sr.getErrors()) {
                            Error_Logs__c error = new Error_Logs__c(Description__c = objErr.getMessage());                   
                            //insert error;
                            errorLogList.add(error);         
                        }                        
                        if(!errorLogList.isEmpty()){
                            insert errorLogList;
                        }                        
                    }
                }            
            }
        
    }
    
    global void finish(Database.BatchableContext BC){}

}