//Modified for Security Review
@isTest 
private class FieldMappingCntrl_Test  {

    static testMethod void fieldMappingCntrlTest() {   
        try{
            Account acc = new Account(Name = 'Test1',SP_contact_telephone_number__c = '01403 567890');
            insert acc;
                   
            User user = new User(ProfileId = [SELECT Id FROM Profile WHERE Name='Standard User'].Id,
                        Alias = 'testu', Email='test1@test.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US',
                        TimeZoneSidKey='America/Los_Angeles', UserName= System.now().millisecond() + 'test1@test.com');                   
            insert user;
            
           String strRecordTypeId = [Select Id From RecordType Where SobjectType = 'In_Form__Agency__c' and Name = 'Support Agency'].Id;
           
          
            
           In_Form__Agency__c formAgency =new In_Form__Agency__c();
           
           formAgency.Name='test';
           formAgency.RecordTypeId = strRecordTypeId;
           formAgency.In_Form__County__c='testcountry';
           formAgency.In_Form__Email__c='test@test.com';
           formAgency.In_Form__Notes__c='test';
           formAgency.In_Form__Phone__c='73462873';
           formAgency.In_Form__Postcode__c='EC1A 1BB';
           formAgency.In_Form__Street__c='teststreet';
           formAgency.In_Form__Street_2__c='teststreet2';
           formAgency.In_Form__Town_City__c='testcity';
           formAgency.In_Form__Type__c='Benefits';
           formAgency.In_Form__Website__c='testts';
           
           insert formAgency;
           
           System.AssertNotEquals(formAgency.Id, null);
           
            String demo = formAgency.Id;
            
            
            List<In_Form__DL_Field_Mapping__c > dlFieldMapList = new List<In_Form__DL_Field_Mapping__c >();
            In_Form__DL_Field_Mapping__c  dlFieldMap = new In_Form__DL_Field_Mapping__c ();
            dlFieldMap.In_Form__Description__c     ='Test Description';
            dlFieldMap.In_Form__Mapping_Status__c  ='Test Mapping Status';
            dlFieldMap.In_Form__Object_Field_Name__c ='Test Object field Name';
            dlFieldMap.In_Form__Object_Field_Type__c ='Picklist';
            dlFieldMap.In_Form__Object_Name__c       ='Test Object Name';
            dlFieldMap.In_Form__Source_Field_Label__c ='Test source Field';
            dlFieldMap.In_Form__Source_Field_Type__c ='Picklist';
            dlFieldMap.In_Form__Source_Object__c = 'Test Source Object';
            dlFieldMap.In_Form__Source_Field__c = 'Test Field';   
            
            dlFieldMapList.add(dlFieldMap);
            insert dlFieldMapList;
            
            System.AssertNotEquals(dlFieldMapList.size(),0) ;
            
            FieldMappingCntrl.getFieldMappings();
            FieldMappingCntrl.getObjectName();
            FieldMappingCntrl.savedFieldMapping(dlFieldMapList);
            FieldMappingCntrl.getSObjectName();
            
            System.AssertEquals(0, FieldMappingCntrl.getFieldMappings().size());
            System.AssertEquals(0,FieldMappingCntrl.getObjectName().size());
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    } 
    
}