/*------------------------------------------------------------------
/* Created By : 
 * Created date : 14/8/2015
 * Description: Rent Charge Split class
 * History: v1.1 - Sneha U. - 26/10/2018 - Enforcing CRUD/FLS
------------------------------------------------------------------*/

public with sharing class SplitRentCharge {

    public Map<String,double> amountMap{get;set;}
    public string strId;
    public string tenancyId;
    public Rent_Schedule__c rsc;
    public Boolean close{get;set;}
    
    
    public SplitRentCharge(ApexPages.StandardController controller) {
        
            close = false;
            rsc = (Rent_Schedule__c) controller.getRecord();
            strId = ApexPages.currentPage().getParameters().get('bid');
            tenancyId = ApexPages.currentPage().getParameters().get('tenancyId');
            amountMap = new Map<String,double>();
            getTypes();

            //List of fields assigned for Rent_Schedule__c Object create access
            /*List<String> fieldList = new List<String>{
                'In_Form__End_Date__c',
                'In_Form__Tenancy__c',
                'In_Form__Total_Rent_Charge__c'
            }; */

            //Check create access for Rent_Schedule__c and its fields 
            //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Rent_Schedule__c',fieldList)){ 
                rsc.End_Date__c = getFiscalLastDate();
                if(strId != null){
                    List<Rent_Schedule__c > rsList = [SELECT Id,Total_Rent_Charge__c,Tenancy__c FROM Rent_Schedule__c WHERE Bedspace__c =:strId AND Start_Date__c <=: Date.Today() AND End_Date__c >=: Date.Today() ORDER BY createddate DESC LIMIT 1];            
                    
                    if(rsList.size()>0 ){ 
                        rsc.Tenancy__c = tenancyId;
                        rsc.Total_Rent_Charge__c=rsList[0].Total_Rent_Charge__c;
                    }
                               
                }
            //}
        
    }
    
    //Method sets available types of rent stream
    public void getTypes(){    
        List<String> options = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Rent_Stream_Charge__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
            amountMap.put(f.getValue(),0);
        }       
    }
    
    //Method to split amount and save.
    public void save(){
        Savepoint sp = Database.setSavepoint();
        try{
            if(rsc.Start_Date__c <= Date.Today()){
                Decimal val=0;
                for(String str : amountMap.keySet()){                    
                    val= val+amountMap.get(str);
                    System.debug('##VAl'+val);
                }
                if(val == rsc.Total_rent_charge__c){
                    
                    //Check create access for Rent_Schedule__c and its fields 
                    //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Rent_Schedule__c',new List<String>{'In_Form__Bedspace__c'})){ 
                        rsc.Bedspace__c = strId;
                        INSERT rsc;
                    //}

                    List<Rent_Stream_Charge__c> rsList = new List<Rent_Stream_Charge__c>();
                    
                    //List of fields assigned for In_Form__Rent_Stream_Charge__c Object create access
                    /*List<String> fieldList = new List<String>{
                        'In_Form__Rent_Schedule__c',
                        'In_Form__Amount__c',
                        'In_Form__Type__c'
                    }; */

                    //Check create access for In_Form__Rent_Stream_Charge__c and its fields 
                    //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Rent_Stream_Charge__c',fieldList)){   

                        for(String str : amountMap.keySet()){
                            Rent_Stream_Charge__c rsObject = new Rent_Stream_Charge__c();
                            rsObject.Rent_Schedule__c = rsc.Id;
                            rsObject.Amount__c = amountMap.get(str);
                            rsObject.Type__c = str;
                            rsList.add(rsObject);    
                        }
                    //}

                    if(!rsList.isEmpty()){
                        Insert rsList; 
                    }
                    
                    close = true;
                    ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Info,'Records created succesfully'));
                }
                else{
                    ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Amount split does not match Total amount'));
                }
            }
            else{
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Start Date can not be a future date'));
            }
        }
         catch(DMLException ex){
            Database.rollback(sp);
        }
        catch(Exception ex){
            Database.rollback(sp); ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Rent split can not be created. Please contact administrator'));
        }
    }
    
    //Method returns last date of fiscal year
    private date getFiscalLastDate(){
        try{
            Integer currentFY;
            Integer currentFYMonth;
            Integer CurrentFYDays;
            Date today;
            Date FYStartDate;
            Date FYEndDate;

            Organization orgInfo = [SELECT FiscalYearStartMonth, UsesStartDateAsFiscalYearName
                                    FROM Organization
                                    WHERE id=:Userinfo.getOrganizationId()];
                                    
            today = system.today();
            currentFYMonth = orgInfo.FiscalYearStartMonth;

            if (today.month() >= orgInfo.FiscalYearStartMonth) {
                if (orgInfo.UsesStartDateAsFiscalYearName) {
                    currentFY = today.year();
                } 
                else {
                currentFY = today.year() + 1;
                }
            } 
            else {
                if (orgInfo.UsesStartDateAsFiscalYearName) {
                        currentFY = today.year() - 1;
                }
                else {
                    currentFY = today.year();
                }
            }
                    
            CurrentFYDays = date.daysInMonth(currentFY, currentFYMonth);

            FYStartDate = date.parse(CurrentFYMonth + '/' + '01' + '/' + currentFY);
            System.debug('===FYStartDate '+FYStartDate );
            FYStartDate = FYStartDate.addYears(-1);
            System.debug('===FYStartDate '+FYStartDate );
            FYEndDate = FYStartDate.addYears(1).addDays(-1);
            return FYEndDate;
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
            return null;
        }
        return null;
    }
}