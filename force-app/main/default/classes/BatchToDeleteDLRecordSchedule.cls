/*------------------------------------------------------------------
/* Created By : Dev Team Phiz
 * Created date : 14/07/2017 14:36
 * Last Modified by:: Dev Team Phiz ,  26/09/2017 11:51
 * Description: Batch class to Schedule BatchToDeleteDLRecordHelper
------------------------------------------------------------------*/
global class BatchToDeleteDLRecordSchedule implements schedulable{
    global void execute(SchedulableContext sc){
        BatchToDeleteDLRecordHelper b = new BatchToDeleteDLRecordHelper(); 
        database.executebatch(b);
    }
}