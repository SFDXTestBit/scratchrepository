/*------------------------------------------------------------------
/* Created By : Saurabh Gupta 
 * Created date :  31/01/2017 12:58
 * Last Modified by:: Saurabh Gupta ,  27/09/2017 11:42
 * Description: Class to Encrypt data
   history : v1.0 - 31/10/2018 - Changes For Security Review
------------------------------------------------------------------*/
Public Class EncryptedFieldHandler{
    //Encrypt data on insert
    public static void encryptDataOnInsert(List<In_Form__DL_Client__c> objList){    
    
    
        //False Positive
        //Initialize Blob
        Blob cryptoKey = Blob.valueOf('380db410e8b11fa9');
        // Encrypt infrm__Full_Name_Encrypted__c 
        for(In_Form__DL_Client__c  o : objList){
             //Encrypt infrm__NI_Number_Encrypted__c
             if(o.In_Form__Full_Name_Encrypted__c <> NULL){
                 Blob dataName = Blob.valueOf(o.In_Form__Full_Name_Encrypted__c);
                 Blob encryptedData = Crypto.encryptWithManagedIV('AES128', cryptoKey , dataName);
                 String b64Data = EncodingUtil.base64Encode(encryptedData);
                 o.In_Form__Full_Name_Encrypted__c = b64Data ;
             }
             
             //Encrypt infrm__NI_Number_Encrypted__c                
             if(o.In_Form__NI_Number_Encrypted__c  <> NULL){
                 Blob dataNiN = Blob.valueOf(o.In_Form__NI_Number_Encrypted__c );
                 Blob encryptedNiN = Crypto.encryptWithManagedIV('AES128', cryptoKey , dataNiN );
                 String b64DataNiN = EncodingUtil.base64Encode(encryptedNiN);
                 o.In_Form__NI_Number_Encrypted__c = b64DataNiN ;      
             }
             
             //Encrypt infrm__Birth_Date_Encrypted__c 
             if(o.In_Form__Birth_Date_Encrypted__c <> NULL){
                 Blob dataDoB  = Blob.valueOf(o.In_Form__Birth_Date_Encrypted__c );
                 Blob encryptedDoB = Crypto.encryptWithManagedIV('AES128', cryptoKey , dataDoB );
                 String b64Datadob = EncodingUtil.base64Encode(encryptedDoB);
                 o.In_Form__Birth_Date_Encrypted__c = b64Datadob ;
             }
        }
    }
    
    //Encrypt data on update
    public static void encryptDataOnUpdate(Map<id,In_Form__DL_Client__c > objectMap , Map<id,In_Form__DL_Client__c > oldMap){    
  
         //False Positive
        Blob cryptoKey = Blob.valueOf('380db410e8b11fa9');

        for(In_Form__DL_Client__c  o : objectMap.values()){
            //Encrypt infrm__Full_Name_Encrypted__c 
            if(o.In_Form__Full_Name_Encrypted__c != oldMap.get(o.id).In_Form__Full_Name_Encrypted__c){
                 Blob dataName = Blob.valueOf(o.In_Form__Full_Name_Encrypted__c);
                 Blob encryptedData = Crypto.encryptWithManagedIV('AES128', cryptoKey , dataName);
                 String b64Data = EncodingUtil.base64Encode(encryptedData);
                 o.In_Form__Full_Name_Encrypted__c = b64Data ;
            }
            
            //Encrypt infrm__NI_Number_Encrypted__c 
            if(o.In_Form__NI_Number_Encrypted__c != oldMap.get(o.id).In_Form__NI_Number_Encrypted__c ){
                Blob dataNiN = Blob.valueOf(o.In_Form__NI_Number_Encrypted__c );
                Blob encryptedNiN = Crypto.encryptWithManagedIV('AES128', cryptoKey , dataNiN );
                String b64DataNiN = EncodingUtil.base64Encode(encryptedNiN);
                o.In_Form__NI_Number_Encrypted__c = b64DataNiN ;      
            }
            
            //Encrypt infrm__Birth_Date_Encrypted__c 
            if(o.In_Form__Birth_Date_Encrypted__c != oldMap.get(o.id).In_Form__Birth_Date_Encrypted__c ){
                Blob dataDoB  = Blob.valueOf(o.In_Form__Birth_Date_Encrypted__c );
                Blob encryptedDoB = Crypto.encryptWithManagedIV('AES128', cryptoKey , dataDoB );
                String b64Datadob = EncodingUtil.base64Encode(encryptedDoB);
                o.In_Form__Birth_Date_Encrypted__c = b64Datadob ;
            }    
        }
    }      
    
}