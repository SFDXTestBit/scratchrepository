/*------------------------------------------------------------------
/* Created By : 
 * Created date : 11/8/2015
 * Description: Batch class to create transaction on creation of tenancy
 * History: v1.1 - Bhargav Shah. - 31/10/2018 - Enforcing CRUD/FLS
------------------------------------------------------------------*/
global with sharing class AutomaticTransactionCreationBatch implements Database.Batchable<sObject>,Database.Stateful{
    
    global List<Tenancy__c> tenancies;
    
    global AutomaticTransactionCreationBatch(){
        tenancies = new List<Tenancy__c>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
            String status = 'Current';
            Date today = System.Today();
            Id rentChargeId = Schema.SObjectType.Transaction__c.getRecordTypeInfosByName().get('Rent charge').getRecordTypeId();
            //List<String> tenancyFieldsList = new List<String>{'Id','Name','In_Form__Bedspace__r.In_Form__No_financial_data__c','In_Form__Bedspace__c','In_Form__Start_Date__c','In_Form__Status__c'};
            //List<String> transactionsFieldsList = new List<String>{'In_Form__Transaction_date__c','In_Form__Bedspace__c'};
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',tenancyFieldsList)){
                return Database.getQueryLocator('SELECT Id, Name,Bedspace__r.No_financial_data__c, Bedspace__c, Start_Date__c,(SELECT Id FROM Transactions__r WHERE Day_Only(convertTimezone(Transaction_date__c))=: today AND RecordTypeId =: rentChargeId LIMIT 1) FROM Tenancy__c where Bedspace__c != null And Status__c =:status');
            //}
        
        return null;
    }
    
    global void execute(Database.BatchableContext BC, List<Tenancy__c> scope){
        try{
            List<Tenancy__c> tenancyList = new List<Tenancy__c>();
            for(Tenancy__c tenancy : scope){
                if(tenancy.Transactions__r.size() == 0){
                    tenancyList.add(tenancy);
                }
            }
            
            AutomaticTransactionCreationUtility.createTransactionforTenancies(tenancyList);
            
            tenancies.addAll(scope);
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
        try{
            Set<Id> tenancyIds = new Set<Id>();
            
            for(Tenancy__c tenancy : tenancies){
                tenancyIds.add(tenancy.Id);
            }
            //List<String> transactionsFieldsList = new List<String>{'In_Form__Transaction_Date__c','In_Form__Tenancy__c'};
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction__c',transactionsFieldsList)){
                Date startDate = Date.today();

                for(Transaction__c trans : [SELECT Transaction_Date__c FROM Transaction__c WHERE Tenancy__c IN: tenancyIds ORDER BY Transaction_Date__c LIMIT 1]){
                    startDate = trans.Transaction_date__c.date();
                }

                Database.executeBatch(new TransactionTotalUpdationBatch(tenancyIds, startDate),5);
           // }    
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
}