/*------------------------------------------------------------------
/* Created By : 
 * Created date : 19/8/2015
 * Modified Date : 30-10-2018 By Bhargav Shah
 * Description: Schedule transaction test
------------------------------------------------------------------*/
@isTest
public class ScheduleTransaction_Test {
    //Test data setup
    @TestSetup
    public static void setupData(){
        try{
            Transaction_Type__c dailytype = new Transaction_Type__c(Daily_Amount__c = true,Is_Active__c=true,Type__c = 'Daily',Name = 'DailyType');
            INSERT dailyType;
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            Map<String,Id> rtMap = new Map<String,Id>();
            if(!rtList.isEmpty()){
                for(RecordType rt: rtList)
                rtMap.put(rt.Name,rt.Id);
            }
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            INSERT oBedspace;
            
            Account acc = new Account(name='TestAccount',SP_contact_telephone_number__c = '01403 567890');
            INSERT acc;
            
            Contact con = new Contact(Lastname='TestName',FirstName='TestFname',AccountId=acc.Id);
            INSERT con;
            
            Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Bedspace__c =oBedspace.Id,Total_Rent_Charge__c=100,Start_date__c=Date.Today().addDays(-4),End_date__c=Date.Today().addDays(4),In_Form__Is_default__c=true);
            INSERT oRentSchedule;
            
            Rent_Stream_Charge__c oRentCharge = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=50);
            INSERT oRentCharge;
            
            Rent_Stream_Charge__c oRentCharge1 = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=100);
            INSERT oRentCharge1;
            
            Tenancy__c ten1 = new Tenancy__c();
            ten1.recordTypeid = rtMap.get('Tenancy');
            ten1.Client__c = con.Id;
            ten1.Start_date__c=Date.Today().addDays(-364);
            ten1.Status__c='Current';
            ten1.Bedspace__c=oBedspace.Id;
            INSERT ten1;
            Test.StartTest();
                AutomaticTransactionCreationUtility.createTransactionforTenancies(new List<Tenancy__c>{ten1});
            Test.StopTest();
            Transaction__c tran1 = new Transaction__c(Tenancy__c = ten1.Id,Amount__c=100,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-1));
            INSERT tran1;
    
            List<Transaction__c> transUpList = [SELECT Id, Name FROM Transaction__c WHERE Tenancy__c =: ten1.Id];
            System.assertNotEquals(null, tran1.Id,'SUCCESS');
         }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
         }
    }  
    
    //Test method to schedule transaction creation for Tenancy
    public static testmethod void ScheduleTransactionCreation(){
        try{
            Test.startTest();
                String CRON_EXP = '0 0 0 1 1 ? 2025';  
                String jobId = System.schedule('ScheduledApex', CRON_EXP, new scheduleTransactionCreation() );
                CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
                System.assertEquals(CRON_EXP, ct.CronExpression); 
                System.assertEquals(0, ct.TimesTriggered);
                System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));
            Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }     
    }
    
}