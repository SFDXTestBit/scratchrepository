/* Class Name  : TransactionDeleteBatch
* Dinesh D.                31-Oct-2018               Enforcing FLS, Try-Catch 
* Extentia 				   10-June-2019				add the  Isbulk condition when inserting the transactions.
*****************************************************************************************/ 
global with sharing class TransactionUpdateBatch implements Database.Batchable<sObject>, Database.Stateful{
    Map<Id,List<Date>> tenancyMapBatch = new Map<Id,List<Date>>();
    Map<String,List<Rent_Stream_Charge__c>> rsChargeMap;
    Set<String> typeList;
    Map<Id,Tenancy__c> tenancies;
    global Map<String,Double> amtTypeMap;
    boolean weeklyAmount;
     boolean isBulk ;  // added by extentia.
    
    
    global TransactionUpdateBatch(Map<Id,List<Date>> mapTenancy){
        tenancyMapBatch = mapTenancy;
        rsChargeMap = new Map<String,List<Rent_Stream_Charge__c>>();
        amtTypeMap = new Map<String,Double>();
        typeList = new Set<String>();
        weeklyAmount = false;
    }
    
    global TransactionUpdateBatch(Map<Id,List<Date>> mapTenancy,Boolean isBulk){  // added by extentia.
        tenancyMapBatch = mapTenancy;
        rsChargeMap = new Map<String,List<Rent_Stream_Charge__c>>();
        amtTypeMap = new Map<String,Double>();
        typeList = new Set<String>();
        weeklyAmount = false;
         this.isBulk = isBulk; 
    }
    
    
    /* Method Name : start
* Description : Batch Class execute Method to select transaction
* Return Type : List of Transactions - List<Transaction__c>
* Input Parameter : Database.BatchableContext
*/
    global List<Transaction__c> start(Database.BatchableContext BC){
        
            Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
            
            if(orgDefaults.Turn_off_Rent_Module_Automation__c || orgDefaults.Turn_off_Transaction_on_Tenancy_Change__c){
                return null;
            }
            
            /*List<String> fieldList = new List<String>{
                'Id',
                'Name',
                'In_Form__Start_date__c',       
                'In_Form__End_date__c',
                'In_Form__Status__c',
                'In_Form__Bedspace__c',
                'In_Form__Bedspace__r.In_Form__No_financial_data__c'    
             };*/
              //InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',fieldList) &&       
            if(!tenancyMapBatch.isEmpty() ){
                
                
                tenancies = new Map<Id,Tenancy__c>([SELECT Id, Name,Start_date__c,End_date__c,Status__c,Bedspace__c,Bedspace__r.In_Form__No_financial_data__c FROM Tenancy__c 
                                                    WHERE Id IN: tenancyMapBatch.keySet()]);
                
                List<Transaction__c> transactions = new List<Transaction__c>();
                List<Id> bedspaceIds = new List<Id>();
                List<DateTime> tranDate = new List<DateTime>();
                Map<Id,Set<Date>> mapDate = new Map<Id,Set<Date>>();
                map<id,boolean> mapBedspace=new map<id,boolean>(); // Added by Ajay on 06/05/2018
                Id tranRecType = Schema.SObjectType.Transaction__c.getRecordTypeInfosByName().get('Rent charge').getRecordTypeId();
                
                for(Tenancy__c tenancy : tenancies.values()){
                    if(tenancy.Bedspace__c != null){
                        bedspaceIds.add(tenancy.Bedspace__c);
                        mapBedspace.put(tenancy.id,tenancy.Bedspace__r.No_financial_data__c); // Added by Ajay on 06/05/2018
                    }
                }
                //Call Helper class to get rent Charges 
                
                rsChargeMap=transactionUtility.getRentCharges(bedspaceIds,typeList,false);
                
                //Call Helper class to get list of Transactions
                
                transactions=transactionUtility.createTransaction(tenancies.values(),mapBedspace,weeklyAmount);
                
                return transactions;
            }
            //else{
             //   return null;
           // }
        
        return null;
    }
    /* Method Name : execute
* Description : Batch Class execute Method to insert transactions based upon Start and end date
* Return Type : void
* Input Parameter : Database.BatchableContext, List of Transaction - List<Transaction__c>
*/
    global void execute(Database.BatchableContext BC, List<Transaction__c> scope){
        
        List<Transaction__c> transactions = new List<Transaction__c>();
        
            //Call helper class method to update the Transactions Amount
            transactions=transactionUtility.updateTransaction(scope,typeList,rsChargeMap,tenancies,weeklyAmount);
            systemBatchHelper.isBatchRunning=true;
        
            if(!transactions.isEmpty() && isBulk == false){  //  added by extentia.
                systemBatchHelper.isBatchRunning=true;
                insert transactions;
            }
        
    }
    /* Method Name : finish
* Description : this method calls a batch to update Balance on Transactions
* Return Type : void
* Input Parameter : Database.BatchableContext
*/
    global void finish(Database.BatchableContext BC){
        Date startDate = Date.today();
                    
            //List of fields assigned for Transaction__c Object read access
            /*List<String> fieldList = new List<String>{
                'In_Form__Transaction_Date__c'
             };*/                        
                 //Check read access for Transaction__c Object
                 //InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction__c',fieldList) &&
                 if( !tenancyMapBatch.isEmpty()){                         
                     for(Transaction__c trans : [SELECT Transaction_Date__c FROM Transaction__c WHERE Tenancy__c IN: tenancyMapBatch.keySet() ORDER BY Transaction_Date__c LIMIT 1]){
                         startDate = trans.Transaction_date__c.date();
                     }                     
                     Database.executeBatch(new TransactionTotalUpdationBatch(tenancyMapBatch.keySet(), startDate),5); 
                 }
        
    }
}