/* -----------------------------------------------------------------
   Created By : Ajay Khatri
*  Created date : 06/25/2018
*  Modified Date: 06/26/2018
*  Description: Helper Class for Transactions of Tenancy
* History: v1.1 - Sneha U. - 26/10/2018 - Enforcing CRUD/FLS
           v1.2 - 31/10/2018 - Changes For Security Review
------------------------------------------------------------------ */

public with sharing class transactionUtility{
    
    //Method to create Transaction on Tenancy Update and Create
    public static list<In_Form__Transaction__c> createTransaction(list<In_Form__Tenancy__c> tenancyList, map<id,boolean> mapBedspace,Boolean weeklyAmount){
        Map<Id,Set<Date>> mapDate = new Map<Id,Set<Date>>();
        List<DateTime> tranDate = new List<DateTime>();
        Id tranRecType = Schema.SObjectType.In_Form__Transaction__c.getRecordTypeInfosByName().get('Rent charge').getRecordTypeId();
        List<In_Form__Transaction__c> transactions = new List<In_Form__Transaction__c>();
        Integer tranPeriod;
        Integer noOfDays;
        String weekday;
        List<In_Form__Transaction_Type__c> tranTypeSetting = new List<In_Form__Transaction_Type__c>();      
        Date endDate;
        Date ExactEndDate;
        
        try
        {
        
            //List<String> fieldList=new List<String>{'In_Form__Type__c','In_Form__Switch_off_all_financial_data__c','In_Form__Daily_Amount__c','In_Form__Day_of_Week__c'};
            
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction_Type__c',fieldList)){
                tranTypeSetting = [SELECT Type__c,Switch_off_all_financial_data__c, Daily_Amount__c,Day_of_Week__c FROM Transaction_Type__c WHERE Is_Active__c =: true LIMIT 1];
            //}
            
            if(tranTypeSetting.isempty()==false)
            for(Tenancy__c tenancy : tenancyList){
                if(tenancy.Start_date__c <= Date.Today() && tranTypeSetting[0].Switch_off_all_financial_data__c==false && mapBedspace.get(tenancy.id)==false  /* Added by Ajay on 06/04/2018 */){
                    Set<Date> dateSet = new Set<Date>();
                    
                    if(tenancy.In_Form__End_date__c == null){
                        endDate = Date.today();
                    }else{
                        endDate = tenancy.End_date__c.addDays(-1);
                    }
                    
                    Integer endDateint=1;
                    Time t = Time.newInstance(DateTime.now().hour(), DateTime.now().minute(), DateTime.now().second(),DateTime.now().millisecond());
                    if(!tranTypeSetting.isEmpty()){
                        if(tranTypeSetting[0].In_Form__Type__c =='Daily'){
                            tranPeriod = 1;
                            noOfDays = 0;
                            if(!tranTypeSetting[0].In_Form__Daily_Amount__c)
                                weeklyAmount = true;
                        }  
                        else if(tranTypeSetting[0].In_Form__Type__c =='Weekly'){
                            tranPeriod = 7;
                            weekday = tranTypeSetting[0].In_Form__Day_of_Week__c;
                            Datetime dateT = DateTime.newInstance(tenancy.In_Form__Start_date__c, Time.newInstance(2, 0, 0, 0));
                            for(INTEGER i =0 ; i<7 ; i++){
                                if(weekday == dateT.format('EEEE')){
                                    noOfDays = -i;
                                    break;
                                }
                                else{
                                    dateT = dateT.addDays(-1);
                                }
                            }
                        }
                        else if(tranTypeSetting[0].In_Form__Type__c == 'Weekly - pro rata'){
                            tranPeriod = 7;
                            weekday = tranTypeSetting[0].In_Form__Day_of_Week__c;
                            Datetime dateT = DateTime.newInstance(tenancy.In_Form__Start_date__c, Time.newInstance(2, 0, 0, 0));
                            for(INTEGER i =0 ; i<7 ; i++){
                                if(weekday == dateT.format('EEEE')){
                                    noOfDays = i;
                                    break;
                                }
                                else{
                                    dateT = dateT.addDays(1);
                                }
                            }
                            
                            
                            Boolean inSameWeek = false;
                            if( tenancy.In_Form__Start_date__c.addDays(noOfDays) >endDate && tenancy.In_Form__End_date__c != null){
                                inSameWeek = true;
                                noOfDays = tenancy.In_Form__Start_date__c.daysBetween(endDate.addDays(1));
                            }
                            
                            if(noOfDays != 0){
                                In_Form__Transaction__c trans = new In_Form__Transaction__c();
                                trans.In_Form__Tenancy__c = tenancy.Id;
                                trans.In_Form__Transaction_date__c = tenancy.In_Form__Start_date__c;
                                trans.RecordTypeId = tranRecType;
                                trans.In_Form__Amount__c = noOfDays/7.0;
                                transactions.add(trans);
                            }                        
                                
                            if(tenancy.In_Form__End_date__c <>null){
                                ExactEndDate= tenancy.In_Form__End_date__c;
                            }
                            if(ExactEndDate != null && !inSameWeek ){
                                endDateint = 0;
                                Datetime dateEndT =  DateTime.newInstance(ExactEndDate, Time.newInstance(2, 0, 0, 0));
                                for(INTEGER i =0 ; i<7 ; i++){
                                    if(weekday == dateEndT.format('EEEE')){
                                        endDateInt = -i;
                                        break;
                                    }
                                    else{
                                        dateEndT = dateEndT.addDays(-1);
                                    }
                                }
                                if(endDateint != 0){
                                    In_Form__Transaction__c trans = new In_Form__Transaction__c();
                                    trans.In_Form__Tenancy__c = tenancy.Id;
                                    trans.In_Form__Transaction_date__c = ExactEndDate.adddays(endDateInt );
                                    trans.RecordTypeId = tranRecType;
                                    trans.In_Form__Amount__c = -endDateint/7.0;
                                    transactions.add(trans);
                                }
                            }  
                        }
                       
                        if(tenancy.In_Form__Start_date__c<>null && EndDate<>null){
                            for(Date dt=tenancy.In_Form__Start_date__c.addDays(noOfDays); dt <= EndDate.adddays(endDateint-1); dt=dt.addDays(tranPeriod)){                    
                                dateSet.add(dt);
                            }
                        }
                        
                        mapDate.put(tenancy.id,dateSet);
                    }
                }
           }
           for(In_Form__Transaction__c tran: [SELECT Id,In_Form__Tenancy__c,In_Form__Transaction_Date__c FROM In_Form__Transaction__c WHERE In_Form__Tenancy__c IN : mapDate.Keyset() AND RecordType.Name =: 'Rent charge']){
                if(mapDate.containsKey(tran.In_Form__Tenancy__c)){
                    Set<Date> dateSet = new Set<Date>();
                    dateSet = mapDate.get(tran.In_Form__Tenancy__c);
                    //Date dt = date.newinstance(tran.In_Form__Transaction_Date__c.year(), tran.In_Form__Transaction_Date__c.month(), tran.Transaction_Date__c.day());
                    /*if(dateSet.contains(dt)){
                        dateSet.remove(dt);
                        mapDate.put(tran.In_Form__Tenancy__c,dateSet);
                    }*/
                    
                    if(dateSet.contains(tran.In_Form__Transaction_Date__c.date())){
                        dateSet.remove(tran.In_Form__Transaction_Date__c.date());
                        mapDate.put(tran.In_Form__Tenancy__c,dateSet);
                    }
                }
            }
            Time t = Time.newInstance(DateTime.now().hour(), DateTime.now().minute(), DateTime.now().second(),DateTime.now().millisecond());
            for(Id tenancy : mapDate.keySet()){
                for(Date dt : mapDate.get(tenancy)){
                    In_Form__Transaction__c trans = new In_Form__Transaction__c();
                    trans.In_Form__Tenancy__c = tenancy;
                    trans.In_Form__Transaction_date__c = DateTime.newInstance(dt,t);
                    trans.RecordTypeId = tranRecType;
                    trans.In_Form__Amount__c = 1;
                    
                    transactions.add(trans);
                }
            }
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
        
        return transactions;
    }
    //Method to get the Exact Amount 
    public static list<In_Form__Transaction__c> updateTransaction(list<In_Form__Transaction__c> transactionList,Set<String> typeList,Map<String,List<In_Form__Rent_Stream_Charge__c>> rsChargeMap,Map<Id,In_Form__Tenancy__c> tenancies,boolean weeklyAmount){
        List<In_Form__Transaction__c> transactions = new List<In_Form__Transaction__c>();

        try
        {
            //List of fields assigned for In_Form__Transaction__c Object
            /*List<String> fieldList = new List<String>{
                'In_Form__Flag_Default__c',
                'In_Form__Type__c'
            };*/

            for(String oType : typeList){
                for(In_Form__Transaction__c trans : transactionList){
                    if(rsChargeMap.containsKey(tenancies.get(trans.In_Form__Tenancy__c).In_Form__Bedspace__c+oType)){                
                        List<In_Form__Rent_Stream_Charge__c> lsRentStream = rsChargeMap.get(tenancies.get(trans.In_Form__Tenancy__c).In_Form__Bedspace__c+oType);
                        for(In_Form__Rent_Stream_Charge__c rsL: lsRentStream){
                            if(rsL.In_Form__Rent_Schedule__r.In_Form__Start_Date__c <= trans.In_Form__Transaction_Date__c && rsL.In_Form__Rent_Schedule__r.In_Form__End_Date__c >= trans.In_Form__Transaction_Date__c && rsL.In_Form__Rent_Schedule__r.In_Form__Tenancy__c == trans.In_Form__Tenancy__c){
                                In_Form__Transaction__c newTrans = trans.clone(false,true);
                                newTrans=getTransaction(newTrans,weeklyAmount,trans.In_Form__Amount__c,rsL.In_Form__Amount__c,oType,rsL.Id);
                                if(newTrans!=null){
                                    transactions.add(newTrans);
                                }
                                //if(InForm_CRUDFLS_Util.getCreateAccessCheck(String.valueOf(trans.getSobjectType()),fieldList)){
                                    trans.In_Form__Flag_Default__c=true;   
                                    trans.In_Form__Type__c = oType;
                                //}
                                break;
                            }
                        }
                    }
                    
                    if(rsChargeMap.containsKey(tenancies.get(trans.In_Form__Tenancy__c).Bedspace__c+oType) && (((!trans.In_Form__Flag_Default__c) && trans.In_Form__Type__c == oType) || trans.In_Form__Type__c == null)){                
                        List<In_Form__Rent_Stream_Charge__c> lsRentStream = rsChargeMap.get(tenancies.get(trans.In_Form__Tenancy__c).Bedspace__c+oType);
                        for(In_Form__Rent_Stream_Charge__c rsL: lsRentStream){
                            if(rsL.In_Form__Rent_Schedule__r.In_Form__Start_Date__c <= trans.In_Form__Transaction_Date__c && rsL.In_Form__Rent_Schedule__r.In_Form__End_Date__c >= trans.In_Form__Transaction_Date__c && rsL.In_Form__Rent_Schedule__r.In_Form__Is_Default__c){
                                In_Form__Transaction__c newTrans = trans.clone(false,true);
                                newTrans=getTransaction(newTrans,weeklyAmount,trans.In_Form__Amount__c,rsL.In_Form__Amount__c,oType,rsL.Id);
                                
                                if(newTrans!=null){
                                    transactions.add(newTrans);
                                }
                                break;
                            }
                        }
                    } 
                }
            }
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
        return transactions ;
    }
    
    //Method to get Transaction with provided details
    public static In_Form__Transaction__c getTransaction(In_Form__Transaction__c newTrans,Boolean weeklyAmount,Decimal transactionAmount,Decimal rentAmount,String type,id rentSchId){
        
        try
        {
            //List of fields assigned for In_Form__Transaction__c Object
            /*List<String> fieldList = new List<String>{
                'In_Form__Amount__c',
                'In_Form__Type__c',
                'In_Form__Rent_Stream_Charge__c'
            };*/

            //if(InForm_CRUDFLS_Util.getCreateAccessCheck(String.valueOf(newTrans.getSobjectType()),fieldList)){

                if(weeklyAmount)
                    newTrans.In_Form__Amount__c = -(transactionAmount*(rentAmount/7));
                else
                    newTrans.In_Form__Amount__c = -(transactionAmount*rentAmount);
                newTrans.In_Form__Type__c = type;
                newTrans.In_Form__Rent_Stream_Charge__c = rentSchId; 
                
                return newTrans;
            //}
        }
        catch(Exception e)
        {
            
        }
        return null;
    }
    
    // Methot to get Rent Charges Map
    public static Map<String,List<In_Form__Rent_Stream_Charge__c>> getRentCharges(List<Id> bedspaceIds,Set<String> typeList,Boolean autoTrans){
        List<In_Form__Rent_Stream_Charge__c> rsCharges=new list<In_Form__Rent_Stream_Charge__c>();
        Map<String,List<In_Form__Rent_Stream_Charge__c>> rsChargeMap= new Map<String,List<In_Form__Rent_Stream_Charge__c>>();
        
        List<String> fieldList = new List<String>{'Id','In_Form__Amount__c','In_Form__Type__c','In_Form__Rent_Schedule__r.In_Form__Bedspace__c','In_Form__Rent_Schedule__r.In_Form__Start_Date__c','In_Form__Rent_Schedule__r.In_Form__Tenancy__c','In_Form__Rent_Schedule__r.In_Form__End_Date__c','In_Form__Rent_Schedule__r.createdDate','In_Form__Rent_Schedule__r.In_Form__Is_Default__c'};
        
        try
        {
            // List of Rent Stream Charge record based on Schedule Start Date and End Date.
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Rent_Stream_Charge__c',fieldList)){
                if(autoTrans==false){
                    rsCharges= [SELECT Id, In_Form__Amount__c,In_Form__Type__c,In_Form__Rent_Schedule__r.In_Form__Bedspace__c,In_Form__Rent_Schedule__r.In_Form__Start_Date__c,In_Form__Rent_Schedule__r.In_Form__Tenancy__c,
                                    In_Form__Rent_Schedule__r.In_Form__End_Date__c,In_Form__Rent_Schedule__r.createdDate,In_Form__Rent_Schedule__r.In_Form__Is_Default__c
                                    FROM In_Form__Rent_Stream_Charge__c WHERE In_Form__Rent_Schedule__r.In_Form__Bedspace__c IN: bedspaceIds 
                                    AND In_Form__Rent_Schedule__r.In_Form__Start_Date__c<=:Date.Today() 
                                    ORDER BY In_Form__Rent_Schedule__r.createddate DESC];
                
                }else{
                    rsCharges= [SELECT Id,In_Form__Amount__c,In_Form__Type__c,In_Form__Rent_Schedule__r.In_Form__Bedspace__c,In_Form__Rent_Schedule__r.In_Form__Start_Date__c,In_Form__Rent_Schedule__r.In_Form__End_Date__c,In_Form__Rent_Schedule__r.createdDate,In_Form__Rent_Schedule__r.In_Form__Is_Default__c
                                                        FROM In_Form__Rent_Stream_Charge__c WHERE In_Form__Rent_Schedule__r.In_Form__Bedspace__c IN:bedspaceIds and In_Form__Rent_Schedule__r.In_Form__Start_Date__c<=:Date.Today() and In_Form__Rent_Schedule__r.In_Form__End_Date__c >=: Date.Today()];
                }
            //}
               
            for(In_Form__Rent_Stream_Charge__c rSC : rsCharges){
                typeList.add(rSC.In_Form__Type__c);
                
                if(rsChargeMap.containsKey(rSC.In_Form__Rent_Schedule__r.In_Form__Bedspace__c + rSC.In_Form__Type__c)){
                    List<In_Form__Rent_Stream_Charge__c> rscList = rsChargeMap.get(rSC.In_Form__Rent_Schedule__r.In_Form__Bedspace__c + rSC.Type__c);
                    rscList.add(rSC);
                    rsChargeMap.put(rSC.In_Form__Rent_Schedule__r.In_Form__Bedspace__c + rSC.In_Form__Type__c,rscList); 
                }
                else{
                    rsChargeMap.put(rSC.In_Form__Rent_Schedule__r.In_Form__Bedspace__c + rSC.In_Form__Type__c,new List<In_Form__Rent_Stream_Charge__c>{rSC});
                }
            }
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
       return rsChargeMap; 
    }
    
    //Method to create Rent Schedule Transactions Specific
    public static list<In_Form__Transaction__c> createRentScheduleTransaction(list<In_Form__Transaction__c> transactionList,list<In_Form__Rent_Stream_Charge__c > rSCs,Boolean weeklyAmount ){
            Boolean switchoffFinancial=false;
            List<In_Form__Transaction_Type__c> tranTypeSetting = new List<In_Form__Transaction_Type__c>();
            
            //List<String> fieldList = new List<String>{'In_Form__Type__c','In_Form__Switch_off_all_financial_data__c','In_Form__Daily_Amount__c','In_Form__Day_of_Week__c'};
        
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction_Type__c',fieldList)){
                tranTypeSetting = [SELECT In_Form__Type__c,In_Form__Switch_off_all_financial_data__c, In_Form__Daily_Amount__c,In_Form__Day_of_Week__c FROM In_Form__Transaction_Type__c WHERE In_Form__Is_Active__c =: true LIMIT 1];  
            //}
            
            if(!tranTypeSetting.isEmpty()){
                if((!tranTypeSetting[0].In_Form__Daily_Amount__c) && tranTypeSetting[0].Type__c == 'Daily'){
                    weeklyAmount = true;
                }
                switchoffFinancial=tranTypeSetting[0].In_Form__Switch_off_all_financial_data__c; // Added by Ajay on 06/05/2018
            }
            
            if(switchoffFinancial==false){
            
                for(In_Form__Transaction__c trans : transactionList){
                    if(trans.In_Form__Tenancy__r.In_Form__Bedspace__r.No_financial_data__c==false){// Added by Ajay on 06/05/2018
                        for(In_Form__Rent_Stream_Charge__c rSC : rSCs){
                            if(trans.In_Form__Type__c == rSC.In_Form__Type__c && trans.In_Form__Transaction_Date__c >= rSC.In_Form__Rent_Schedule__r.In_Form__Start_Date__c && 
                                trans.In_Form__Transaction_Date__c <= rSC.In_Form__Rent_Schedule__r.In_Form__End_Date__c && 
                                trans.In_Form__Tenancy__c == rSC.In_Form__Rent_Schedule__r.In_Form__Tenancy__c){
                                
                                
                                if(trans.In_Form__Balance__c != null){
                                    trans.In_Form__Balance__c = trans.In_Form__Balance__c +(-rSC.In_Form__Amount__c)- (trans.In_Form__Amount__c);
                                }
                                
                                trans.In_Form__Rent_Stream_Charge__c = rSC.Id; 
                                if(weeklyAmount)
                                    trans.In_Form__Amount__c = -(rsC.In_Form__Amount__c/7);
                                else
                                    trans.In_Form__Amount__c = -rSC.In_Form__Amount__c;   
                            }
                        }
                    }
                }
            }
        return transactionList;
    }
    
    //Method to create/get Transaction
    public static In_Form__Transaction__c getCreateTransaction(id tenancyid,DateTime tranDate,id rentChargesid,id recordType,Decimal rentCharges,string type,boolean WeeklyAmount){
        In_Form__Transaction__c trans=new In_Form__Transaction__c();

        
            //List of fields assigned for In_Form__Transaction__c Object
            /*List<String> fieldList = new List<String>{'In_Form__Tenancy__c',
                'In_Form__Transaction_date__c',
                'In_Form__Rent_Stream_Charge__c',
                'RecordTypeId',
                'In_Form__Amount__c',
                'In_Form__Type__c'
            };*/

            //if(InForm_CRUDFLS_Util.getCreateAccessCheck(String.valueOf(trans.getSobjectType()),fieldList)){
                
                trans.In_Form__Tenancy__c = tenancyid;
                trans.In_Form__Transaction_date__c= tranDate;
                trans.In_Form__Rent_Stream_Charge__c = rentChargesid;
                trans.RecordTypeId = recordType;

                if(weeklyAmount)
                    trans.In_Form__Amount__c = -(rentCharges/7);
                else
                    trans.In_Form__Amount__c = - rentCharges;
                 
                trans.In_Form__Type__c=type;
                return trans;
            //}
        
        
        //return null;
    }  
}