/*------------------------------------------------------------------
/* Created By : 
 * Created date : 6/8/2015
 * Description: Create transaction automatically based on scheduler
 * History: v1.1 - Sneha U. - 26/10/2018 - Enforcing CRUD/FLS
------------------------------------------------------------------*/ 
public with sharing class AutomaticTransactionCreationUtility{
    //Transaction creation for tenancy
    public static void createTransactionforTenancies(List<Tenancy__c> tenancyList){
        List<Id> bedspaceIdList = new List<Id>();
        Date currentDate = Date.Today();
        boolean weeklyAmount = false;
        Map<String,Rent_Stream_Charge__c> rsChargeMap = new Map<String,Rent_Stream_Charge__c>();
        Id tranRecType = Schema.SObjectType.Transaction__c.getRecordTypeInfosByName().get('Rent charge').getRecordTypeId();
        List<In_Form__Transaction__c> transactionList = new List<In_Form__Transaction__c>();
        Set<String> typeList = new Set<String>();
        Map<String,Rent_Stream_Charge__c> rsTenancyChargeMap = new Map<String,Rent_Stream_Charge__c>();
        boolean financialDataOff=false; // Added by Ajay on 06/01/2018      
        map<id,boolean> mapBedspace=new map<id,boolean>(); // Added by Ajay on 06/05/2018
        
        for(In_Form__Tenancy__c bed : tenancyList){
            if(bed.In_Form__Bedspace__c != null)
                bedspaceIdList.add(bed.In_Form__Bedspace__c);
            mapBedspace.put(bed.id,bed.Bedspace__r.In_Form__No_financial_data__c); // Added by Ajay on 06/05/2018
        }
        //List<String> fieldList = new List<String>{'In_Form__Type__c','In_Form__Switch_off_all_financial_data__c','In_Form__Daily_Amount__c','In_Form__Day_of_Week__c'};
        //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction_Type__c',fieldList) ){
        List<In_Form__Transaction_Type__c> tranTypeSetting = [SELECT In_Form__Type__c, In_Form__Switch_off_all_financial_data__c,In_Form__Daily_Amount__c,In_Form__Day_of_Week__c FROM Transaction_Type__c WHERE In_Form__Is_Active__c =: true LIMIT 1];
        if(!tranTypeSetting.isEmpty()){
            if((!tranTypeSetting[0].In_Form__Daily_Amount__c) && tranTypeSetting[0].In_Form__Type__c =='Daily' ){
                weeklyAmount = true;
            }
            // Added by Ajay on 06/01/2018
            financialDataOff = tranTypeSetting[0].In_Form__Switch_off_all_financial_data__c;
        }
        //}
        //List<String> fieldListRSC = new List<String>{'In_Form__Amount__c','In_Form__Type__c','In_Form__Rent_Schedule__r.In_Form__Tenancy__c','In_Form__Rent_Schedule__r.In_Form__Bedspace__c','In_Form__Rent_Schedule__r.In_Form__Start_Date__c','In_Form__Rent_Schedule__r.In_Form__End_Date__c','In_Form__Rent_Schedule__r.In_Form__Is_Default__c'};
        //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Rent_Stream_Charge__c',fieldListRSC)){
        // List of Rent Stream Charge record needs to be updated based on Schedule Start Date and End Date.
        List<In_Form__Rent_Stream_Charge__c> rsCharges= [SELECT Id,In_Form__Amount__c,In_Form__Type__c,Rent_Schedule__r.In_Form__Tenancy__c ,Rent_Schedule__r.Bedspace__c,Rent_Schedule__r.Start_Date__c,Rent_Schedule__r.End_Date__c,Rent_Schedule__r.createdDate,Rent_Schedule__r.Is_Default__c
                                                FROM In_Form__Rent_Stream_Charge__c WHERE In_Form__Rent_Schedule__r.In_Form__Bedspace__c IN:bedspaceIdList and Rent_Schedule__r.Start_Date__c<=:Date.Today() and Rent_Schedule__r.End_Date__c >=: Date.Today()];

        for(In_Form__Rent_Stream_Charge__c isc: rsCharges){
            typeList.add(isc.In_Form__Type__c);
            
            if(isc.Rent_Schedule__r.Tenancy__c!=null){
                if(rsTenancyChargeMap.containsKey(isc.Rent_Schedule__r.In_Form__Bedspace__c+isc.In_Form__Type__c+isc.In_Form__Rent_Schedule__r.In_Form__Tenancy__c)){
                        if(isc.Rent_Schedule__r.createddate > rsTenancyChargeMap.get(isc.Rent_Schedule__r.Bedspace__c+isc.Type__c+isc.In_Form__Rent_Schedule__r.Tenancy__c).In_Form__Rent_Schedule__r.createdDate)
                            rsTenancyChargeMap.put(isc.Rent_Schedule__r.In_Form__Bedspace__c+isc.Type__c+isc.Rent_Schedule__r.In_Form__Tenancy__c,isc);  
                }
                else{
                    rsTenancyChargeMap.put(isc.In_Form__Rent_Schedule__r.In_Form__Bedspace__c+isc.In_Form__Type__c+isc.In_Form__Rent_Schedule__r.In_Form__Tenancy__c,isc);
                }
            }else if(isc.Rent_Schedule__r.In_Form__Is_Default__c){
                if(rsChargeMap.containsKey(isc.In_Form__Rent_Schedule__r.In_Form__Bedspace__c+isc.Type__c)){
                        if(isc.In_Form__Rent_Schedule__r.createddate > rsChargeMap.get(isc.Rent_Schedule__r.In_Form__Bedspace__c+isc.In_Form__Type__c).In_Form__Rent_Schedule__r.createdDate)
                            rsChargeMap.put(isc.In_Form__Rent_Schedule__r.In_Form__Bedspace__c+isc.In_Form__Type__c,isc);  
                }
                else{
                    rsChargeMap.put(isc.In_Form__Rent_Schedule__r.In_Form__Bedspace__c+isc.In_Form__Type__c,isc);
                }
            }
        }
        //}
        // Added by Ajay on 06/01/2018
        if(financialDataOff==false){
        //End Added by Ajay on 06/01/2018
            for(In_Form__Tenancy__c tenants : tenancyList){
                if(mapBedspace.get(tenants.id)==false){ // Added by Ajay on 06/05/2018
                   for(String oType : typeList){
                        if(rsTenancyChargeMap.containsKey(tenants.In_Form__Bedspace__c+ oType+tenants.id)){
                            In_Form__Transaction__c trans = new In_Form__Transaction__c();
                            trans=transactionUtility.getCreateTransaction(tenants.Id,DateTime.Now(),rsTenancyChargeMap.get(tenants.In_Form__Bedspace__c+oType+tenants.id).Id,tranRecType,rsTenancyChargeMap.get(tenants.Bedspace__c+oType+tenants.id).Amount__c,rsTenancyChargeMap.get(tenants.Bedspace__c+oType+tenants.id).Type__c,WeeklyAmount);
                            
                            //Check if method has returned null
                            if(trans!=null){
                                transactionList.add(trans);
                            }
                        }else if(rsChargeMap.containsKey(tenants.In_Form__Bedspace__c+ oType)){
                            In_Form__Transaction__c trans = new In_Form__Transaction__c();
                            trans=transactionUtility.getCreateTransaction(tenants.Id,DateTime.Now(),rsChargeMap.get(tenants.In_Form__Bedspace__c+oType).Id,tranRecType,rsChargeMap.get(tenants.Bedspace__c+oType).Amount__c,rsChargeMap.get(tenants.Bedspace__c+oType).Type__c,WeeklyAmount);
                            
                            //Check if method has returned null
                            if(trans!=null){
                                transactionList.add(trans);
                            }
                        }
                    }
                }
            }
        }
        if(!transactionList.isEmpty()){
            systemBatchHelper.isBatchRunning=true;
            insert transactionList;
        }
    }
}