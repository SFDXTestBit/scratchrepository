/*------------------------------------------------------------------
/* Created By : 
* Created date : 14/8/2015
* Description: Rent Charge Split class
               v1.0 - 31/10/2018 - Changes For Security Review
------------------------------------------------------------------*/

global with sharing class SplitRentChargeLightning {
    
    @AuraEnabled
    public static Boolean checkfinancialData(Id tenancyId) {
        
        Boolean nofinancialData=false;
        Boolean result=false;

        try
        {
            Id strId = getBedSpaceid(tenancyId);
            
            //List<String> fieldList=new List<String>{'id','In_Form__No_financial_data__c'};

            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Bedspace__c',fieldList)){
                for(Bedspace__c bdS: [SELECT id,In_Form__No_financial_data__c from In_Form__Bedspace__c where id =: String.escapeSingleQuotes(strId)]){
                    nofinancialData=bdS.In_Form__No_financial_data__c;
                }
            //}
            
            //fieldList.clear();
            //fieldList=new List<String>{'In_Form__Type__c','In_Form__Switch_off_all_financial_data__c','In_Form__Is_Active__c', 'In_Form__Daily_Amount__c','In_Form__Day_of_Week__c'};
            
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction_Type__c',fieldList)){
                List<In_Form__Transaction_Type__c> setting = [SELECT In_Form__Type__c,In_Form__Switch_off_all_financial_data__c,In_Form__Is_Active__c,In_Form__Daily_Amount__c,In_Form__Day_of_Week__c FROM In_Form__Transaction_Type__c WHERE In_Form__Is_Active__c =: true LIMIT 1];
                
                if(!setting .isEmpty()){
                    if((nofinancialData==true || setting[0].In_Form__Switch_off_all_financial_data__c == true) && setting[0].In_Form__Is_Active__c == true ){              
                        result = true;
                        
                    }
                }
            //}
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
        return result;
    }
    @AuraEnabled
    public static id getBedSpaceid(Id tenancyId) {
        
        //List<String> fieldList=new List<String>{'id','In_Form__Bedspace__c'};
        
        try
        {
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__tenancy__c',fieldList)){
                list<In_Form__tenancy__c> listTen=[SELECT id,In_Form__Bedspace__c from In_Form__tenancy__c where id =: String.escapeSingleQuotes(tenancyId)];
            
                if(listTen.isempty()==false){
                    return listTen[0].In_Form__bedspace__c;
                }
            //}
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
        
        return null;
    }

    @AuraEnabled
    public static In_Form__Rent_Schedule__c splitRent(Id tenancyId) {
        In_Form__Rent_Schedule__c rentsch = new In_Form__Rent_Schedule__c();
        
        try
        {
            Id strId = getBedSpaceid(tenancyId);
            
            //List<String> fieldList=new List<String>{'Id','In_Form__Start_date__c','In_Form__End_date__c','In_Form__Total_rent_charge__c','In_Form__Tenancy__c'};
            //&& InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Rent_Schedule__c',fieldList)
            if(strId != null ){
                List<In_Form__Rent_Schedule__c> rsList = [SELECT Id,In_Form__Start_date__c,In_Form__End_date__c,In_Form__Total_rent_charge__c,In_Form__Tenancy__c FROM In_Form__Rent_Schedule__c WHERE In_Form__Bedspace__c =: String.escapeSingleQuotes(strId) AND In_Form__Start_date__c <=: Date.Today() AND In_Form__End_date__c >=: Date.Today() ORDER BY createddate DESC LIMIT 1];            

                rentsch.In_Form__End_date__c = getFiscalLastDate();
                
                if(rsList.size()>0 ){ 
                    rentsch.In_Form__Total_rent_charge__c=rsList[0].In_Form__Total_rent_charge__c;
                }
            }
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
        
        return rentsch;           
        
    }

    
    //Method sets available types of rent stream
    @AuraEnabled
    public static List<amountTypeWrapper> getTypes() {       
        List<amountTypeWrapper> amountTypeWrapList = new List<amountTypeWrapper>();
        
        try
        {
            Schema.DescribeFieldResult fieldResult = In_Form__Rent_Stream_Charge__c.In_Form__Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for(Schema.PicklistEntry f : ple)
            {
                amountTypeWrapper wrap = new amountTypeWrapper();
                wrap.type = f.getValue();
                wrap.amount = 0;
                amountTypeWrapList.add(wrap);
            }   
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
        return amountTypeWrapList;    
    }
    
    //Method to split amount and save.
    @AuraEnabled
    public static String save(In_Form__Rent_Schedule__c rentsch,String amountType,Id tenancyId) {
        String returnMessage;

        Id strId = getBedSpaceid(tenancyId);
        
        List<amountTypeWrapper> amountTypeList = new list<amountTypeWrapper>(); 

        try{
        if(amountType<>null && amountType<>'')
            amountTypeList =(List<amountTypeWrapper>)System.JSON.deserialize(amountType, List<amountTypeWrapper>.class);
        }
        catch(Exception ex){
            returnMessage = 'Rent split can not be created. Please contact administrator';
        }
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            
            //List<String> fieldList=new List<String>{'In_Form__Start_Date__c','In_Form__Total_rent_charge__c','In_Form__Tenancy__c','In_Form__Bedspace__c'};
            //InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Rent_Schedule__c',fieldList) &&
            if( rentsch.In_Form__Start_Date__c <= Date.Today()){
                Decimal val=0;
                for(amountTypeWrapper str : amountTypeList){                    
                    val= val+str.amount;
                }
                if(val == rentsch.Total_rent_charge__c){
                    rentsch.In_Form__Tenancy__c = tenancyId;
                    rentsch.In_Form__Bedspace__c = strId;

                    INSERT rentsch;
                    
                    //fieldList.clear();
                    //fieldList = new List<String>{'In_Form__Rent_Schedule__c','In_Form__Amount__c','In_Form__Type__c'};
                    
                    //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Rent_Stream_Charge__c',fieldList)){
                        List<In_Form__Rent_Stream_Charge__c> rsList = new List<In_Form__Rent_Stream_Charge__c>();
                        
                        for(amountTypeWrapper str : amountTypeList){
                            In_Form__Rent_Stream_Charge__c rsObject = new In_Form__Rent_Stream_Charge__c();
                            rsObject.In_Form__Rent_Schedule__c = rentsch.Id;
                            rsObject.In_Form__Amount__c = str.amount;
                            rsObject.In_Form__Type__c = str.type;
                            rsList.add(rsObject);
                        }
                        Insert rsList; 
                    //}
         
                    returnMessage = 'Records created succesfully';
                }
                else{
                    returnMessage = 'Amount split does not match Total amount';
                }
            }
            //else{
               // returnMessage = 'Start Date can not be a future date';
            //}
        }
        catch(DMLException ex){
            Database.rollback(sp);
        }
        catch(Exception ex){
            Database.rollback(sp);
            returnMessage = 'Rent split can not be created. Please contact administrator';
        }
        return returnMessage;
    }      
    
    //Method returns last date of fiscal year
    @AuraEnabled
    public static date getFiscalLastDate() {
        Integer currentFY;
        Integer currentFYMonth;
        Integer CurrentFYDays;
        Date today;
        Date FYStartDate;
        Date FYEndDate;
        
        try
        {
            Organization orgInfo = [SELECT FiscalYearStartMonth, UsesStartDateAsFiscalYearName
                                    FROM Organization
                                    WHERE id=:Userinfo.getOrganizationId()];
            
            today = system.today();
            currentFYMonth = orgInfo.FiscalYearStartMonth;
            
            if (today.month() >= orgInfo.FiscalYearStartMonth) {
                if (orgInfo.UsesStartDateAsFiscalYearName) {
                    currentFY = today.year();
                } 
                else {
                    currentFY = today.year() ;
                }
            } 
            else {
                if (orgInfo.UsesStartDateAsFiscalYearName) {
                    currentFY = today.year() - 1;
                }
                else {
                    currentFY = today.year();
                }
            }
            
            CurrentFYDays = date.daysInMonth(currentFY, currentFYMonth);
            
            FYStartDate = date.parse('01/' +CurrentFYMonth + '/' + currentFY);
            FYEndDate = FYStartDate.addYears(1).addDays(-1);
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }

        return FYEndDate;
    }   
    
}