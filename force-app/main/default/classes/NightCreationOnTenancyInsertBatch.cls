/* Class Name  : NightCreationOnTenancyInsertBatch
 * Description   : Batch Apex class to create records for Night after insertion of Tenancy.
 * Created By    : Raushan Anand
 * Created On    : 03-Dec-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              03-Dec-2015               Initial version.
 *  * Dinesh D.                  31-Oct-2018               Enforcing FLS/Try-Catch 
 *****************************************************************************************/

global with sharing class NightCreationOnTenancyInsertBatch implements Database.Batchable<sObject>, Database.Stateful{
    List<Id> tenancyIds = new List<Id>();
    global NightCreationOnTenancyInsertBatch(List<ID> idList){
        tenancyIds=idList;
    }
    /* Method Name : start
     * Description : Batch Class Start Method
     * Return Type : List of Night Records - List<Night__c>
     * Input Parameter : Database.BatchableContext
     */
    global List<Night__c> start(Database.BatchableContext BC){
        
        //List of fields assigned for Tenancy__c Object read access
        /*List<String> fieldList = new List<String>{
            'Id',
            'In_Form__Start_date__c',
            'In_Form__End_date__c',
            'RecordType.Name',
            'In_Form__Void_Status__c',
            'In_Form__Void_Reason__c'               
        };*/ 
        List<Night__c> nightList = new List<Night__c>();
        try{
            //Check read access for Tenancy Object
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',fieldList) ){    
                List<Tenancy__c> tenList = [SELECT Id,Start_date__c,End_date__c, RecordType.Name, Void_Status__c, Void_Reason__c FROM Tenancy__c WHERE Id IN: tenancyIds];
                for(Tenancy__c ten : tenList){
                    Date endDate;
                    if(ten.End_date__c == null){
                        endDate = Date.today();
                    }
                    else{
                        endDate = ten.End_date__c.addDays(-1);
                    }
                    
                    List<String> fieldList_Night = new List<String>{
                        'In_Form__Tenancy__c',
                        'In_Form__Date__c',
                        'In_Form__Void_Status__c',
                        'In_Form__Void_Reason__c'               
                    }; 
                               
                    //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Night__c',fieldList_Night)){ 
                        for(Date dt = ten.Start_date__c ; dt<= endDate ; dt = dt.addDays(1)){
                            Night__c oNight = new Night__c(Tenancy__c = ten.Id, Date__c = dt);
                            
                            if(ten.RecordType.Name == 'Void'){
                                oNight.Void_Status__c = ten.Void_Status__c;
                                oNight.Void_Reason__c = ten.Void_Reason__c;
                            }
            
                            nightList.add(oNight);
                        }
                    //}
                }
            //}
            return nightList;
        }Catch(Exception e){
            System.debug('Exception:'+e.getMessage());
            return null;
        }    
        return null;            
    }
    /* Method Name : execute
     * Description : Batch Class execute Method to insert Nights
     * Return Type : void
     * Input Parameter : Database.BatchableContext, List of Nights - List<Night__c>
     */
    global void execute(Database.BatchableContext BC, List<Night__c> scope){
        if(!scope.isEmpty() && scope != null) {           
            systemBatchHelper.isBatchRunning=true;
            INSERT scope;                      
        }     
    }
    
    global void finish(Database.BatchableContext BC){
       
    }
}