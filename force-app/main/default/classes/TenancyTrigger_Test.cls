/*------------------------------------------------------------------
/* Created By : 
 * Created date : 17/8/2015
 * Modified Date : 30-10-2018
 * Modified By : Bhargav Shah
 * Description: Test class for Tenancy creation updation and related functionality
------------------------------------------------------------------*/
@isTest
public class TenancyTrigger_Test{
    //Test data setup
    @TestSetup 
    public static void createData(){
        try{
        Transaction_Type__c dailytype = new Transaction_Type__c(Day_of_Week__c = 'Monday',Is_Active__c=true,Type__c = 'Weekly',Name = 'weeklyType');
        INSERT dailyType;
        
        Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
        INSERT con;
        Contact con1 = new Contact(Lastname='DemoName',FirstName='DemoFname');
        INSERT con1;
        Bedspace__c bed = new Bedspace__c(Name='testBedSpace',Status__c = null, In_Form__Basic_Rent__c=100);
        INSERT bed;
        List<Bedspace__c> bedSList = [SELECT Id,Name FROM Bedspace__c WHERE Name=:'testBedSpace' LIMIT 1];
        if(!bedSList.isEmpty()){
            System.assertEquals('testBedSpace', bedSList[0].Name, 'Amount match');
        }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }
        
    }
    //Test method to test bedspace status on insertiion of Let tenancy
    public static testmethod void testStatusUpdate(){
        try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace'];
            if(!bedList.isEmpty()){
                List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
                
                Map<String,Id> rtMap = new Map<String,Id>();
                if(!rtList.isEmpty()){
                    for(RecordType rt: rtList)
                        rtMap.put(rt.Name,rt.Id);
                }
                List<Tenancy__c> tenList = new List<Tenancy__c>();
               
                Tenancy__c ten1 = new Tenancy__c();
                ten1.recordTypeid = rtMap.get('Tenancy');
                ten1.Client__c = conList[0].Id;
                ten1.Start_date__c=Date.Today();
                ten1.Status__c=null;
                ten1.Bedspace__c=bedList[0].Id;
                tenList.add(ten1);
                
                Tenancy__c ten2 = new Tenancy__c();
                ten2.recordTypeid = rtMap.get('Void');
                ten2.Client__c = conList[0].Id;
                ten2.Start_date__c=Date.Today();
                ten2.Status__c='Current';
                ten2.Void_Status__c = 'Available To Let';
                ten2.Bedspace__c=bedList[0].Id;
                tenList.add(ten2);
                
                Tenancy__c ten3 = new Tenancy__c();
                ten3.recordTypeid = rtMap.get('Void');
                ten3.Client__c = conList[0].Id;
                ten3.Start_date__c=Date.Today();
                ten3.Status__c='Current';
                ten3.Void_Status__c = 'Unavailable';
                ten3.Bedspace__c=bedList[0].Id;
                tenList.add(ten3);
                
                Tenancy__c ten4 = new Tenancy__c();
                ten4.recordTypeid = rtMap.get('Tenancy');
                ten4.Client__c = conList[1].Id;
                ten4.Status__c = 'Void';
                ten4.Start_date__c=Date.Today();
                ten4.Bedspace__c=bedList[0].Id;
                tenList.add(ten4);   
                try{
                   INSERT tenList;
                }catch(Exception e){
                   System.debug('Exception:'+e.getMessage());
                }
                Transaction__c tran = new Transaction__c();
                tran.Tenancy__c = tenList[0].Id;
                tran.Transaction_Date__c= DateTime.now().addDays(-1);
                tran.Type__c = 'Key Deposit';
                try{
                    insert tran;
                    Tenancy__c tenUpdate = tenList.get(0);
                    tenUpdate.End_Date__c = Date.today();
                }Catch(Exception  e){
                    System.debug('Exception:'+e.getMessage());
                }
                 Test.startTest();
                    TenancyTriggerUtility.closeTenancy(tenList);
                 Test.stopTest();
                List<Bedspace__c> bedUpdatedList = [SELECT Id,Status__c FROM Bedspace__c WHERE Id=:bedList[0].Id LIMIT 1];
                System.assertEquals('Void - available to let',bedUpdatedList[0].Status__c, 'Available Status Update');
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    }
    //Test method to test bedspace status on insertion of Available to let tenancy
    public static testmethod void testVoidStatusAvailableToLet(){
        try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace'];
            if(!bedList.isEmpty()){
                List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
                
                Map<String,Id> rtMap = new Map<String,Id>();
                if(!rtList.isEmpty()){
                    for(RecordType rt: rtList)
                        rtMap.put(rt.Name,rt.Id);
                }
                List<Tenancy__c> tenList = new List<Tenancy__c>();
               
                Tenancy__c ten1 = new Tenancy__c();
                ten1.recordTypeid = rtMap.get('Void');
                ten1.Client__c = conList[0].Id;
                ten1.Start_date__c=Date.Today();
                ten1.Status__c='Current';
                ten1.Void_Status__c = 'Available To Let';
                ten1.Bedspace__c=bedList[0].Id;
                tenList.add(ten1);
                
                INSERT tenList;
                List<Bedspace__c> bedUpdatedList = [SELECT Id,Status__c FROM Bedspace__c WHERE Id=:bedList[0].Id LIMIT 1];
                System.assertEquals('Void - available to let',bedUpdatedList[0].Status__c, 'Available Status Update');
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }        
    }
    //Test method to test bedspace status on insertion of Void tenancy
    public static testmethod void testVoidStatusUnavailable(){
        try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace'];
            if(!bedList.isEmpty()){
                List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
                
                Map<String,Id> rtMap = new Map<String,Id>();
                if(!rtList.isEmpty()){
                    for(RecordType rt: rtList)
                        rtMap.put(rt.Name,rt.Id);
                }
                List<Tenancy__c> tenList = new List<Tenancy__c>();
               
                Tenancy__c ten1 = new Tenancy__c();
                ten1.recordTypeid = rtMap.get('Void');
                ten1.Client__c = conList[0].Id;
                ten1.Start_date__c=Date.Today();
                ten1.Status__c='Current';
                ten1.Void_Status__c = 'Unavailable';
                ten1.Bedspace__c=bedList[0].Id;
                tenList.add(ten1);
                INSERT tenList;
                List<Bedspace__c> bedUpdatedList = [SELECT Id,Status__c FROM Bedspace__c WHERE Id=:bedList[0].Id LIMIT 1];
                System.assertEquals('Void - unavailable',bedUpdatedList[0].Status__c, 'Unavailable Status Update');
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }        
    }
    //Test method to test bedspace status on insertion of historic tenancy
    public static testmethod void testVoidStatusHistoric(){
        try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace'];
            if(!bedList.isEmpty()){
                List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
                
                Map<String,Id> rtMap = new Map<String,Id>();
                if(!rtList.isEmpty()){
                    for(RecordType rt: rtList)
                        rtMap.put(rt.Name,rt.Id);
                }
                List<Tenancy__c> tenList = new List<Tenancy__c>();
               
                Tenancy__c ten1 = new Tenancy__c();
                ten1.recordTypeid = rtMap.get('Tenancy');
                ten1.Client__c = conList[0].Id;
                ten1.Start_date__c=Date.Today();
                ten1.Bedspace__c=bedList[0].Id;
                INSERT ten1;
                ten1.Status__c='Historic'; 
                update ten1;
                ten1.Status__c='Current';
                update ten1;
                List<Bedspace__c> bedUpdatedList = [SELECT Id,Status__c FROM Bedspace__c WHERE Id=:bedList[0].Id LIMIT 1];
                System.AssertNotEquals(ten1.Id,null);
             }
         }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }        
    }
    //Test method to test bedspace status on deletion of historic tenancy
    public static testmethod void testVoidStatusDeleteHistoric(){
        try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace'];
            if(!bedList.isEmpty()){
                List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
                
                Map<String,Id> rtMap = new Map<String,Id>();
                if(rtList!= null && rtList.size()>0){
                    for(RecordType rt: rtList)
                    rtMap.put(rt.Name,rt.Id);
                }
                List<Tenancy__c> tenList = new List<Tenancy__c>();
               
                Tenancy__c ten1 = new Tenancy__c();
                ten1.recordTypeid = rtMap.get('Tenancy');
                ten1.Client__c = conList[0].Id;
                ten1.Start_date__c=Date.Today();
                ten1.Bedspace__c=bedList[0].Id;
                Tenancy__c ten2 = new Tenancy__c();
                ten2.recordTypeid = rtMap.get('Tenancy');
                ten2.Client__c = conList[0].Id;
                ten2.Start_date__c=Date.Today();
                ten2.Bedspace__c=bedList[0].Id;
                INSERT ten1;
                delete ten1;
                List<Bedspace__c> bedUpdatedList = [SELECT Id,Status__c FROM Bedspace__c WHERE Id=:bedList[0].Id LIMIT 1];
                System.assertEquals('Let',bedUpdatedList[0].Status__c, 'Historic Delete Status Update');
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }
    }
    // Test Method to test creation of transaction
    public static testmethod void testTransactionCreation(){
        try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace' LIMIT 1];
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            Map<String,Id> rtMap = new Map<String,Id>();
            if(!rtList.isEmpty()){
                for(RecordType rt: rtList)
                    rtMap.put(rt.Name,rt.Id);
            }
            Rent_Schedule__c oRentSchedule1 = new Rent_Schedule__c(Bedspace__c =bedList[0].Id, Start_date__c =Date.today().adddays(-4),End_date__c =Date.today().adddays(-1),Is_Default__c=true,In_Form__Is_default__c=true);
            Rent_Schedule__c oRentSchedule2 = new Rent_Schedule__c(Bedspace__c =bedList[0].Id, Start_date__c =Date.today().adddays(-4),End_date__c =Date.today().adddays(1),In_Form__Is_default__c=true);
            List<Rent_Schedule__c> rsList = new List<Rent_Schedule__c>();
            rsList.add(oRentSchedule1);
            rsList.add(oRentSchedule2);
            INSERT rsList;
            Rent_Stream_Charge__c oRSCharge1 = new Rent_Stream_Charge__c(Rent_Schedule__c = rsList[0].Id,Type__c='Housing benefit',Amount__c=100);
            Rent_Stream_Charge__c oRSCharge2 = new Rent_Stream_Charge__c(Rent_Schedule__c = rsList[1].Id,Type__c='Housing benefit',Amount__c=50);
            Rent_Stream_Charge__c oRSCharge3 = new Rent_Stream_Charge__c(Rent_Schedule__c = rsList[0].Id,Type__c='Housing benefit',Amount__c=100);
            Rent_Stream_Charge__c oRSCharge4 = new Rent_Stream_Charge__c(Rent_Schedule__c = rsList[1].Id,Type__c='Housing benefit',Amount__c=50);
            List<Rent_Stream_Charge__c> rscList = new List<Rent_Stream_Charge__c>();
            rscList.add(oRSCharge1);
            rscList.add(oRSCharge2);
            rscList.add(oRSCharge3);
            rscList.add(oRSCharge4);
            INSERT rscList;
            List<Transaction__c> tranList = new List<Transaction__c>();
            Tenancy__c ten1 = new Tenancy__c();
            ten1.recordTypeid = rtMap.get('Tenancy');
            ten1.Client__c = conList[0].Id;
            ten1.Start_date__c=Date.Today().addDays(-3);
            ten1.Status__c='Current';
            ten1.Bedspace__c=bedList[0].Id;
            INSERT ten1;
            
            Transaction__c tran1 = new Transaction__c(Tenancy__c = ten1.Id,Amount__c=100,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-1));
            tranList.add(tran1);
            Transaction__c tran2 = new Transaction__c(Tenancy__c = ten1.Id,Amount__c=50,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-2));
            tranList.add(tran2);
            
            INSERT tranList;
            
            List<Transaction__c> tranUpdatedList = [SELECT Id,Name from Transaction__c WHERE Tenancy__c =: ten1.Id];
            System.assertEquals(2,tranUpdatedList.size(), 'SUCCESS');
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    } 
    
    //Test method to test bedspace status on insertiion of Let tenancy
    public static testmethod void preventContinueExecution(){
        try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace'];
            if(!bedList.isEmpty()){
                List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
                
                Map<String,Id> rtMap = new Map<String,Id>();
                if(rtList.isEmpty()){
                    for(RecordType rt: rtList)
                    rtMap.put(rt.Name,rt.Id);
                }
                List<Tenancy__c> tenList = new List<Tenancy__c>();
               
                Tenancy__c ten1 = new Tenancy__c();
                ten1.recordTypeid = rtMap.get('Void');
                ten1.Status__c='Historic';
                ten1.Client__c = conList[0].Id;
                ten1.Start_date__c=Date.Today().addDays(-2);
                ten1.Bedspace__c=bedList[0].Id;
                tenList.add(ten1);
                INSERT tenList;
                
                Transaction__c tran = new Transaction__c();
                tran.Tenancy__c = tenList[0].Id;
                tran.Transaction_Date__c= DateTime.now();
                tran.Type__c = 'Key Deposit';
                insert tran;
                Tenancy__c tenUpdate = tenList.get(0);
                tenUpdate.End_Date__c = Date.today();
                Update tenUpdate;
                TenancyTriggerUtility.updateonDelStatusonBedspace(tenList);
        
                List<Bedspace__c> bedUpdatedList = [SELECT Id,Status__c FROM Bedspace__c WHERE Id=:bedList[0].Id LIMIT 1];
                System.assertEquals('Void - available to let',bedUpdatedList[0].Status__c, 'Available Status Update');
            }    
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }
    }
     public static testmethod void newtendency(){
         try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace'];
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            
            Map<String,Id> rtMap = new Map<String,Id>();
            if(!rtList.isEmpty()){
                for(RecordType rt: rtList)
                    rtMap.put(rt.Name,rt.Id);
            }
            List<Tenancy__c> tenList = new List<Tenancy__c>();
           
            Tenancy__c ten5 = new Tenancy__c();
            ten5.recordTypeid = rtMap.get('Tenancy');
            ten5.Client__c = conList[0].Id;
            ten5.Start_date__c=Date.Today();
            ten5.Status__c= 'Current';
            ten5.Bedspace__c=bedList[0].Id;
            tenList.add(ten5);    
            INSERT tenList;
            Test.startTest();    
                TenancyTriggerUtility.updateonDelStatusonBedspace(tenList);
            Test.stopTest();    
            System.assertEquals(1, tenList.size());
         }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
     }
     
     public static testmethod void validateNights(){
         try{
            List<Contact> conList = [SELECT Id FROM Contact];
            List<Bedspace__c> bedList = [SELECT Id FROM Bedspace__c WHERE Name=:'testBedSpace'];
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            
            Map<String,Id> rtMap = new Map<String,Id>();
            if(!rtList.isEmpty()){
                for(RecordType rt: rtList)
                rtMap.put(rt.Name,rt.Id);
            }
            List<Tenancy__c> tenList = new List<Tenancy__c>();
           
            Tenancy__c ten5 = new Tenancy__c();
            ten5.recordTypeid = rtMap.get('Tenancy');
            ten5.Client__c = conList[0].Id;
            ten5.Start_date__c=Date.Today();
            ten5.Status__c= 'Current';
            ten5.Bedspace__c=bedList[0].Id;
            tenList.add(ten5);
            if(!tenList.isEmpty()){
                INSERT tenList;
                    
                map<id,Tenancy__c> mapTen=new map<id,Tenancy__c >(); 
                tenList[0].Start_date__c=Date.Today().adddays(-5);
                mapTen.put(tenList[0].id,tenList[0]);  
                Test.startTest(); 
                    TenancyTriggerUtility.validateNights(tenList,null);
                    TenancyTriggerUtility.updateNightsOnVoidReasonChange(tenList); 
                    TenancyTriggerUtility.validateNights(tenList,mapTen);
                Test.stopTest();
                System.assertEquals(1, tenList.size());
            }
       }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
     }
}