/*------------------------------------------------------------------
 * Created By : Saurabh Gupta ,
 * Created date :  02/02/2017 10:41
 * Last Modified by:: Saurabh Gupta ,  25/09/2017 05:46
 * Description: Batch class to create child records of dl batch object
 history : v1.0 - 31/10/2018 - Changes For Security Review
------------------------------------------------------------------*/
global with sharing class BatchToCreateDLRecord Implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful {
    //Initialization
    global List<String> orderList; 
    global String objName;
    global String dlscopeName;
    global Id batchDLId;
    global Id networkId;    
    global List<In_Form__DL_Field_Mapping__c> fieldMapList;
    global Map<String,List<In_Form__DL_Field_Mapping__c>> objMap;
    global String fields;
    global String status;
    global Boolean checkRecordType;
    global set<Id> sharingIds;
    
    //constructor
    public BatchToCreateDLRecord(List<String> orderList,Id batchId,String status,set<Id> sharingIds){
    
    try
    {
        this.orderList = orderList;
        this.batchDLId = batchId;
        this.objName = orderList.get(0);
        this.status = status;
        this.sharingIds = sharingIds;
        checkRecordType = false;
        
        In_Form__DataBatchCallOrder__mdt dataBatch = [Select DeveloperName,MasterLabel,In_Form__DL_Scope_Field_Names__c 
                            FROM In_Form__DataBatchCallOrder__mdt WHERE MasterLabel =: objName LIMIT 1];

        this.dlscopeName = dataBatch.In_Form__DL_Scope_Field_Names__c;
        
        
        List<RecordType> rts = [SELECT Id FROM RecordType WHERE SObjectType = :String.escapeSingleQuotes(objName)];
        
        if (rts.size() > 0) {
            checkRecordType = true;
        }
        
        networkId = BatchToCreateDLRecordHelper.getConnectionId(System.Label.ConnectionName);
        
        objMap = new Map<String,List<In_Form__DL_Field_Mapping__c>>();
        
        //List<String> fieldList = new List<String>{'Id','In_Form__Record_Type_Selection__c','In_Form__Record_Type__c','In_Form__Description__c','In_Form__Mapping_Status__c', 'In_Form__Object_Field_Name__c','In_Form__Object_Field_Type__c','In_Form__Object_Name__c', 'In_Form__Source_Field__c','In_Form__Source_Field_Label__c','In_Form__Source_Field_Type__c', 'In_Form__Source_Object__c'};
        
        //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__DL_Field_Mapping__c',fieldList)){
            fieldMapList = [SELECT Id,In_Form__Record_Type_Selection__c,In_Form__Record_Type__c,In_Form__Description__c,In_Form__Mapping_Status__c,
                            In_Form__Object_Field_Name__c,In_Form__Object_Field_Type__c,In_Form__Object_Name__c,
                            In_Form__Source_Field__c,In_Form__Source_Field_Label__c,In_Form__Source_Field_Type__c,
                            In_Form__Source_Object__c FROM In_Form__DL_Field_Mapping__c 
                            WHERE In_Form__Source_Object__c =: String.escapeSingleQuotes(objName)];
        //}
        
        set<String> fieldSet = new Set<String>();
        
        for(In_Form__DL_Field_Mapping__c obj : fieldMapList){
            
            if(obj.In_Form__Source_Field__c != 'Id' && obj.In_Form__Source_Field__c != 'id' && obj.In_Form__Source_Field__c != 'RecordTypeId' && obj.In_Form__Source_Field__c != 'recordtypeid'){
                
                fieldSet.add(obj.In_Form__Source_Field__c);
                
            } 
            
            if(objMap.containsKey(obj.In_Form__Object_Name__c)){
                
                List<In_Form__DL_Field_Mapping__c> vName = objMap.get(obj.In_Form__Object_Name__c);
                vName.add(obj);
                objMap.put(obj.In_Form__Object_Name__c,vName);
                
            }else{
                
                List<In_Form__DL_Field_Mapping__c> vName  = new List<In_Form__DL_Field_Mapping__c>();
                vName.add(obj);
                objMap.put(obj.In_Form__Object_Name__c,vName);    
            } 
            
        }
        
        if(checkRecordType){
            fieldSet.add('RecordTypeId');
        }
        
        fields = '';
        
        for(String val : fieldSet){
            if(fields !=''){
                fields +=','; 
            }  
            
            fields += val;
        }        
        
        fields = fields.removeEnd(',');
        
        if(fields != ''){
            fields =','+fields;
        } 
    }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
    
    }
    
    //return custom metadata field whose infrm__DL_Scope_Field_Names__c true
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id'+fields+' FROM '+objName+' WHERE '+dlscopeName+' = true';
        
        /*List<String> fieldList = new List<String>{'Id'};
        fieldList.add(fields);*/
        
        //if(InForm_CRUDFLS_Util.getReadAccessCheck(objName,fieldList)){
            return Database.getQueryLocator(query); 
        //}else{
           // return null;
        //}   
    }
    
    //Execute method to insert records in dl batch object
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        try{
            List<sObject> objList = new List<sObject>();
            
            if(!fieldMapList.isEmpty()){
                List<String> destObjs = new List<String>(objMap.keySet());
               for (Integer i = 0; i < destObjs.size(); i++){
                    
                    String destinationObj = destObjs[i];
                    for(sObject s : scope){
                        sObject sObj = Schema.getGlobalDescribe().get(destinationObj).newSObject(); 
                        
                        List<In_Form__DL_Field_Mapping__c> fieldList = objMap.get(destinationObj);
                        
                        Boolean recordType = false;
                        
                        for(In_Form__DL_Field_Mapping__c obj : fieldList){
                            
                            String recordIds = obj.In_Form__Record_Type__c;
                            String recordId ='';
                            
                            if(checkRecordType){
                                
                                recordId = String.valueof(s.get('RecordTypeId'));
                            } 
                            
                            if(obj.In_Form__Record_Type_Selection__c == 'Specific Record Type' && recordIds.contains(recordId)){
                            
                               sObj.put(obj.In_Form__Object_Field_Name__c,s.get(obj.In_Form__Source_Field__c)); 
                                 
                            } else if(obj.In_Form__Record_Type_Selection__c == 'All Type Selection'){
                               
                               sObj.put(obj.In_Form__Object_Field_Name__c,s.get(obj.In_Form__Source_Field__c)); 
                            
                            } else {
                            
                                recordType = true;
                            }    
                        } 
                        
                        if(recordType){
                            continue;
                        }
                        
                        if(!fieldList.isEmpty()){
                            sObj.put('In_Form__DL_Batch_Name__c',batchDLId);
                            objList.add(sObj);
                        }
                    }
                    
                } 
            }
            
            //List<String> fieldList=new List<String>{'In_Form__Object_Field_Name__c','In_Form__DL_Batch_Name__c'};
            //&& InForm_CRUDFLS_Util.getCreateAccessCheck(objName,fieldList)
            if(!objList.isEmpty() ){
                List<Database.SaveResult> srList = Database.insert(objList,false);//Database method to update the records in List               
                
                // Iterate through each returned result by the method
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                        // This condition will be executed for successful records and will fetch the ids of successful records
                        sharingIds.add(sr.getId());
                    }
                    else {
                        // This condition will be executed for failed records
                        for(Database.Error objErr : sr.getErrors()) {
                            if(status == 'Processing'){
                                status = objName+objErr.getMessage();
                            }  
                        }
                    }  
                }                  
            }
        } catch(Exception e){
            
            if(status == 'Processing'){
              status = objName+e.getMessage(); 
            }
            System.debug('Exception :'+e.getMessage());
        }       
    } 
    
    //finish method to send httpRequest to set batchStatus
    global void finish(Database.BatchableContext BC){  
        try
        {
        if(status == 'Processing'){            
            orderList.remove(0);            
            if(!orderList.isEmpty()){
                Database.executeBatch(new BatchToCreateDLRecord(orderList,batchDLId,status,sharingIds),1);
            }            
            if(orderList.isEmpty() && status=='Processing'){
                //List<String> fieldList=new List<String>{'In_Form__Batch_Id__c','In_Form__DL_Batch_Name__c'};
                
                In_Form__DL_Batch__c dlbatchRecord = new In_Form__DL_Batch__c();
                
                //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__DL_Batch__c',fieldList)){
                    dlbatchRecord = [SELECT In_Form__Batch_Id__c FROM In_Form__DL_Batch__c WHERE Id =: String.escapeSingleQuotes(batchDLId)];
                //}
                
                In_Form__ClientInformation__c clientInfo = In_Form__ClientInformation__c.getOrgDefaults();
 
                String endPt = clientInfo.In_Form__Status_URL__c;
          
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(endPt+'?batchStatus=Completed&batchId='+dlbatchRecord.In_Form__Batch_Id__c);
                
                HttpResponse res = http.send(req);
                
                In_Form__DL_Batch__c dlbatch = new In_Form__DL_Batch__c(id=batchDLId);
                dlbatch.In_Form__Batch_Status__c = 'Completed';
                    
                //fieldList.clear();
                
                //fieldList=new List<String>{'In_Form__Batch_Status__c'};
                
                //if(InForm_CRUDFLS_Util.getUpdateAccessCheck('In_Form__DL_Batch__c',fieldList)){
                    update dlbatch;   
                //}
                  
                if(sharingIds.isempty()==false){          
                    Database.executeBatch(new BatchToCreateConnectionRecordsNew(sharingIds)); 
                }
                if(batchDLId<>null){
                    Database.executeBatch(new BatchDLFieldMapping(batchDLId)); 
                }              
            }
            
            
        } 
        else 
        { 

            List<String> fieldList=new List<String>{'In_Form__Batch_Id__c'};        
        
            In_Form__DL_Batch__c dlbatchRecord = new In_Form__DL_Batch__c();
            
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__DL_Batch__c',fieldList)){
                dlbatchRecord = [SELECT In_Form__Batch_Id__c FROM In_Form__DL_Batch__c WHERE Id =: String.escapeSingleQuotes(batchDLId)];
            //}
            In_Form__ClientInformation__c clientInfo = In_Form__ClientInformation__c.getOrgDefaults();
 
            String endPt = clientInfo.In_Form__Status_URL__c;
      
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint(endPt+'?batchStatus=Error&batchId='+dlbatchRecord.In_Form__Batch_Id__c);
            
            HttpResponse res = http.send(req);
            fieldList.clear();
                
            //fieldList=new List<String>{'In_Form__Batch_Status__c','In_Form__Error_Description__c'};
            
            //if(InForm_CRUDFLS_Util.getUpdateAccessCheck('In_Form__DL_Batch__c',fieldList)){

                In_Form__DL_Batch__c dlbatch = new In_Form__DL_Batch__c(id=batchDLId);
                dlbatch.In_Form__Batch_Status__c = 'Error';
                dlbatch.In_Form__Error_Description__c = status;
                
                update dlbatch;
            //}
        }    
    }
    catch(Exception e)
    {
        System.debug('Exception :'+e.getMessage());
    }       
    }
    
}