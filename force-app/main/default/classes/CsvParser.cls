/*------------------------------------------------------------------
/* Created By : Saurabh Gupta,
 * Created date : 13/01/2016 05:41
 * Last Modified by::  Saurabh Gupta ,  27/09/2017 11:40
 * Description: CSV Parser Class
------------------------------------------------------------------*/
public with Sharing class CsvParser {

    //Method to Parse Content
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        try{
            List<List<String>> allFields = new List<List<String>>();
            contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
            contents = contents.replaceAll('""','DBLQT');
            List<String> lines = new List<String>();
            try {
                lines = contents.split('\n');
            } 
            catch (System.ListException e) {
                System.debug('Limits exceeded?' + e.getMessage());
            }
            Integer num = 0;
            
            for(String line : lines){
                if (line.replaceAll(',','').trim().length() == 0) 
                    break;
                List<String> fields = line.split(',');  
                List<String> cleanFields = new List<String>();
                String compositeField;
                Boolean makeCompositeField = false;
                for(String field : fields) {
                    if (field.startsWith('"') && field.endsWith('"')) {
                        cleanFields.add(field.replaceAll('DBLQT','"'));
                    } 
                    else if (field.startsWith('"')) {
                        makeCompositeField = true;
                        compositeField = field;
                    } 
                    else if (field.endsWith('"')) {
                        compositeField += ',' + field;
                        cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                        makeCompositeField = false;
                    } 
                    else if (makeCompositeField) {
                        compositeField +=  ',' + field;
                    } 
                    else {
                        cleanFields.add(field.replaceAll('DBLQT','"'));
                    }
                }
                allFields.add(cleanFields);
            }
            if (skipHeaders) allFields.remove(0);
            return allFields;
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
            return null;
        }      
        return null;
    }
}