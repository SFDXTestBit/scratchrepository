global with sharing class amountTypeWrapper{

    @AuraEnabled
    public String type{get;set;}
    
    @AuraEnabled
    public Double amount{get;set;}

}