/*------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 31/01/2017 07:08
 * Last Modified by:: Saurabh Gupta ,  27/09/2017 11:46
 * Description: FieldMapping class for lightning component
 history : v1.0 - 31/10/2018 - Changes For Security Review
------------------------------------------------------------------*/
public with sharing class FieldMappingCntrl {

    //AuraEnabled method to return dl field mapping data whose visible field is true 
    @AuraEnabled
    public static List<In_Form__DL_Field_Mapping__c> getFieldMappings(){
        
            /*List<String> fieldList=new List<String>{
            'Id',
            'Name',
            'In_Form__Description__c',
            'In_Form__Mapping_Status__c',
            'In_Form__Object_Field_Name__c',
            'In_Form__Object_Field_Type__c',
            'In_Form__Object_Name__c',
            'In_Form__Source_Field__c',
            'In_Form__Source_Field_Label__c',
            'In_Form__Source_Field_Type__c',
            'In_Form__Source_Object__c',
            'In_Form__Record_Type_Selection__c', 
            'In_Form__Record_Type__c'
            };*/
            
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__DL_Field_Mapping__c',fieldList)){
                return [SELECT Id, Name,In_Form__Description__c, In_Form__Mapping_Status__c, In_Form__Object_Field_Name__c, 
                            In_Form__Object_Field_Type__c, In_Form__Object_Name__c, In_Form__Source_Field__c,In_Form__Source_Field_Label__c,
                            In_Form__Source_Field_Type__c, In_Form__Source_Object__c, In_Form__Record_Type_Selection__c,
                            In_Form__Record_Type__c
                        FROM In_Form__DL_Field_Mapping__c 
                        WHERE In_Form__Visible__c =: true
                        ORDER BY In_Form__Object_Name__c ASC];
            //}
            //return null;
        
    }
    
    //AuraEnabled method to return dl field mapping object name whose visible field is true 
    @AuraEnabled
    public static List<String> getObjectName(){
          Set<String> objSet = new Set<String>();
            List<String> setList=null;
            /*List<String> fieldList=new List<String>{
                'Id',
                'Name',
                'In_Form__Description__c',
                'In_Form__Mapping_Status__c',
                'In_Form__Object_Field_Name__c', 
                'In_Form__Object_Field_Type__c',
                'In_Form__Object_Name__c',
                'In_Form__Source_Field__c',
                'In_Form__Source_Field_Label__c', 
                'In_Form__Source_Field_Type__c',
                'In_Form__Source_Object__c',
                'In_Form__Record_Type_Selection__c', 
                'In_Form__Record_Type__c'};*/
        
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__DL_Field_Mapping__c',fieldList)){
                List<In_Form__DL_Field_Mapping__c> fieldMappingList = [SELECT Id, Name,In_Form__Description__c, In_Form__Mapping_Status__c, In_Form__Object_Field_Name__c, 
                            In_Form__Object_Field_Type__c, In_Form__Object_Name__c, In_Form__Source_Field__c,In_Form__Source_Field_Label__c,
                            In_Form__Source_Field_Type__c, In_Form__Source_Object__c, In_Form__Record_Type_Selection__c,
                            In_Form__Record_Type__c
                        FROM In_Form__DL_Field_Mapping__c 
                        WHERE In_Form__Visible__c =: true
                        ORDER BY In_Form__Object_Name__c ASC];
                        
                for(In_Form__DL_Field_Mapping__c fM : fieldMappingList ){
                    objSet.add(fM.Object_Name__c);
                }
                
                setList = new List<String>();
                setList.addAll(objset);
            //}
            return setList;
       
    }
    
    //AuraEnabled method to return dl field mapping data to update fields value 
    @AuraEnabled
    public static String savedFieldMapping(List<In_Form__DL_Field_Mapping__c> updateFieldList) { 
        String errorList = '';
        try{ 
            for(In_Form__DL_Field_Mapping__c fieldMap : updateFieldList){
            }      
            //if(InForm_CRUDFLS_Util.getUpdateAccessCheck('In_Form__DL_Field_Mapping__c',new List<String>())){  
                update updateFieldList;
            //}
        }
        catch(Exception e){errorList = e.getMessage();
        }
        
        return errorList;
    }
    
    //AuraEnabled method to return dl field mapping object name without standard object whose visible field is true 
    @AuraEnabled
    public static List<SObjectResult> getSObjectName() {
        
        List<SObjectResult> objectList = new List<SObjectResult>(); 
        
        try
        {
        
            for(Schema.DescribeSObjectResult res : Schema.describeSObjects(new List<String>(Schema.getGlobalDescribe().keyset()))) { 
                if(res.getName() == 'Account' || res.getName() == 'Contact' || (res.getName().endsWith('__c') && res.getName().startsWith(Label.Org_Namespace) && !res.getName().contains('_DL_'))) {
                    
                    List<Schema.RecordTypeInfo> recTypeSchema = res.getRecordTypeInfos();

                    List<RecTypes> recordTypes = new List<RecTypes>();
                    
                    for(Schema.RecordTypeInfo recTypeInfo : recTypeSchema){
                        if(recTypeInfo.isAvailable() && recTypeInfo.getName() != 'Master'){
                            recordTypes.add(new RecTypes(recTypeInfo.getRecordTypeId(), recTypeInfo.getName()));
                        }
                    }

                    List<SObjectFields> sObjectFieldsList = new List<SObjectFields>();
                    for (String fieldName: res.fields.getMap().keySet()) {  
                        
                        sObjectFieldsList.add(new SObjectFields(String.valueOf(res.fields.getMap().get(fieldName).getDescribe().getType()), fieldName, res.fields.getMap().get(fieldName).getDescribe().getLabel()));
                    }
                                   
                    objectList.add(new SObjectResult(res.getLabel(), res.getName(), sObjectFieldsList, recordTypes));
                }
            }
        }
        catch(Exception e)
        {
            System.debug('Exception :'+e.getMessage());
        }
        
        return objectList;
    }
    
    //Wrapper class
    public class SObjectResult{
        @AuraEnabled
        public String name{get;set;}
        @AuraEnabled
        public String apiName{get;set;}
        @AuraEnabled
        public List<SObjectFields> sObjectFields{get;set;}
        @AuraEnabled
        public List<RecTypes> recordTypes{get;set;}

        public SObjectResult(String name, String apiName, List<SObjectFields> sObjectFields, List<RecTypes> recordTypes){
            this.name = name;
            this.apiName = apiName;
            this.sObjectFields = sObjectFields;
            this.recordTypes = recordTypes;
        }
    }
    
    //Wrapper class for fields api
    public class SObjectFields{
        @AuraEnabled
        public String type{get;set;}
        @AuraEnabled
        public String apiName{get;set;}
        @AuraEnabled
        public String label{get;set;}        
        
        public SObjectFields(String type, String apiName, String label){
            this.type = type;
            this.apiName = apiName;
            this.label = label;
        }
    }
    
    //Wrapper class for record type
    public class RecTypes{
        @AuraEnabled
        public String apiName{get;set;}
        @AuraEnabled
        public String label{get;set;}   
        
        public RecTypes(String apiName, String label){
            this.apiName = apiName;
            this.label = label;
        }
    }
}