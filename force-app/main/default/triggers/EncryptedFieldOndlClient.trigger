/*------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 06/02/2017 09:19
 * Last Modified by:: Saurabh Gupta,  27/09/2017 12:39
 * Description: Trigger to excute EncryptedFieldHandler class
------------------------------------------------------------------*/
trigger EncryptedFieldOndlClient on In_Form__DL_Client__c(before insert, before update) {
    if(trigger.isInsert){      
        EncryptedFieldHandler.encryptDataOnInsert(Trigger.new);
    }    
    if(trigger.isUpdate){       
        EncryptedFieldHandler.encryptDataOnUpdate(Trigger.newMap,Trigger.oldMap);
    }   
}