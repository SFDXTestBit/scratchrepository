/*--------------------------------------------------------------------
 * Created By : Dev Team Phiz,  09/08/2015 12:51
 * Last Modified By Dev Team Phiz,  20/02/2017 13:06
 * Description: Trigger to execute TransactionTotalUpdationBatch class
 * History: v1.1 - Sneha U. - 30/10/2018 - Removed Commented Code, Added Try Catch,Added Read Access check
 * 			v1.2 - Extentia  - 10th june 2019 - added the Isbulk condition to controll the rogue transactions.
----------------------------------------------------------------------*/

trigger TransactionTrigger on Transaction__c (after insert, after update) {
    if(Trigger.IsAfter){
        if(Trigger.IsUpdate || Trigger.IsInsert){
            //if(!(System.isBatch() || System.isScheduled())){
        // Added by Ajay on 02/14 for adding own static variable for System.isbatch
            if(systemBatchHelper.isBatchRunning==false){
                try{
                    Set<Id> tenancyIds = new Set<Id>();
                    
                    for(Transaction__c transactions : Trigger.New){
                        tenancyIds.add(transactions.Tenancy__c);
                        
                        
                        if(transactions.Isbulk__c == true && systemBatchHelper.isBulk == false)
                        {
                            systemBatchHelper.isBulk =true;
                            
                        }   // added by extentia on 10 june 2019 for finding rogue trnasacions

                        
                    }
                    
                    //List of fields assigned for Transaction__c Object read access
                    /*List<String> fieldList = new List<String>{
                        'In_Form__Tenancy__c',
                        'In_Form__Tenancy__r.In_Form__Balance_are_up_to_Date__c',
                        'In_Form__Transaction_Date__c'
                    };*/ 

                    //Check read access for Tenancy Object
                    //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction__c',fieldList) ){
                        if(!TenancyTriggerUtility.hasRunTransaction ){
                            
                            TenancyTriggerUtility.hasRunTransaction = true;
                            
                            for(Transaction__c trans : [SELECT Id, Tenancy__c, Tenancy__r.Balance_are_up_to_Date__c FROM Transaction__c WHERE Id IN: Trigger.New]){
                                if(!trans.Tenancy__r.Balance_are_up_to_Date__c){   
                                    Transaction__c actualRecord = Trigger.newMap.get(trans.Id);          
                                    actualRecord.Tenancy__c.addError(Label.Balance_Error);
                                    return;
                                }
                                
                            }
                        }
                        
                        
                        Date startDate = Date.today();

                        for(Transaction__c trans : [SELECT Transaction_Date__c, Tenancy__r.Balance_are_up_to_Date__c  FROM Transaction__c WHERE Tenancy__c IN: tenancyIds ORDER BY Transaction_Date__c LIMIT 1]){
                            
                            startDate = trans.Transaction_date__c.date();
                        }
                        
                        Database.executeBatch(new TransactionTotalUpdationBatch(tenancyIds, startDate),5);
                    //}
                }
                catch(DMLException e){
                    for (Transaction__c objTransaction: Trigger.New) {
                        objTransaction.addError('There was a problem inserting/updating the record');
                    }
                }
            }
        }
    }
}