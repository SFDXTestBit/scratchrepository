/*------------------------------------------------------------------
/* Created By : 
 * Created date : 12/8/2015
 * Description: Rent Stream Charge Trigger
 * History: v1.2 - Sneha U. - 31/10/2018 - Added Try Catch,Read access check
------------------------------------------------------------------*/
trigger RentStreamChargeTrigger on Rent_Stream_Charge__c (after insert,after update) {
    if(Trigger.IsAfter){
        if(Trigger.IsInsert || Trigger.IsUpdate){
            try{
                List<Id> idList = new List<Id>();
                String rentChargeRecordTypeId = Schema.SObjectType.Transaction__c.getRecordTypeInfosByName().get('Rent charge').getRecordTypeId();
                
                //List of fields assigned for Rent_Stream_Charge__c Object read access
                /*List<String> fieldListRent = new List<String>{
                    'In_Form__Rent_Schedule__r.In_Form__Bedspace__c',
                    'In_Form__Rent_Schedule__r.In_Form__Tenancy__c'
                };*/ 

                //List of fields assigned for Tenancy__c Object read access
                /*List<String> fieldListTenancy = new List<String>{
                    'In_Form__Balance_are_up_to_Date__c'
                };*/ 

                //Method to update transaction related to Rent Stream
                /*&&
                    InForm_CRUDFLS_Util.getReadAccessCheck(String.valueOf(Schema.SObjectType.Rent_Stream_Charge__c.getSobjectType()),fieldListRent) && 
                    InForm_CRUDFLS_Util.getReadAccessCheck(String.valueOf(Schema.SObjectType.Tenancy__c.getSobjectType()),fieldListTenancy) */
                //if(!System.isBatch() ){
                 // Added by Ajay on 02/14 for adding own static variable for System.isbatch
                if(systemBatchHelper.isBatchRunning==false){
         
                    List<Rent_Stream_Charge__c> rscList = [SELECT Id, Rent_Schedule__r.Bedspace__c, Rent_Schedule__r.Tenancy__c FROM Rent_Stream_Charge__c WHERE Id IN : Trigger.New];
                   
                    for(Rent_Stream_Charge__c rsC : rscList){
                        idList.add(rsC.Rent_Schedule__r.Tenancy__c);
                    }

                    List<Tenancy__c> tenList = [Select Id,Balance_are_up_to_Date__c FROM Tenancy__c WHERE Bedspace__c IN: idList];
                    
                    UPDATE tenList;
                    
                    Database.executeBatch(new RentStreamUpdateTransactionsBatch (rscList),2000);
                }
            }
            catch(DMLException e){
                for (Rent_Stream_Charge__c objRent: Trigger.New) {
                    objRent.addError('There was a problem inserting/updating the record');
                }
            }
        }
        
    }
}