/*------------------------------------------------------------------
/* Created By : Saurabh Gupta,  
 * Created date : 08/02/2017 12:41
 * Last Modified by::Saurabh Gupta,  27/09/2017 12:37
 * Description: Trigger to excute batchBatchContactDLScope
------------------------------------------------------------------*/
trigger DLBatchTrigger on In_Form__DL_Batch__c (after insert) {    
    for(In_Form__DL_Batch__c obj: trigger.new){     
        Database.executeBatch(new BatchContactDLScope(obj.Id),200);         
    }    
 }