<apex:page standardController="Session__c"
    standardStylesheets="false"  
    showheader="false"
    sidebar="false"
    applyBodyTag="false"
    applyHtmlTag="false"
    cache="false"
    docType="html-5.0"
    title="Create Actions">
<html lang="en">
<head> 
<title>Create Actions</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<apex:stylesheet value="{!$Resource.reset_css}" />
<apex:stylesheet value="{!URLFOR($Resource.bootstrap_3_3_4_dist, '/bootstrap-3.3.4-dist/css/bootstrap.min.css')}" />
<apex:stylesheet value="{!$Resource.actions_css}" />
<apex:remoteObjects jsNamespace="RemoteObjectModel">
    <apex:remoteObjectModel name="RecordType" jsShorthand="rt" fields="Id,Name,DeveloperName,SobjectType,IsActive"/>
    <apex:remoteObjectModel name="In_Form__Session__c" jsShorthand="session" fields="Id,IsDeleted,In_Form__Project__c,In_Form__Project_name__c,In_Form__Start_date_time__c,In_Form__End_date_time__c,In_Form__Record_type_name__c" />
    <apex:remoteObjectModel name="In_Form__Action__c" jsShorthand="actions" fields="Id,IsDeleted,RecordTypeId,In_Form__Client__c,In_Form__Session__c,In_Form__Timeline_Event__c" />
    <apex:remoteObjectModel name="In_Form__Timeline_Event__c" jsShorthand="timelines" fields="Id,IsDeleted,CreatedDate,In_Form__Status__c,In_Form__Project_Lookup__c,In_Form__Client__c,In_Form__Client_name__c,In_Form__Client_first_name__c,In_Form__Client_last_name__c,In_Form__Client_nickname__c,In_Form__Client_gender__c" />       
</apex:remoteObjects>
<script>
    $j = jQuery.noConflict();
    $j(document).ready(function(){    
        // initialize the user interface
        HOMELESSLINKACTIONS.initUI('<apex:outputText value="{!JSENCODE(Session__c.Id)}" />',
                                   '<apex:outputText value="{!JSENCODE(Session__c.Project__c)}" />',
                                   '<apex:outputText value="{!JSENCODE(Session__c.RecordType.DeveloperName)}" />',
                                   new RemoteObjectModel.actions(),
                                   new RemoteObjectModel.session(),
                                   new RemoteObjectModel.timelines(),
                                   new RemoteObjectModel.rt(),
                                   $j,
                                   [<apex:outputText value="{!$Setup.Timeline_statuses_for_sessions__c.Statuses__c}" />],
                                   ['Open', 'Referral', 'Referral accepted'],
                                   'Something has gone wrong. Please contact In-Form support with the message below:',
                                   'You cannot create actions from this page because this project has more than ',
                                   'current timeline events.',
                                   'You cannot create actions from this page because this session has more than',
                                   'actions',
                                   'This session\'s information cannot be retrieved as it has over 2000 attendees.',
                                   'No actions were created.',
                                   'You are viewing <strong>all clients</strong> with <strong>current timeline events</strong> at <strong>',
                                   '</strong>',
                                   'You are viewing <strong> clients</strong> who (1) attended the <strong>selected session</strong>, (2) have <strong>current timeline events at ',
                                   '</strong> and (3) have <strong>not previously been added</strong> to this session.',
                                   'Click anywhere on this row to remove this client.',
                                   'Click anywhere on this row to add this client.',
                                   'You can only add ',
                                   ' clients at a time.',
                                   'Show more',
                                   'Add',
                                   'Add all',
                                   'First name',
                                   'Last name',
                                   'Nickname',
                                   'Gender',
                                   'Timeline status',
                                   'Show All',
                                   'No clients found',
                                   'Remove all',
                                   'Added Clients',
                                   'Added Client',
                                   'No errors',
                                   'No added clients',
                                   'Name',
                                   'Error message',
                                   'Name',
                                   'Action');
    });  
</script>        
</head>
    <body>    
    <div class="container">
    <div class="app-container">
        <div id="navigation-buttons" class="row">
            <p class="text-right">
            <apex:outputLink value="/{!Session__c.Id}" target="_parent"><small>&laquo;&nbsp;Back to session</small></apex:outputLink>&nbsp;|
            <a id="createActionsHelpLink" target="_blank"><small>Help</small></a>&nbsp;|&nbsp;
            <apex:outputLink value="?id={!Session__c.Id}" target="_parent"><small>Start again in full screen mode&nbsp;&raquo;</small></apex:outputLink>&nbsp;
            </p>
        </div> 
        <div id="title" class="row"><h2><p class="text-center">Create Actions for multiple Clients</p></h2></div>
        <div id="help-container" class="row text-center">
            <img id="help-img" src="{!$Resource.In_Form__actions_help}" />
        </div>
        <div id="filter-container" class="row">
            <div id="filter-inner" class="row">
                <p class="text-center">
                    <select id="filter"></select>
                    <img id="find-spinner-img" src="{!$Resource.In_Form__spinner}" />
                    <button id="find-button" type="button" class="btn btn-primary btn-xs">Find</button>
                </p>                
            </div>         
            <div class="row">
                <p class="text-center">
                <small>
                    <strong>Session:&nbsp;</strong><apex:outputText value="{!Session__c.Name}"/>&nbsp;
                    <strong>Project:&nbsp;</strong><apex:outputText value="{!Session__c.In_Form__Project__r.Name}"/><br />
                    <strong>Subject:&nbsp;</strong><apex:outputText value="{!Session__c.Subject__c}"/><br />
                    <strong>Type:&nbsp;</strong><apex:outputText value="{!Session__c.Record_type_name__c}" />
                </small>     
                </p>
            </div>
            <div id="data-epicycle-container" class="row">
                <small>
                    <div id="data-epicycle" class="alert alert-info text-center" role="alert"></div>
                </small>
            </div>       
        </div>
        <div id="clients-container" class="row container-min-20em">
            <div class="tl-container-name"><h3><p class="text-center">Clients</p></h3></div>
            <div id="clients-table-container"></div>
        </div>
        <div id="added-clients-container" class="row container-min-20em">
            <div class="tl-container-name"><h3><p id="added-clients-label" class="text-center">Added Clients</p></h3></div>
            <div id="added-table-container"></div>   
        </div>
        <div id="actions-container" class="row container-min-20em">
            <div class="tl-container-name"><h3><p class="text-center">Successfully created Actions</p></h3></div>
            <div id="created-table-container"></div>   
        </div>
        <div id="errors-container" class="row container-min-20em">
            <div class="tl-container-name"><h3><p class="text-center">Unable to create these Actions</p></h3></div>
            <div id="error-message-container"></div>
            <div id="error-table-container"></div>   
        </div>
        <div id="progress-bar" class="row"><p class="text-center"><img id="progress-bar-img" src="{!$Resource.In_Form__loading}" /></p></div>
        <div id="progress-bar-loading-ui" class="row">
            <div class="progress">
                <div id="progress-bar-loading-ui-widget" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                    <span class="sr-only">10% Complete</span>
                </div>
            </div>        
        </div>
        <div id="progress-data" class="row"></div>  
        <div id="progress-count-container" class="row"><p id="progress-count" class="text-center"></p></div>
        <apex:outputPanel rendered="{!$Profile.Name=='System Administrator'}" styleClass="consoleLogger" onclick="HOMELESSLINKACTIONS.consoleLogger()">.</apex:outputPanel>  
        <script>
            <apex:includeScript value="{!$Resource.jquery_1_11_2_min_js}"/>
            <apex:includeScript value="{!$Resource.remote_objects_actions_min_js}"/>
        </script>
    </div>
    </div><!-- container -->
    </body>
</html> 
</apex:page>