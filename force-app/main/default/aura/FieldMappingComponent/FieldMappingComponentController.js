({
    doInit : function(component, event, helper) {
        var action = component.get('c.getFieldMappings');
        action.setCallback(this, function(response){
            var state = response.getState();

            if (state === 'SUCCESS') {
                var fieldMapList = response.getReturnValue();
                component.set('v.fieldMappingList', fieldMapList);

                var fetchSObjects = component.get('c.getSObjectName');
                fetchSObjects.setCallback(this, function(response) {
                    var state = response.getState();            
                    
                    if (state === 'SUCCESS') {  

                        var sObjList = response.getReturnValue();

                        var objFieldMap = {};
                        var objRecTypeMap = {};
                        
                        for(var i = 0; i < fieldMapList.length; i++){
                            var opts = [];
                            
                            opts.push({label:'--None--', value:'', selected:true});

                            for (var j = 0; j < sObjList.length; j++) {

                                var isSelected = fieldMapList[i].In_Form__Source_Object__c == sObjList[j].apiName?true:false;
                                opts.push({ label: sObjList[j].name, value: sObjList[j].apiName, selected:isSelected });                    
                                
                                var fieldOpts = [];
                                fieldOpts.push({label:'--None--', value:'', selected:true});

                                objFieldMap[sObjList[j].apiName] = sObjList[j].sObjectFields;

                                objRecTypeMap[sObjList[j].apiName] = sObjList[j].recordTypes;


                                if(isSelected){

        							var fields = sObjList[j].sObjectFields;
                                    for (var k = 0; k < fields.length; k++) {
                                        
                                        var isFieldSelected = fieldMapList[i].In_Form__Source_Field__c == fields[k].apiName?true:false;        
                                        
                                        fieldOpts.push({ label: fields[k].label, value: fields[k].apiName, selected:isFieldSelected });     
                                        
                                        if(isFieldSelected){
                                            component.find('fieldLabel')[i].set('v.value', fields[k].apiName);
                                            component.find('fieldType')[i].set('v.value', fields[k].type);
                                        }
                                    }
        
                                    component.find('Fields')[i].set('v.options', fieldOpts);
                                }
                            }

                            component.find('Role')[i].set('v.options', opts);
                        }
                        component.set('v.sObjectMap', objFieldMap);
                        component.set('v.recTypeMap', objRecTypeMap);

                        helper.recTypeSelect(component, event, helper);

                    }else {
                        
                    }
                    $(component.find('multiSelectObjFilter').getElement()).multiselect({
                        onChange: function(option, checked){
                            component.filterObject(component,event,helper);
                        },
                        numberDisplayed: 1,
                        includeSelectAllOption: true,
                        disableIfEmpty: true
                    });
                    
                });
                $A.enqueueAction(fetchSObjects);
            }
            else {
                
            }
        });
        $A.enqueueAction(action);
    },
    filterObject : function(component,event,helper){
        var fieldMappingList = component.get('v.fieldMappingList');
        var selectedOptions = '';
        
        $.each($(component.find('multiSelectObjFilter').getElement()).find('option:selected'), function(){
            if(selectedOptions == ''){
                selectedOptions = $(this).val();
            }
            else{
                selectedOptions += ',' + $(this).val();
            }
        });
        
        for(var i = 0; i < fieldMappingList.length; i++){
            if(selectedOptions.indexOf(fieldMappingList[i].In_Form__Object_Name__c) < 0){
                $($('.objRows')[i]).hide();
            }
            else{
                $($('.objRows')[i]).show();
            }
        }
    },
    scriptsLoaded : function(component,event,helper){    
        
        var obj = component.get('c.getObjectName');
        obj.setCallback(this, function(response){
            var state = response.getState();
             if (state === 'SUCCESS') {
                var objMapList = response.getReturnValue();     

                var opts = [];

                for(var i = 0; i < objMapList.length; i++){
                    opts.push({ label: objMapList[i], value: objMapList[i], selected: true});
                }

                component.find('multiSelectObjFilter').set('v.options', opts);
                
            }
        });
        $A.enqueueAction(obj);
    },
    onObjectSelect: function(component, event, helper) { 
        
        var fieldMapList = component.get('v.fieldMappingList');       

        for (var i = 0; i < fieldMapList.length; i++) {
            var fields = component.get("v.sObjectMap")[fieldMapList[i].In_Form__Source_Object__c];

            var fieldOpts = [];
            fieldOpts.push({label:'--None--', value:'', selected:false});
			
            if(fields != undefined){
            for (var k = 0; k < fields.length; k++) {

                var isFieldSelected = fieldMapList[i].In_Form__Source_Field__c == fields[k].apiName?true:false;
    
                fieldOpts.push({ label: fields[k].label, value: fields[k].apiName, selected:isFieldSelected });     
                if(isFieldSelected){
                    component.find('fieldLabel')[i].set('v.value', fields[k].apiName);
                    component.find('fieldType')[i].set('v.value', fields[k].type);
                }
            }
        }
            component.find('Fields')[i].set('v.options', fieldOpts);
        }   
        
    },    
    onRecTypeSelect: function(component, event, helper) {
        helper.setRecordTypeValues(component, event, helper);
        setInterval(function() {
            if(true){
                $('.recordTypeMultiSelect').multiselect('rebuild');
            }
        },400);
    }, 
    updateRecord : function(component, event, helper) {

        var fieldMapList = component.get('v.fieldMappingList');       

        var recordTypeSelectedValues = $('.recordTypeMultiSelect');

        for (var i = 0; i < fieldMapList.length; i++) {
            var selectedRecordTypes = $(recordTypeSelectedValues[i]).val();
            
            if(selectedRecordTypes){
                fieldMapList[i].In_Form__Record_Type__c = selectedRecordTypes.join();
            }
        }   

        var action = component.get('c.savedFieldMapping');  
        action.setParams({updateFieldList:fieldMapList});    

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {

                if(response.getReturnValue() != null && response.getReturnValue() != '' && response.getReturnValue() != undefined){
                	var error = response.getReturnValue();
                   
                    var ErrorLength=error.length;
                        component.set("v.message", error.substring(error.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')+34,ErrorLength).replace('&amp;', '&'));
                        component.set("v.isVisible", false); 
                }
                else{
                	component.set("v.isVisible", true); 
                    component.set("v.isSuccess", true); 
                }
            }else {
                
            }
        });
        $A.enqueueAction(action);
    },     
    setLabelAndType: function(component, event, helper) { 
        var fieldMapList = component.get('v.fieldMappingList');  

        for (var i = 0; i < fieldMapList.length; i++) {
            var fields = component.get("v.sObjectMap")[fieldMapList[i].In_Form__Source_Object__c];

            var fieldOpts = [];

            for (var k = 0; k < fields.length; k++) {
                var isFieldSelected = fieldMapList[i].In_Form__Source_Field__c == fields[k].apiName?true:false;
    
                fieldOpts.push({ label: fields[k].label, value: fields[k].apiName, selected:isFieldSelected });     
                if(isFieldSelected){
                    component.find('fieldLabel')[i].set('v.value', fields[k].apiName);
                    component.find('fieldType')[i].set('v.value', fields[k].type);
                }
            }
        }        

    }
})