({
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');     
        component.set("v.spinnerIsVisibile", "true");
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();
    },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        component.set("v.spinnerIsVisibile", "false");
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();
    },
    recTypeSelect : function (component, event, helper) {
        helper.setRecordTypeValues(component, event, helper);

        setInterval(function() {
            if(true){
                $('.recordTypeMultiSelect').multiselect({
                    numberDisplayed: 1,
                    includeSelectAllOption: true,
                    disableIfEmpty: true
                });
            }
        },400);
    },
    setRecordTypeValues : function(component, event, helper){
        var fieldMapList = component.get('v.fieldMappingList');       
        
        for (var i = 0; i < fieldMapList.length; i++) {
            var recTypeOpts = [];
            var recTypes = component.get("v.recTypeMap")[fieldMapList[i].In_Form__Source_Object__c];
            
            for (var k = 0; k < recTypes.length; k++) {
                var isSelected = false;
                
                if(fieldMapList[i].In_Form__Record_Type__c){
                    isSelected = fieldMapList[i].In_Form__Record_Type__c.indexOf(recTypes[k].apiName) > -1?true:false;
                }
                recTypeOpts.push({ label: recTypes[k].label, value: recTypes[k].apiName, selected:isSelected });          
            }

            component.find('recType')[i].set('v.options', recTypeOpts);
        }
    }
})