({
    doInit: function(component, event, helper) {
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
        // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        helper.fetchPickListVal(component, 'In_Form__Void_status__c', 'accIndustry');
        helper.fetchPickListVal(component, 'In_Form__Void_reason__c', 'Voidreasonid'); 
        
        component.set("v.enddatetoday",false);
        component.set("v.Tenancy.In_Form__End_date__c",todayFormattedDate);
        var action = component.get("c.getUIThemeDescription");
        action.setCallback(this, function(a) {
            component.set("v.Name", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    closeModel: function(component, event, helper) {
        window.top.location.href='/'+component.get("v.recordId");
    },
    saveREcords : function(component, event, helper) {
        component.set("v.showSpinner", true); 
        component.set("v.errorMessage",'');
        component.set("v.enddatetoday",false);
        
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
        // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        
        if(component.get("v.Tenancy.In_Form__Start_date__c") > todayFormattedDate){
            component.set("v.errorMessage",'Start date cannot be a future date.');
            component.set("v.showSpinner", false);
            return;
        }
        if(component.get("v.Tenancy.In_Form__End_date__c") > todayFormattedDate){
            component.set("v.errorMessage",'End Date must not be in the future.');
            component.set("v.showSpinner", false);
            return;
        }
        if(component.get("v.Tenancy.In_Form__End_date__c") < component.get("v.Tenancy.In_Form__Start_date__c")){
            component.set("v.errorMessage",'End Date must not before start Date.');
            component.set("v.showSpinner", false);
            return;
        }
        
        var action = component.get("c.save");

        action.setParams({
            "recordid" :  component.get("v.recordId"),
            "nights" : JSON.stringify(component.get("v.nights")),
            "tenency" : JSON.stringify(component.get("v.Tenancy"))
        });
        
        action.setCallback(this, function(a) {
           
            var state = a.getState(); 
            var returnValue = a.getReturnValue();  
            if (state == "SUCCESS") {
                if(returnValue == "Success"){
                    component.set("v.showSpinner", false);
                    if((typeof sforce != 'undefined') && sforce && (!!sforce.one)) {
                        sforce.one.navigateToSObject(component.get("v.recordId"));
                    }else{
                        window.top.location.href='/'+component.get("v.recordId");
                    }  
                    
                } else{
                    component.set("v.showSpinner", false);   
                    component.set("v.errorMessage",'There is some system issue please contact your system administrator');
                    
                }
            }else{
                component.set("v.showSpinner", false); 
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + errors[0].message);
                    }
                } 
            }
        });
        $A.enqueueAction(action);
        
    }
    
})