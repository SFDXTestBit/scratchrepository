({
    doInit : function(component, event, helper) {
        var action = component.get("c.checkfinancialData");
        
        action.setParams({
            "tenancyId" : component.get("v.recordId")
        })
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){			
                if(response.getReturnValue() == true){
                    component.set("v.errorMsg",true);
                }else{
                    component.set("v.errorMsg",false);
                    var actionRent = component.get("c.splitRent");
                    
                    actionRent.setParams({
                        "tenancyId" : component.get("v.recordId")
                    })
                    actionRent.setCallback(this, function(response){
                        var state = response.getState();
                        if(state == 'SUCCESS'){
                            component.set("v.splitRent",response.getReturnValue());
                        }
                        
                    });
                    $A.enqueueAction(actionRent);
                }
            }
        })
        
        $A.enqueueAction(action);
        
        var splitType = component.get("c.getTypes");
        
        splitType.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                component.set('v.splitTypeList', response.getReturnValue());
            }
            
        })
        
        $A.enqueueAction(splitType);
        helper.helperFun(component,event,'articleOne');
        helper.helperFun(component,event,'articleTwo');
    },
    
    
    sectionOne : function(component, event, helper) {
        helper.helperFun(component,event,'articleOne');
    },
    
    sectionTwo : function(component, event, helper) {
        helper.helperFun(component,event,'articleTwo');
    },
    cancelbutton :  function(component, event, helper){
        window.top.location.href='/'+component.get("v.recordId");
    },
    saveRecords :  function(component, event, helper){
        
        component.set("v.errorM",null);
        component.set("v.sucessMsg",null);
        if(component.get("v.splitRent.In_Form__Start_date__c") ==undefined){
            component.set("v.errorM",'Start date: You must enter a value');
            return;
        }
        if(component.get("v.splitRent.In_Form__End_date__c") ==undefined){
            component.set("v.errorM",'End date: You must enter a value');
            return;
        }

        if(component.get("v.splitRent.In_Form__Total_rent_charge__c")==undefined || component.get("v.splitRent.In_Form__Total_rent_charge__c")==''){
            component.set("v.errorM",'Total rent charge: You must enter a value');
            return;
        }
        var action = component.get("c.save");
        var cmpList = component.get("v.splitTypeList");
        
        if(cmpList !=undefined){
            for(var i=0;i<cmpList.length;i++){
                if(cmpList[i].amount.length==0){
                    component.set("v.errorM",'Rent Stream Charges: You must enter a value');
            		return;
                }
            }
            
        }
        action.setParams({
            "rentsch" : component.get("v.splitRent"),
            "amountType" : JSON.stringify(component.get("v.splitTypeList")),
            "tenancyId" : component.get("v.recordId")
        })
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                if(response.getReturnValue()=='Records created succesfully'){
                    component.set("v.sucessMsg",response.getReturnValue());
                    if((typeof sforce != 'undefined') && sforce && (!!sforce.one)) {
                        sforce.one.navigateToSObject(component.get("v.recordId"));
                    }else{
                        window.top.location.href='/'+component.get("v.recordId");
                    } 
                }else{
                    component.set("v.errorM",response.getReturnValue());
                }
            }
            
        })
        
        $A.enqueueAction(action);
        
    }    
})