<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Tenancy_Status_to_Current</fullName>
        <field>Status__c</field>
        <literalValue>Current</literalValue>
        <name>Update Tenancy Status to Current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>true</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Tenancy_Status_to_Historic</fullName>
        <field>Status__c</field>
        <literalValue>Historic</literalValue>
        <name>Update Tenancy Status to Historic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>true</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Status field to Current</fullName>
        <actions>
            <name>Update_Tenancy_Status_to_Current</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tenancy__c.Start_date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Tenancy__c.Start_date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Tenancy__c.End_date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When the Tenancy&apos;s Start date is in the past and the End date is blank, update the Status field to &apos;Current&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status field to Historic</fullName>
        <actions>
            <name>Update_Tenancy_Status_to_Historic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tenancy__c.Start_date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Tenancy__c.End_date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Tenancy__c.End_date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the Tenancy&apos;s Start date not blank and the End date is today or in the past, update the Status field to &apos;Historic&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
