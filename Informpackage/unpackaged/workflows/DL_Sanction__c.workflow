<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Dl_Participant_Batch</fullName>
        <field>DL_Participant_Batch__c</field>
        <formula>DL_Batch_Name__r.Batch_Id__c</formula>
        <name>Dl Participant Batch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update DL Participant Fields on DL Sanction</fullName>
        <actions>
            <name>Dl_Participant_Batch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>DL_Batch_Name__c &lt;&gt; NULL</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
