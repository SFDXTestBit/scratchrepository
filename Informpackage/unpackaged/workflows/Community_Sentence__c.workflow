<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Community_Sentence_to_current</fullName>
        <field>Status__c</field>
        <literalValue>Current</literalValue>
        <name>Update Community Sentence to current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Community_Sentence_to_historic</fullName>
        <field>Status__c</field>
        <literalValue>Historic</literalValue>
        <name>Update Community Sentence to historic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Community Sentence to current</fullName>
        <actions>
            <name>Update_Community_Sentence_to_current</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Community_Sentence__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Community service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Community_Sentence__c.Date_sentence_ends__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>For all Community Sentence records that are not of the Community Service record type, update the status to &apos;Current&apos; if there is no Date sentence ends and the Date sentence starts is today or in the past.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Community Sentence to historic</fullName>
        <actions>
            <name>Update_Community_Sentence_to_historic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Community_Sentence__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Community service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Community_Sentence__c.Date_sentence_ends__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Community_Sentence__c.Date_sentence_ends__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>For all Community Sentence records that are not of the Community Service record type, update the status to &apos;Historic&apos; on the Date sentence ends.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Community Sentence to historic if end date changed</fullName>
        <actions>
            <name>Update_Community_Sentence_to_historic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For all Community Sentence records that are not of the Community Service record type, update the status to &apos;Historic&apos; on the Date sentence ends if it changed</description>
        <formula>RecordType.Name &lt;&gt; &quot;Community Service&quot; &amp;&amp;
 ISPICKVAL( Status__c , &quot;Current&quot; ) &amp;&amp;
 ISCHANGED( Date_sentence_ends__c ) &amp;&amp;
 Date_sentence_ends__c &lt;= TODAY()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
