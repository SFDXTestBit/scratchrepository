<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_record_creator_assessment_approved_email</fullName>
        <description>Send record creator assessment approved email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>In_Form_Approval_Process_Templates/Assessment_approved</template>
    </alerts>
    <alerts>
        <fullName>Send_record_creator_assessment_rejected_email</fullName>
        <description>Send record creator assessment rejected email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>In_Form_Approval_Process_Templates/Assessment_rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_assessment_status_to_approved</fullName>
        <field>Form_status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update assessment status to approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_assessment_status_to_rejected</fullName>
        <field>Form_status__c</field>
        <literalValue>Rejected - changes required</literalValue>
        <name>Update assessment status to rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_assessment_status_to_submitted</fullName>
        <field>Form_status__c</field>
        <literalValue>Submitted for approval</literalValue>
        <name>Update assessment status to submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
