<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Client_Mailing_City</fullName>
        <field>MailingCity</field>
        <formula>Town_City__c</formula>
        <name>Update Client Mailing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Mailing_Country</fullName>
        <field>MailingCountry</field>
        <formula>TEXT( Country__c )</formula>
        <name>Update Client Mailing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Mailing_County</fullName>
        <field>MailingState</field>
        <formula>County__c</formula>
        <name>Update Client Mailing County</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Mailing_Postcode</fullName>
        <field>MailingPostalCode</field>
        <formula>Postcode__c</formula>
        <name>Update Client Mailing Postcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Mailing_Street</fullName>
        <field>MailingStreet</field>
        <formula>Address_1__c &amp; IF( ISBLANK( Address_2__c ) , &quot;&quot; , &quot;, &quot; &amp; Address_2__c )</formula>
        <name>Update Client Mailing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Client__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update Address History on Client</fullName>
        <actions>
            <name>Update_Client_Mailing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Client_Mailing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Client_Mailing_County</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Client_Mailing_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Client_Mailing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Address_History__c.Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <description>Copies the address of a current address history record to the Address field on the Contact,</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update changed Address History on Client</fullName>
        <actions>
            <name>Update_Client_Mailing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Client_Mailing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Client_Mailing_County</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Client_Mailing_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Client_Mailing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the address of a current address history record to the Address field on the Contact if it has been edited after creation</description>
        <formula>Status__c = &quot;Current&quot; &amp;&amp; ISCHANGED( Address__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
