<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Action_end_datetime_via_Session</fullName>
        <field>End_date_time__c</field>
        <formula>Session__r.End_date_time__c</formula>
        <name>Update Action end datetime via Session</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Action_start_datetime_via_Session</fullName>
        <field>Start_date_time__c</field>
        <formula>Session__r.Start_date_time__c</formula>
        <name>Update Action start datetime via Session</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Action start datetime and end datetime</fullName>
        <actions>
            <name>Update_Action_end_datetime_via_Session</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Action_start_datetime_via_Session</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the action&apos;s start datetime and end datetime to the Session&apos;s start datetime and end datetime.</description>
        <formula>NOT ( ISBLANK (Session__c ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
