<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_record_creator_email_reminder_to_review_Sanction</fullName>
        <description>Send record creator email reminder to review Sanction</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>In_Form_Reminder_Templates/Sanction_review_reminder_email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Sanction_status_to_current</fullName>
        <field>Secondary_status__c</field>
        <literalValue>Current</literalValue>
        <name>Update Sanction status to current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sanction_status_to_historic</fullName>
        <field>Secondary_status__c</field>
        <literalValue>Historic</literalValue>
        <name>Update Sanction status to historic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Sanction status to current immediately</fullName>
        <actions>
            <name>Update_Sanction_status_to_current</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sanction__c.End_date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sanction status to historic immediately</fullName>
        <actions>
            <name>Update_Sanction_status_to_historic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sanction__c.End_date__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
