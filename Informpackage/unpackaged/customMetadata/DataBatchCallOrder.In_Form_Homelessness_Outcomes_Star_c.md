<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>In_Form__Homelessness_Outcomes_Star__c</label>
    <protected>false</protected>
    <values>
        <field>DL_Scope_Field_Name__c</field>
        <value xsi:type="xsd:string">In_Form__DL_Scope__c</value>
    </values>
    <values>
        <field>DL_Scope_Field_Names__c</field>
        <value xsi:type="xsd:string">In_Form__DL_Scope__c</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Orders__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
</CustomMetadata>
