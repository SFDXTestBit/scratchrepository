<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>In_Form__DL_Empowerment_Star__c</label>
    <protected>false</protected>
    <values>
        <field>Is_Deleted__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Object_name__c</field>
        <value xsi:type="xsd:string">In_Form__DL_Empowerment_Star__c</value>
    </values>
</CustomMetadata>
