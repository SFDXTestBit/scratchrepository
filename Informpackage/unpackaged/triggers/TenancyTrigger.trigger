/*------------------------------------------------------------------
/* Created By : 
 * Created date : 11/8/2015
 * Description: Trigger on Tenancy Object
 * History    : v1.1 - Extentia  - 10th june 2019 - pass the Isbulk value to NightDeleteBatch to controll the rogue transactions. 
 
------------------------------------------------------------------*/ 
trigger TenancyTrigger on Tenancy__c (after insert, after update,after delete,before Update,before insert){
    Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
        
    if(Trigger.IsBefore){
        //if((Trigger.IsInsert || Trigger.IsUpdate) && (!System.isBatch())){
        
        // Added by Ajay on 02/14 for adding own static variable for System.isbatch
        if((Trigger.IsInsert || Trigger.IsUpdate) && (systemBatchHelper.isBatchRunning ==false)){
            TenancyTriggerUtility.validateTenancy(Trigger.New);
        }
        //if((Trigger.IsUpdate || Trigger.IsInsert) && (!System.isBatch())){
        // Added by Ajay on 02/14 for adding own static variable for System.isbatch
        if((Trigger.IsUpdate || Trigger.IsInsert) && (systemBatchHelper.isBatchRunning ==false)){

            for(Tenancy__c tenn : Trigger.New){
                if(!Trigger.IsInsert && !tenn.Balance_are_up_to_Date__c && !TenancyTriggerUtility.hasRun){
                    tenn.Balance_are_up_to_Date__c.addError(Label.Balance_Error);
                }
                else{
                    tenn.Balance_are_up_to_Date__c = false;
                }
            }
            if((Trigger.IsInsert || Trigger.IsUpdate) && !orgDefaults.In_Form__Turn_off_Tenancy_Creation_Validation__c){
                TenancyTriggerUtility.validateNights(Trigger.New,Trigger.oldmap);
            }
            
        }
    }
    
    if(Trigger.isAfter && !TenancyTriggerUtility.hasRun){
        TenancyTriggerUtility.hasRun = true;
       
        // Update Bedspace status after updation of any tenancy record
        if(Trigger.IsUpdate){
            
            //Update Status whenever Tenancy status is updated
            TenancyTriggerUtility.updateStatusonBedspace(Trigger.New);
            
            TenancyTriggerUtility.updateNightsOnVoidReasonChange(Trigger.New);
            
            //Create/Delete Night Creation Based on Tenancy Update
            Map<Id,Tenancy__c> newTenancyMap = Trigger.NewMap;
            Map<Id,List<Date>> tenancyDateMap = new Map<Id,List<Date>>();
            for(String tenId : newTenancyMap.Keyset()){
                List<Date> dtList = new List<Date>();
                dtList.add(newTenancyMap.get(tenId).Start_Date__c);
                /*if(newTenancyMap.get(tenId).End_Date__c == null){
                    dtList.add(Date.Today().addDays(1));
                }
                else{
                    dtList.add(newTenancyMap.get(tenId).End_Date__c);
                }*/
                dtList.add(newTenancyMap.get(tenId).End_Date__c);
                dtList.add(Trigger.oldMap.get(tenId).Start_Date__c);
                dtList.add(Trigger.oldMap.get(tenId).End_Date__c);
                tenancyDateMap.put(tenId,dtList);
            }
            //if(!(System.isBatch() || System.isScheduled())){
            // Added by Ajay on 02/14 for adding own static variable for System.isbatch
                if(systemBatchHelper.isBatchRunning ==false){
                    if(!tenancyDateMap.IsEmpty()){
                        SYstem.debug('=====Calling night=');
                        
                        Database.executeBatch(new NightDeleteBatch(tenancyDateMap,systemBatchHelper.isBulk));   // added by extentia. 
                    }
                }
            //}
        }
       
        if(Trigger.IsInsert){
            List<Id> idList = new List<Id>();
            List<Tenancy__c> closeTenancyList = new List<Tenancy__c>();
            for(Tenancy__c ten : Trigger.New){
                idList.add(ten.Id);
            }
            //Batch execution for creation of bulk Transaction on creation of new Tenancy record
            if(!orgDefaults.Turn_off_Rent_Module_Automation__c && !orgDefaults.Turn_off_Transaction_on_Tenancy_Creation__c){
                Database.executeBatch(new TransactionCreationBatch(idList),365);
            }
            if(!Test.isRunningTest() && !orgDefaults.Turn_off_Rent_Module_Automation__c && !orgDefaults.Turn_off_Nights_on_Tenancy_Creation__c){
                 Database.executeBatch(new NightCreationOnTenancyInsertBatch(idList),365);
            }
            //Update Status whenever Tenancy status is inserted
            TenancyTriggerUtility.updateStatusonBedspace(Trigger.New);
        }
       
        // Update Bedspace status after deletion of any tenancy record  
        if(Trigger.IsDelete){
             //Update Status whenever Tenancy is deleted
            TenancyTriggerUtility.updateonDelStatusonBedspace(Trigger.old);
        }
    }
    
}