/*------------------------------------------------------------------
/* Created By : 
 * Created date : 15/9/2015
 * Description: Trigger on Bedspace Object
------------------------------------------------------------------*/ 
trigger Bedspace_Trigger on Bedspace__c (before insert,before update) {
    if(Trigger.IsBefore){
        if(Trigger.IsInsert || Trigger.IsUpdate){
        	BedspaceTriggerUtility.updateStatus(Trigger.New);
        }
    }
}