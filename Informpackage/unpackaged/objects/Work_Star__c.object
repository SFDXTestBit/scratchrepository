<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Work_Star_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>Work_Star_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Work_Star_Layout</fullName>
        <fields>Client__c</fields>
        <fields>Name</fields>
        <fields>Timeline_Event__c</fields>
        <fields>Project__c</fields>
        <fields>Date__c</fields>
        <label>Work Star Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Aspiration_and_motivation__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Aspiration and motivation</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>1 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>2 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>2 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>3 - Thinking about work</fullName>
                    <default>false</default>
                    <label>3 - Thinking about work</label>
                </value>
                <value>
                    <fullName>4 - Thinking about work</fullName>
                    <default>false</default>
                    <label>4 - Thinking about work</label>
                </value>
                <value>
                    <fullName>5 - Making progress</fullName>
                    <default>false</default>
                    <label>5 - Making progress</label>
                </value>
                <value>
                    <fullName>6 - Making progress</fullName>
                    <default>false</default>
                    <label>6 - Making progress</label>
                </value>
                <value>
                    <fullName>7 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>7 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>8 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>8 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>9 - Self-reliance</fullName>
                    <default>false</default>
                    <label>9 - Self-reliance</label>
                </value>
                <value>
                    <fullName>10 - Self-reliance</fullName>
                    <default>false</default>
                    <label>10 - Self-reliance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Aspiration_and_motivation_notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Aspiration and motivation notes</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Aspiration_and_motivation_score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF (ISBLANK(TEXT( Aspiration_and_motivation__c  )),0,
VALUE(
TRIM(
LEFT(
TEXT(
Aspiration_and_motivation__c 
) ,
2)
)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Aspiration and motivation score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Basic_skills__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Basic skills</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>1 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>2 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>2 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>3 - Thinking about work</fullName>
                    <default>false</default>
                    <label>3 - Thinking about work</label>
                </value>
                <value>
                    <fullName>4 - Thinking about work</fullName>
                    <default>false</default>
                    <label>4 - Thinking about work</label>
                </value>
                <value>
                    <fullName>5 - Making progress</fullName>
                    <default>false</default>
                    <label>5 - Making progress</label>
                </value>
                <value>
                    <fullName>6 - Making progress</fullName>
                    <default>false</default>
                    <label>6 - Making progress</label>
                </value>
                <value>
                    <fullName>7 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>7 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>8 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>8 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>9 - Self-reliance</fullName>
                    <default>false</default>
                    <label>9 - Self-reliance</label>
                </value>
                <value>
                    <fullName>10 - Self-reliance</fullName>
                    <default>false</default>
                    <label>10 - Self-reliance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Basic_skills_notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Basic skills notes</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Basic_skills_score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF (ISBLANK(TEXT( Basic_skills__c )),0,
VALUE( 
TRIM( 
LEFT( 
TEXT( 
  Basic_skills__c   
) , 
2) 
) 
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Basic skills score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Challenges__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Challenges</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>1 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>2 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>2 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>3 - Thinking about work</fullName>
                    <default>false</default>
                    <label>3 - Thinking about work</label>
                </value>
                <value>
                    <fullName>4 - Thinking about work</fullName>
                    <default>false</default>
                    <label>4 - Thinking about work</label>
                </value>
                <value>
                    <fullName>5 - Making progress</fullName>
                    <default>false</default>
                    <label>5 - Making progress</label>
                </value>
                <value>
                    <fullName>6 - Making progress</fullName>
                    <default>false</default>
                    <label>6 - Making progress</label>
                </value>
                <value>
                    <fullName>7 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>7 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>8 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>8 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>9 - Self-reliance</fullName>
                    <default>false</default>
                    <label>9 - Self-reliance</label>
                </value>
                <value>
                    <fullName>10 - Self-reliance</fullName>
                    <default>false</default>
                    <label>10 - Self-reliance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Challenges_notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Challenges notes</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Challenges_score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF (ISBLANK(TEXT( Challenges__c  )),0,
VALUE(
TRIM(
LEFT(
TEXT(
Challenges__c 
) ,
2)
)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Challenges score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Client</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Work Stars</relationshipLabel>
        <relationshipName>Work_Stars</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Completed_by__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Completed by</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Worker and client jointed - agreed</fullName>
                    <default>false</default>
                    <label>Worker and client jointed - agreed</label>
                </value>
                <value>
                    <fullName>Worker and client jointed - disagreed</fullName>
                    <default>false</default>
                    <label>Worker and client jointed - disagreed</label>
                </value>
                <value>
                    <fullName>Worker alone</fullName>
                    <default>false</default>
                    <label>Worker alone</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>DL_Scope__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Timeline_Event__r.DL_Scope__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>DL Scope</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Job_search_skills__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Job search skills</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>1 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>2 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>2 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>3 - Thinking about work</fullName>
                    <default>false</default>
                    <label>3 - Thinking about work</label>
                </value>
                <value>
                    <fullName>4 - Thinking about work</fullName>
                    <default>false</default>
                    <label>4 - Thinking about work</label>
                </value>
                <value>
                    <fullName>5 - Making progress</fullName>
                    <default>false</default>
                    <label>5 - Making progress</label>
                </value>
                <value>
                    <fullName>6 - Making progress</fullName>
                    <default>false</default>
                    <label>6 - Making progress</label>
                </value>
                <value>
                    <fullName>7 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>7 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>8 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>8 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>9 - Self-reliance</fullName>
                    <default>false</default>
                    <label>9 - Self-reliance</label>
                </value>
                <value>
                    <fullName>10 - Self-reliance</fullName>
                    <default>false</default>
                    <label>10 - Self-reliance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Job_search_skills_notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Job search skills notes</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Job_search_skills_score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF (ISBLANK(TEXT(  Job_search_skills__c  )),0,
VALUE(
TRIM(
LEFT(
TEXT(
Job_search_skills__c
) ,
2)
)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Job search skills score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Job_specific_skills__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Job-specific skills</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>1 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>2 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>2 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>3 - Thinking about work</fullName>
                    <default>false</default>
                    <label>3 - Thinking about work</label>
                </value>
                <value>
                    <fullName>4 - Thinking about work</fullName>
                    <default>false</default>
                    <label>4 - Thinking about work</label>
                </value>
                <value>
                    <fullName>5 - Making progress</fullName>
                    <default>false</default>
                    <label>5 - Making progress</label>
                </value>
                <value>
                    <fullName>6 - Making progress</fullName>
                    <default>false</default>
                    <label>6 - Making progress</label>
                </value>
                <value>
                    <fullName>7 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>7 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>8 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>8 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>9 - Self-reliance</fullName>
                    <default>false</default>
                    <label>9 - Self-reliance</label>
                </value>
                <value>
                    <fullName>10 - Self-reliance</fullName>
                    <default>false</default>
                    <label>10 - Self-reliance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Job_specific_skills_notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Job-specific skills notes</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Job_specific_skills_score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF (ISBLANK(TEXT( Job_specific_skills__c )),0,
VALUE( 
TRIM( 
LEFT( 
TEXT( 
  Job_specific_skills__c   
) , 
2) 
) 
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Job-specific skills score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Timeline_Event__r.Project_Lookup__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Project</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Review_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Review date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Social_skills_for_work__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Social skills for work</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>1 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>2 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>2 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>3 - Thinking about work</fullName>
                    <default>false</default>
                    <label>3 - Thinking about work</label>
                </value>
                <value>
                    <fullName>4 - Thinking about work</fullName>
                    <default>false</default>
                    <label>4 - Thinking about work</label>
                </value>
                <value>
                    <fullName>5 - Making progress</fullName>
                    <default>false</default>
                    <label>5 - Making progress</label>
                </value>
                <value>
                    <fullName>6 - Making progress</fullName>
                    <default>false</default>
                    <label>6 - Making progress</label>
                </value>
                <value>
                    <fullName>7 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>7 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>8 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>8 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>9 - Self-reliance</fullName>
                    <default>false</default>
                    <label>9 - Self-reliance</label>
                </value>
                <value>
                    <fullName>10 - Self-reliance</fullName>
                    <default>false</default>
                    <label>10 - Self-reliance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Social_skills_for_work_notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Social skills for work notes</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Social_skills_for_work_score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF (ISBLANK(TEXT(  Social_skills_for_work__c  )),0,
VALUE(
TRIM(
LEFT(
TEXT(
Social_skills_for_work__c 
) ,
2)
)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Social skills for work score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Stability__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Stability</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>1 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>2 - Not thinking about work</fullName>
                    <default>false</default>
                    <label>2 - Not thinking about work</label>
                </value>
                <value>
                    <fullName>3 - Thinking about work</fullName>
                    <default>false</default>
                    <label>3 - Thinking about work</label>
                </value>
                <value>
                    <fullName>4 - Thinking about work</fullName>
                    <default>false</default>
                    <label>4 - Thinking about work</label>
                </value>
                <value>
                    <fullName>5 - Making progress</fullName>
                    <default>false</default>
                    <label>5 - Making progress</label>
                </value>
                <value>
                    <fullName>6 - Making progress</fullName>
                    <default>false</default>
                    <label>6 - Making progress</label>
                </value>
                <value>
                    <fullName>7 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>7 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>8 - Work-ready with support</fullName>
                    <default>false</default>
                    <label>8 - Work-ready with support</label>
                </value>
                <value>
                    <fullName>9 - Self-reliance</fullName>
                    <default>false</default>
                    <label>9 - Self-reliance</label>
                </value>
                <value>
                    <fullName>10 - Self-reliance</fullName>
                    <default>false</default>
                    <label>10 - Self-reliance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Stability_notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Stability notes</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Stability_score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF (ISBLANK(TEXT( Stability__c )),0,
VALUE( 
TRIM( 
LEFT( 
TEXT( 
  Stability__c   
) , 
2) 
) 
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Stability score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Star_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF 
( ISNULL(   Date__c   
  ) , 
DATEVALUE(CreatedDate) , 
 Date__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Star date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Timeline_Event__c</fullName>
        <deprecated>false</deprecated>
        <description>Label changed from &apos;Timeline Event&apos; to &apos;Timeline&apos; in v.1.44.</description>
        <externalId>false</externalId>
        <inlineHelpText>To see all of this Client&apos;s Timelines, type TL- and then hit Enter to search.</inlineHelpText>
        <label>Timeline</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Timeline_Event__c.Client__c</field>
                <operation>equals</operation>
                <valueField>$Source.Client__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Timeline_Event__c</referenceTo>
        <relationshipLabel>Work Stars</relationshipLabel>
        <relationshipName>Work_Stars</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Work Star</label>
    <listViews>
        <fullName>All_Work_Stars</fullName>
        <columns>NAME</columns>
        <columns>Client__c</columns>
        <columns>Timeline_Event__c</columns>
        <columns>Project__c</columns>
        <columns>Date__c</columns>
        <columns>Review_date__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All Work Stars</label>
    </listViews>
    <nameField>
        <displayFormat>WS-{000000}</displayFormat>
        <label>Work Star number</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Work Stars</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Client__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Timeline_Event__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Project__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Review_date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Client__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Timeline_Event__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Project__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Review_date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Client__c</searchFilterFields>
        <searchFilterFields>Timeline_Event__c</searchFilterFields>
        <searchFilterFields>Project__c</searchFilterFields>
        <searchFilterFields>Date__c</searchFilterFields>
        <searchFilterFields>Review_date__c</searchFilterFields>
        <searchFilterFields>CREATEDBY_USER</searchFilterFields>
        <searchResultsAdditionalFields>Client__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Timeline_Event__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Project__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Review_date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
