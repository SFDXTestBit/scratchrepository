<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Project_Agency_Link_Lightning_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>Project_Agency_Link_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Project_Agency_Link_Layout</fullName>
        <fields>Project_Agency__c</fields>
        <fields>Name</fields>
        <fields>Project__c</fields>
        <fields>Agency__c</fields>
        <fields>Status__c</fields>
        <label>Project-Agency Link Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Agency__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Agency</label>
        <referenceTo>Agency__c</referenceTo>
        <relationshipLabel>Related Projects</relationshipLabel>
        <relationshipName>Project_Agency_Links</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>DL_Scope__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>AND(Project__r.DL_Scope__c ,Agency__r.DL_Scope__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>DL Scope</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Details__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Details</label>
        <length>30000</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>50</visibleLines>
    </fields>
    <fields>
        <fullName>End_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>End date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Project_Agency__c</fullName>
        <deprecated>false</deprecated>
        <description>Shows the Client&apos;s name and the Agency&apos;s name concatenated together to use in compact layouts and search layouts in Lightning.</description>
        <externalId>false</externalId>
        <formula>HYPERLINK( Id , 
 Project__r.Name &amp; &quot; / &quot; &amp; Agency__r.Name )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Project &amp; Agency</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Related Agencies</relationshipLabel>
        <relationshipName>Project_Agency_Links</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Start_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Start date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( End_date__c &lt; TODAY() , &quot;Historic&quot; , &quot;Current&quot; )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>This is automatically determined by the End date and the date today.</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Project-Agency Link</label>
    <nameField>
        <displayFormat>PA-{000000}</displayFormat>
        <label>Project-Agency Link number</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Project-Agency Links</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Project__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Agency__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Start_date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>End_date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Details__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Project__c</searchFilterFields>
        <searchFilterFields>Agency__c</searchFilterFields>
        <searchFilterFields>Start_date__c</searchFilterFields>
        <searchFilterFields>End_date__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Start_date_must_be_before_End_date</fullName>
        <active>true</active>
        <description>The Start date must come before (or be the same as) the End date.</description>
        <errorConditionFormula>Start_date__c &gt; End_date__c</errorConditionFormula>
        <errorMessage>The End date must not come before the Start date.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
