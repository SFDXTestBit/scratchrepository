<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Income_Lightning_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>Income_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Income_Layout</fullName>
        <fields>Client__c</fields>
        <fields>Name</fields>
        <fields>Type__c</fields>
        <fields>Stage__c</fields>
        <fields>Sanctioned__c</fields>
        <label>Income Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Application_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Application date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Applied_for_via__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This is a lookup to the Agency that assisted the client with this application.</inlineHelpText>
        <label>Applied for via</label>
        <referenceTo>Agency__c</referenceTo>
        <relationshipLabel>Income applications supported by this Agency</relationshipLabel>
        <relationshipName>Incomes</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Claim_reference_number__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Claim reference number</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Client</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Income</relationshipLabel>
        <relationshipName>Incomes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>DL_Scope__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Client__r.DL_Scope__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>DL Scope</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>End_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>End date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Notes</label>
        <length>10000</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Number_of_current_sanctions__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Number of current sanctions</label>
        <summaryFilterItems>
            <field>Sanction__c.Secondary_status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </summaryFilterItems>
        <summaryForeignKey>Sanction__c.Income__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Payment_day__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment day</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Monday</fullName>
                    <default>false</default>
                    <label>Monday</label>
                </value>
                <value>
                    <fullName>Tuesday</fullName>
                    <default>false</default>
                    <label>Tuesday</label>
                </value>
                <value>
                    <fullName>Wednesday</fullName>
                    <default>false</default>
                    <label>Wednesday</label>
                </value>
                <value>
                    <fullName>Thursday</fullName>
                    <default>false</default>
                    <label>Thursday</label>
                </value>
                <value>
                    <fullName>Friday</fullName>
                    <default>false</default>
                    <label>Friday</label>
                </value>
                <value>
                    <fullName>Saturday</fullName>
                    <default>false</default>
                    <label>Saturday</label>
                </value>
                <value>
                    <fullName>Sunday</fullName>
                    <default>false</default>
                    <label>Sunday</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Payment_frequency__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment frequency</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Weekly</fullName>
                    <default>false</default>
                    <label>Weekly</label>
                </value>
                <value>
                    <fullName>Every 2 weeks</fullName>
                    <default>false</default>
                    <label>Every 2 weeks</label>
                </value>
                <value>
                    <fullName>Every 4 weeks</fullName>
                    <default>false</default>
                    <label>Every 4 weeks</label>
                </value>
                <value>
                    <fullName>Monthly</fullName>
                    <default>false</default>
                    <label>Monthly</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Sanctioned__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( Number_of_current_sanctions__c &gt; 0 , 
IMAGE( &quot;/img/samples/flag_red.gif&quot; , &quot;Sanctioned&quot; ) , &quot;&quot; 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Shows a red flag if this income record has a current sanction.</inlineHelpText>
        <label>Sanctioned</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sign_on_day__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sign on day</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Monday</fullName>
                    <default>false</default>
                    <label>Monday</label>
                </value>
                <value>
                    <fullName>Tuesday</fullName>
                    <default>false</default>
                    <label>Tuesday</label>
                </value>
                <value>
                    <fullName>Wednesday</fullName>
                    <default>false</default>
                    <label>Wednesday</label>
                </value>
                <value>
                    <fullName>Thursday</fullName>
                    <default>false</default>
                    <label>Thursday</label>
                </value>
                <value>
                    <fullName>Friday</fullName>
                    <default>false</default>
                    <label>Friday</label>
                </value>
                <value>
                    <fullName>Saturday</fullName>
                    <default>false</default>
                    <label>Saturday</label>
                </value>
                <value>
                    <fullName>Sunday</fullName>
                    <default>false</default>
                    <label>Sunday</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Stage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Stage</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Type__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Application</fullName>
                    <default>false</default>
                    <label>Application</label>
                </value>
                <value>
                    <fullName>In receipt</fullName>
                    <default>false</default>
                    <label>In receipt</label>
                </value>
                <value>
                    <fullName>No longer in receipt</fullName>
                    <default>false</default>
                    <label>No longer in receipt</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Income Support</controllingFieldValue>
                <controllingFieldValue>Universal Credit</controllingFieldValue>
                <controllingFieldValue>Income support with disability premium</controllingFieldValue>
                <controllingFieldValue>Other</controllingFieldValue>
                <controllingFieldValue>Job Seekers Allowance (JSA)</controllingFieldValue>
                <controllingFieldValue>Maternity Allowance</controllingFieldValue>
                <controllingFieldValue>Youth Allowance</controllingFieldValue>
                <controllingFieldValue>Home Office immigration benefit</controllingFieldValue>
                <controllingFieldValue>Pension</controllingFieldValue>
                <controllingFieldValue>Personal Independence Payment</controllingFieldValue>
                <controllingFieldValue>Severe Disability Premium</controllingFieldValue>
                <controllingFieldValue>Armed Forces Independence Payment</controllingFieldValue>
                <controllingFieldValue>Attendance Allowance</controllingFieldValue>
                <controllingFieldValue>Carer&apos;s Allowance</controllingFieldValue>
                <controllingFieldValue>Child Tax credits</controllingFieldValue>
                <controllingFieldValue>Disability Living Allowance (DLA)</controllingFieldValue>
                <controllingFieldValue>Disability Premium</controllingFieldValue>
                <controllingFieldValue>Employment and Support Allowance (ESA)</controllingFieldValue>
                <controllingFieldValue>Housing Benefit</controllingFieldValue>
                <controllingFieldValue>Housing Support</controllingFieldValue>
                <controllingFieldValue>Statutory Sick Pay</controllingFieldValue>
                <controllingFieldValue>Incapacity Benefit</controllingFieldValue>
                <valueName>Application</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Income Support</controllingFieldValue>
                <controllingFieldValue>Universal Credit</controllingFieldValue>
                <controllingFieldValue>Income support with disability premium</controllingFieldValue>
                <controllingFieldValue>Other</controllingFieldValue>
                <controllingFieldValue>Job Seekers Allowance (JSA)</controllingFieldValue>
                <controllingFieldValue>Maternity Allowance</controllingFieldValue>
                <controllingFieldValue>Youth Allowance</controllingFieldValue>
                <controllingFieldValue>Home Office immigration benefit</controllingFieldValue>
                <controllingFieldValue>Pension</controllingFieldValue>
                <controllingFieldValue>Personal Independence Payment</controllingFieldValue>
                <controllingFieldValue>Severe Disability Premium</controllingFieldValue>
                <controllingFieldValue>Armed Forces Independence Payment</controllingFieldValue>
                <controllingFieldValue>Attendance Allowance</controllingFieldValue>
                <controllingFieldValue>Carer&apos;s Allowance</controllingFieldValue>
                <controllingFieldValue>Child Tax credits</controllingFieldValue>
                <controllingFieldValue>Disability Living Allowance (DLA)</controllingFieldValue>
                <controllingFieldValue>Disability Premium</controllingFieldValue>
                <controllingFieldValue>Employment and Support Allowance (ESA)</controllingFieldValue>
                <controllingFieldValue>Housing Benefit</controllingFieldValue>
                <controllingFieldValue>Housing Support</controllingFieldValue>
                <controllingFieldValue>Statutory Sick Pay</controllingFieldValue>
                <controllingFieldValue>Incapacity Benefit</controllingFieldValue>
                <valueName>In receipt</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Income Support</controllingFieldValue>
                <controllingFieldValue>Universal Credit</controllingFieldValue>
                <controllingFieldValue>Income support with disability premium</controllingFieldValue>
                <controllingFieldValue>Other</controllingFieldValue>
                <controllingFieldValue>Job Seekers Allowance (JSA)</controllingFieldValue>
                <controllingFieldValue>Maternity Allowance</controllingFieldValue>
                <controllingFieldValue>Youth Allowance</controllingFieldValue>
                <controllingFieldValue>Home Office immigration benefit</controllingFieldValue>
                <controllingFieldValue>Pension</controllingFieldValue>
                <controllingFieldValue>Personal Independence Payment</controllingFieldValue>
                <controllingFieldValue>Severe Disability Premium</controllingFieldValue>
                <controllingFieldValue>Armed Forces Independence Payment</controllingFieldValue>
                <controllingFieldValue>Attendance Allowance</controllingFieldValue>
                <controllingFieldValue>Carer&apos;s Allowance</controllingFieldValue>
                <controllingFieldValue>Child Tax credits</controllingFieldValue>
                <controllingFieldValue>Disability Living Allowance (DLA)</controllingFieldValue>
                <controllingFieldValue>Disability Premium</controllingFieldValue>
                <controllingFieldValue>Employment and Support Allowance (ESA)</controllingFieldValue>
                <controllingFieldValue>Housing Benefit</controllingFieldValue>
                <controllingFieldValue>Housing Support</controllingFieldValue>
                <controllingFieldValue>Statutory Sick Pay</controllingFieldValue>
                <controllingFieldValue>Incapacity Benefit</controllingFieldValue>
                <valueName>No longer in receipt</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Start_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Start date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Armed Forces Independence Payment</fullName>
                    <default>false</default>
                    <label>Armed Forces Independence Payment</label>
                </value>
                <value>
                    <fullName>Attendance Allowance</fullName>
                    <default>false</default>
                    <label>Attendance Allowance</label>
                </value>
                <value>
                    <fullName>Carer&apos;s Allowance</fullName>
                    <default>false</default>
                    <label>Carer&apos;s Allowance</label>
                </value>
                <value>
                    <fullName>Child Tax credits</fullName>
                    <default>false</default>
                    <label>Child Tax credits</label>
                </value>
                <value>
                    <fullName>Disability Living Allowance (DLA)</fullName>
                    <default>false</default>
                    <label>Disability Living Allowance (DLA)</label>
                </value>
                <value>
                    <fullName>Disability Premium</fullName>
                    <default>false</default>
                    <label>Disability Premium</label>
                </value>
                <value>
                    <fullName>Employment and Support Allowance (ESA)</fullName>
                    <default>false</default>
                    <label>Employment and Support Allowance (ESA)</label>
                </value>
                <value>
                    <fullName>Home Office immigration benefit</fullName>
                    <default>false</default>
                    <label>Home Office immigration benefit</label>
                </value>
                <value>
                    <fullName>Housing Benefit</fullName>
                    <default>false</default>
                    <label>Housing Benefit</label>
                </value>
                <value>
                    <fullName>Housing Support</fullName>
                    <default>false</default>
                    <label>Housing Support</label>
                </value>
                <value>
                    <fullName>Incapacity Benefit</fullName>
                    <default>false</default>
                    <label>Incapacity Benefit</label>
                </value>
                <value>
                    <fullName>Income Support</fullName>
                    <default>false</default>
                    <label>Income Support</label>
                </value>
                <value>
                    <fullName>Income support with disability premium</fullName>
                    <default>false</default>
                    <label>Income support with disability premium</label>
                </value>
                <value>
                    <fullName>Job Seekers Allowance (JSA)</fullName>
                    <default>false</default>
                    <label>Job Seekers Allowance (JSA)</label>
                </value>
                <value>
                    <fullName>Maternity Allowance</fullName>
                    <default>false</default>
                    <label>Maternity Allowance</label>
                </value>
                <value>
                    <fullName>Pension</fullName>
                    <default>false</default>
                    <label>Pension</label>
                </value>
                <value>
                    <fullName>Personal Independence Payment</fullName>
                    <default>false</default>
                    <label>Personal Independence Payment</label>
                </value>
                <value>
                    <fullName>Severe Disability Premium</fullName>
                    <default>false</default>
                    <label>Severe Disability Premium</label>
                </value>
                <value>
                    <fullName>Statutory Sick Pay</fullName>
                    <default>false</default>
                    <label>Statutory Sick Pay</label>
                </value>
                <value>
                    <fullName>Work</fullName>
                    <default>false</default>
                    <label>Work</label>
                </value>
                <value>
                    <fullName>Universal Credit</fullName>
                    <default>false</default>
                    <label>Universal Credit</label>
                </value>
                <value>
                    <fullName>Youth Allowance</fullName>
                    <default>false</default>
                    <label>Youth Allowance</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Income</label>
    <nameField>
        <displayFormat>I-{000000}</displayFormat>
        <label>Income number</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Income</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Client__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Stage__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Client__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Stage__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Client__c</searchFilterFields>
        <searchFilterFields>Type__c</searchFilterFields>
        <searchFilterFields>Stage__c</searchFilterFields>
        <searchResultsAdditionalFields>Client__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Stage__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Start_date_must_be_before_End_date</fullName>
        <active>true</active>
        <description>The Start date must come before (or be the same as) the End date.</description>
        <errorConditionFormula>Start_date__c &gt; End_date__c</errorConditionFormula>
        <errorMessage>The End date must not come before the Start date.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
