<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This is part of the In-Form Rent Module.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Occupancy_status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( Tenancy__r.RecordType.Name = &quot;Tenancy&quot; , &quot;Occupied&quot; ,
 IF( ISPICKVAL( Void_status__c , &quot;Available to let&quot; ) , &quot;Available&quot; ,
 IF( ISPICKVAL( Void_status__c , &quot;Unavailable&quot; ) , TEXT( Void_reason__c ) , &quot;&quot; )))</formula>
        <inlineHelpText>This is used in reporting to show exactly what was happening (occupied, available, etc.) for a given Night.</inlineHelpText>
        <label>Occupancy status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tenancy__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tenancy</label>
        <referenceTo>Tenancy__c</referenceTo>
        <relationshipLabel>Nights</relationshipLabel>
        <relationshipName>Nights</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Total_Nights__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>1</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Used in reports to allowing the counting of distinct Night records.</inlineHelpText>
        <label>Total Nights</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Void_reason__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Void reason</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Waiting for referral</fullName>
                    <default>false</default>
                    <label>Waiting for referral</label>
                </value>
                <value>
                    <fullName>Waiting for cleaning / delivery</fullName>
                    <default>false</default>
                    <label>Waiting for cleaning / delivery</label>
                </value>
                <value>
                    <fullName>Routine repairs needed</fullName>
                    <default>false</default>
                    <label>Routine repairs needed</label>
                </value>
                <value>
                    <fullName>Major repairs needed</fullName>
                    <default>false</default>
                    <label>Major repairs needed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Void_status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Void status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Available to let</fullName>
                    <default>false</default>
                    <label>Available to let</label>
                </value>
                <value>
                    <fullName>Unavailable</fullName>
                    <default>false</default>
                    <label>Unavailable</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Night</label>
    <nameField>
        <displayFormat>N-{000000000}</displayFormat>
        <label>Night number</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Nights</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>No_void_information_for_Tenancies</fullName>
        <active>true</active>
        <description>You cannot enter void information if the parent Tenancy does not have a record type of &apos;Void&apos;.</description>
        <errorConditionFormula>Tenancy__r.RecordType.Name &lt;&gt; &quot;Void&quot; &amp;&amp;
(
 NOT( ISPICKVAL( Void_reason__c , &quot;&quot; )) ||
 NOT( ISPICKVAL( Void_status__c , &quot;&quot; ))
)</errorConditionFormula>
        <errorMessage>You cannot enter void information if the Tenancy isn&apos;t a void record.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
