<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Rent_Schedule_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>Rent_Schedule_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Rent_Schedule_Layout</fullName>
        <fields>Bedspace__c</fields>
        <fields>Name</fields>
        <fields>Start_date__c</fields>
        <fields>End_date__c</fields>
        <fields>Is_default__c</fields>
        <fields>Tenancy__c</fields>
        <fields>Number_of_Rent_Stream_Charge_records__c</fields>
        <label>Rent Schedule Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>This is part of the In-Form Rent Module.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Autocalculated_total_rent_charge__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This is the total of the Amount field from all of this Rent Schedule&apos;s Rent Stream Charges, to ensure that the Total rent charge is the same as all of the Rent Stream Charges added together.</inlineHelpText>
        <label>Autocalculated total rent charge</label>
        <summarizedField>Rent_Stream_Charge__c.Amount__c</summarizedField>
        <summaryForeignKey>Rent_Stream_Charge__c.Rent_Schedule__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Bedspace__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bedspace</label>
        <referenceTo>Bedspace__c</referenceTo>
        <relationshipLabel>Rent Schedules</relationshipLabel>
        <relationshipName>Rent_Schedules</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>End_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>End date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Is_default__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This is to show that the Rent Schedule (RS) is the default RS for this Bedspace in this time period.
This means that if a new Tenancy is created the values for the rent charges will be taken from the default RS, not necessarily the most recently created.</inlineHelpText>
        <label>Is default</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Number_of_Rent_Stream_Charge_records__c</fullName>
        <deprecated>false</deprecated>
        <description>Used in validation rules.</description>
        <externalId>false</externalId>
        <inlineHelpText>Counts how many Rent Stream Charge records are attached to this Rent Schedule.</inlineHelpText>
        <label>Number of Rent Stream Charge records</label>
        <summaryForeignKey>Rent_Stream_Charge__c.Rent_Schedule__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Start_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Start date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Tenancy__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tenancy</label>
        <referenceTo>Tenancy__c</referenceTo>
        <relationshipLabel>Rent Schedules</relationshipLabel>
        <relationshipName>Rent_Schedules</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Total_rent_charge__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This is the total amount of rent charged for the Bedspace. This must match the Autocalculated total rent charge, to ensure that it is the same as all of the Rent Stream Charges added together.</inlineHelpText>
        <label>Total rent charge</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_rent_schedules__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>1</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Used in reports to allowing the counting of distinct Rent Schedule records.</inlineHelpText>
        <label>Total rent schedules</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Totals_do_not_match__c</fullName>
        <deprecated>false</deprecated>
        <description>This is used in a rich text component on the Rent Schedule Lightning Page to show an alert if the two totals do not match.</description>
        <externalId>false</externalId>
        <formula>Autocalculated_total_rent_charge__c &lt;&gt;  Total_rent_charge__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>This is checked if the Total rent charge field (which is entered manually) does not match the Autocalculated total rent charge field.</inlineHelpText>
        <label>Totals do not match</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Rent Schedule</label>
    <nameField>
        <displayFormat>RS-{000000}</displayFormat>
        <label>Rent Schedule Number</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Rent Schedules</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Bedspace__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Start_date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>End_date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total_rent_charge__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Bedspace__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Start_date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>End_date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Total_rent_charge__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Is_default__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Bedspace__c</searchFilterFields>
        <searchFilterFields>Start_date__c</searchFilterFields>
        <searchFilterFields>End_date__c</searchFilterFields>
        <searchFilterFields>Total_rent_charge__c</searchFilterFields>
        <searchFilterFields>Is_default__c</searchFilterFields>
        <searchResultsAdditionalFields>Bedspace__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Start_date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>End_date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Total_rent_charge__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Cannot_change_Rent_Schedule_details</fullName>
        <active>true</active>
        <description>Once a Rent Schedule has been created, the Total Rent Charge, Start date, End date, Tenancy and Default checkbox cannot be changed.</description>
        <errorConditionFormula>/* Exclude Users with the Edit Rent Schedules Custom Permission - anyone with this custom permission will bypass this validation rule */ 
NOT( $Permission.Edit_Rent_Schedules ) &amp;&amp; 


/* The Total rent change, Start date, End date, Tenancy and Is default fields cannot be edited */
(
 ISCHANGED( Total_rent_charge__c ) ||
 ISCHANGED( Start_date__c ) ||
 ISCHANGED( End_date__c ) ||
 ISCHANGED( Tenancy__c ) ||
 ISCHANGED( Is_default__c )
)</errorConditionFormula>
        <errorMessage>Once the Rent Schedule has been created, you cannot change the Total Rent Charge, Start date, End date, Tenancy or Default.
If you wish to change this information please create a new Rent Schedule by clicking &apos;Split rent Charge&apos; on the relevant Tenancy.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Default_or_Tenancy_not_blank</fullName>
        <active>true</active>
        <errorConditionFormula>AND(ISNEW(),AND( Is_default__c = false, (ISBLANK( Tenancy__c ))))</errorConditionFormula>
        <errorDisplayField>Tenancy__c</errorDisplayField>
        <errorMessage>Either Default Rent Schedule is True OR Tenancy should  not be blank</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>End_date_must_be_after_start_date</fullName>
        <active>true</active>
        <description>The end date must be after the start date.</description>
        <errorConditionFormula>End_date__c  &lt;  Start_date__c</errorConditionFormula>
        <errorMessage>The end date must be after the start date.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
