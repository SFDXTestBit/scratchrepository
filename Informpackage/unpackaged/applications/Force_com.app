<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Start Here</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Force.com</label>
    <tabs>standard-Chatter</tabs>
    <tabs>DL_Batch__c</tabs>
    <tabs>DL_Field_Mappings</tabs>
    <tabs>DL_Client__c</tabs>
    <tabs>DL_Timeline_Event__c</tabs>
    <tabs>Connect_Datalab</tabs>
    <tabs>DL_Field_Mapping__c</tabs>
    <tabs>WEMWBS__c</tabs>
</CustomApplication>
