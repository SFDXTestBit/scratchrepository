/* Class Name  : ScheduleNightCreation_Test
 * Description   : Apex Test class to create a night record everyday at 3.00 AM
 * Created By    : Raushan Anand
 * Created On    : 03-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              03-Nov-2015               Initial version.
 *  * Bhargav Shah               30-Oct-2018               Security Review.
 *****************************************************************************************/
@isTest
public class ScheduleNightCreation_Test {
    @TestSetup
    public static void setupData(){
        try{
            Transaction_Type__c dailytype = new Transaction_Type__c(Daily_Amount__c = true,Is_Active__c=true,Type__c = 'Daily',Name = 'DailyType');
            INSERT dailyType;
            List<Tenancy__c> tenList = new List<Tenancy__c>();
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            Map<String,Id> rtMap = new Map<String,Id>();
            if(!rtList.isEmpty()){
                for(RecordType rt: rtList)
                    rtMap.put(rt.Name,rt.Id);
            }
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace',In_Form__Basic_Rent__c=100);
            INSERT oBedspace;
            
            Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
            INSERT con;
            
            Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Bedspace__c =oBedspace.Id,Total_Rent_Charge__c=100,Start_date__c=Date.Today().addDays(-10),End_date__c=Date.Today().addDays(4),In_Form__Is_default__c=true);
            INSERT oRentSchedule;
            
            Rent_Stream_Charge__c oRentCharge = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=50);
            INSERT oRentCharge;
            
            Rent_Stream_Charge__c oRentCharge1 = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=100);
            INSERT oRentCharge1;        
            
            Tenancy__c ten1 = new Tenancy__c();
            ten1.recordTypeid = rtMap.get('Tenancy');
            ten1.Client__c = con.Id;
            ten1.Start_date__c=Date.Today().addDays(-34);
            ten1.Status__c='Current';
            ten1.Bedspace__c=oBedspace.Id;
            tenList.add(ten1);
            
            Tenancy__c ten2 = new Tenancy__c();
            ten2.recordTypeid = rtMap.get('Tenancy');
            ten2.Client__c = con.Id;
            ten2.Start_date__c=Date.Today().addDays(-34);
            ten2.Status__c='Current';
            ten2.Bedspace__c=oBedspace.Id;
            tenList.add(ten2);
                    
            Tenancy__c ten3 = new Tenancy__c();
            ten3.recordTypeid = rtMap.get('Tenancy');
            ten3.Client__c = con.Id;
            ten3.Start_date__c=Date.Today().addDays(-34);
            ten3.Status__c='Current';
            ten3.Bedspace__c=oBedspace.Id;
            tenList.add(ten3);
            
            
            Tenancy__c ten4 = new Tenancy__c();
            ten4.recordTypeid = rtMap.get('Tenancy');
            ten4.Client__c = con.Id;
            ten4.Start_date__c=Date.Today().addDays(-34);
            ten4.Status__c='Current';
            ten4.Bedspace__c=oBedspace.Id;
            tenList.add(ten4);
            INSERT tenList;
    
            System.assertEquals(4, tenList.size(),'SUCCESS');
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }
    } 
    
    public static testmethod void TestScheduleNightCreation(){
        try{
            Test.startTest();
            String CRON_EXP = '0 0 0 1 1 ? 2025';  
            String jobId = System.schedule('ScheduledApex', CRON_EXP, new scheduleNightCreation() );
            CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression); 
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));
            Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    }
    public static testmethod void TestNightDelete(){
        try{
            Test.startTest();
                List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34) ];
                Tenancy__c ten = tenList[0];
                ten.Start_date__c = Date.Today().addDays(-2);
                ten.End_date__c = Date.Today();
                Night__c nigh = new Night__c(Date__c = Date.today().addDays(-4),Tenancy__c = ten.Id);
                INSERT nigh;
                try{
                    Update ten;
                }Catch(Exception e){
                
                }
                System.assertEquals(Date.Today(), ten.End_Date__c);
            Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }
    }
     public static testmethod void TestNightUpdate(){
         try{
             Map<Id,List<Date>> mapDate = new map<Id, List<Date>>();
             List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34)];
             if(!tenList.isEmpty()){
                 Tenancy__c ten = tenList[0];
                 Night__c nigh = new Night__c(Date__c = Date.today(),Tenancy__c = ten.Id);
                 INSERT nigh;
                 date startDate = date.Today().adddays(-2);
                 date endDate = date.Today();
                 List<Date> dtList = new List<Date>();
                 dtList.add(startDate);
                 dtList.add(endDate);
                 dtList.add(date.Today().adddays(3));
                 dtList.add(date.Today().adddays(4));
                 mapDate.put(ten.Id, dtList);
                 Test.startTest();
                     Database.executeBatch(new NightUpdateBatch(mapDate,systemBatchHelper.isBulk));  // added by extentia.
                 Test.stopTest();
                 List<Night__c> nighList = [SELECT Id FROM Night__c where Tenancy__c =: ten.Id];
                 System.assertEquals(3,nighList.size());
            }     
         }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }
    }
    public static testmethod void TestTransactcionUpdateDelete(){
        try{
            Map<Id,List<Date>> mapDate = new map<Id, List<Date>>();
            Test.startTest();
                List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34)];
                if(!tenList.isEmpty()){
                    Tenancy__c ten = tenList[0];
                    date startDate = date.Today().adddays(-2);
                    date endDate = date.Today();
                    List<Date> dtList = new List<Date>();
                    dtList.add(startDate);
                    dtList.add(endDate);
                    dtList.add(date.Today().adddays(3));
                    dtList.add(date.Today().adddays(4));
                    mapDate.put(ten.Id, dtList);
                    Database.executeBatch(new TransactionDeleteBatch(mapDate,systemBatchHelper.isBulk));
                    List<Transaction__c> tranList = [SELECT Id FROM Transaction__c where Tenancy__c =:ten.Id limit 11];
                    System.assertEquals(11,tranList.size());
                }
            Test.stopTest();
       }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
       }  
    }
    public static testmethod void TestNightCreationonTenancy(){
        try{
            List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34) ];
            List<Id> ids = new List<Id>();
            for(Tenancy__c tenId : tenList){
                ids.add(tenId.Id);
            }
            Test.startTest();
                Database.executeBatch(new NightCreationOnTenancyInsertBatch(ids),365);
            Test.stopTest();
            if(!tenList.isEmpty()){
                List<Night__c> nighList = [SELECT Id FROM Night__c where Tenancy__c =: tenList[0].Id];
                System.assertEquals(35,nighList.size());
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    }
}