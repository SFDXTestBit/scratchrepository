/*------------------------------------------------------------------
/* Created By : 
 * Created date : 20/8/2015
 * Description: Batch class to create transaction on creation of tenancy
 * History: v1.1 - Bhargav Shah. - 31/10/2018 - Enforcing CRUD/FLS
------------------------------------------------------------------*/
global with sharing class TransactionTotalUpdationBatch implements Database.Batchable<sObject>,Database.Stateful{
    
    Set<Id> tenancyIds = new Set<Id>();
    Date startDate;
    Date endDate;
    global Map<Id,Map<String,Double>> tenancyTypeBalanceMap;
 
    global TransactionTotalUpdationBatch(Set<Id> tenancyIds, Date startDate){
        this(tenancyIds, new Map<Id,Map<String,Double>>(), startDate);
    }
    
    global TransactionTotalUpdationBatch(Set<Id> tenancyIds, Map<Id,Map<String,Double>> tenancyTypeBalanceMap, Date startDate){
        this.tenancyIds = tenancyIds;
        this.startDate = startDate;
        this.endDate = startDate.addYears(1);
        this.tenancyTypeBalanceMap = tenancyTypeBalanceMap;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
            Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
            
            if(orgDefaults.Turn_off_Rent_Module_Automation__c || orgDefaults.Turn_off_Total_Balance_Update_on_Trans__c){
                return null;
            }
            //List<String> tenancyFieldsList = new List<String>{'In_Form__End_date__c','In_Form__Start_date__c'};
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',tenancyFieldsList)){
                return Database.getQueryLocator('SELECT Id, Name, End_date__c, Start_date__c FROM Tenancy__c WHERE Id IN: tenancyIds');
            //}
        
        //return null;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try{
            List<Tenancy__c>  tenancyList = (List<Tenancy__c>)scope;
            List<Transaction__c> tranList = new List<Transaction__c>();
            //List<String> transactionsFieldsList = new List<String>{'In_Form__Amount__c','In_Form__Type__c','In_Form__Balance__c','In_Form__Tenancy__c','In_Form__Transaction_Date__c'};
            //if(InForm_CRUDFLS_Util.getUpdateAccessCheck('In_Form__Transaction__c',transactionsFieldsList)){
                tranList = [SELECT Id, Amount__c, Type__c, Balance__c, 
                                    Tenancy__c,Transaction_date__c 
                                    FROM Transaction__c 
                                    WHERE Tenancy__c IN: tenancyList 
                                    AND DAY_ONLY(convertTimezone(Transaction_Date__c)) >=: startDate 
                                    AND DAY_ONLY(convertTimezone(Transaction_Date__c)) <: endDate 
                                    ORDER BY Transaction_date__c ASC];
                
                //Added by Ajay on 05/03/2018
                for(Transaction__c trans : tranList){
                    Map<String,Double> rentTypeAmount = new Map<String,Double>();
                    
                    if(tenancyTypeBalanceMap.containsKey(trans.Tenancy__c)){
                        rentTypeAmount = tenancyTypeBalanceMap.get(trans.Tenancy__c);
                        
                        if(rentTypeAmount.containsKey(trans.Type__c)){
                            rentTypeAmount.put(trans.Type__c, rentTypeAmount.get(trans.Type__c) + trans.Amount__c);
                        }
                        else{
                            rentTypeAmount.put(trans.Type__c, trans.Amount__c);
                        }
                    }
                    else{
                        rentTypeAmount.put(trans.Type__c,trans.Amount__c);
                    }
                    
                    trans.Balance__c = rentTypeAmount.get(trans.Type__c);
                    tenancyTypeBalanceMap.put(trans.Tenancy__c,rentTypeAmount);
                }
                systemBatchHelper.isBatchRunning=true;
                update tranList;
            //}

            //Added by Ajay on 05/03/2018
            if(tranList.isempty()){
                for(Tenancy__c tnc: tenancyList ){
                    tnc.Balance_are_up_to_Date__c = true;
                }
                if(tenancyList.isempty()==false){
                    systemBatchHelper.isBatchRunning=true;
                    UPDATE tenancyList;
                }
            }
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
        try{        
            if(startDate.addYears(1) <= Date.Today()){
                Database.executeBatch(new TransactionTotalUpdationBatch(tenancyIds, tenancyTypeBalanceMap, startDate.addYears(1)), 5);
            }
            else{
                //Mayur Patil[26/10/2018]:v1.1 - Extentia - Enforcing CRUD/FLS
                List<Tenancy__c> tenUpList = [Select Id, Balance_are_up_to_Date__c FROM Tenancy__c WHERE Id IN:tenancyTypeBalanceMap.keySet()];
                //List<String> fieldList=new List<String>{'In_Form__Balance_are_up_to_Date__c'};
                //if(InForm_CRUDFLS_Util.getUpdateAccessCheck('In_Form__Tenancy__c', fieldList)){
                    List<Tenancy__c> tenUpdate = new List<Tenancy__c>();
                    for(Tenancy__c ten : tenUpList){
                        ten.Balance_are_up_to_Date__c = true;
                        tenUpdate.add(ten);
                    }
                    systemBatchHelper.isBatchRunning=true;
                    update tenUpdate;
                //}
            }
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
}