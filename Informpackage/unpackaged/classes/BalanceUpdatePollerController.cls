/*-----------------------------------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 12/01/2016 13:43
 * Last Modified by:: Saurabh Gupta ,  26/09/2017 11:22
 * Description: BalanceUpdatePollerController  is the extension of BalanceStatus visualforce page
 * History: v1.1 - Sneha U. - 31/10/2018 - Removed commented code, Add try catch, Read access check
-------------------------------------------------------------------------------------------------*/

public with sharing class BalanceUpdatePollerController {
    Tenancy__c ten;
    public Tenancy__c tenUp {get;set;}
    public BalanceUpdatePollerController(ApexPages.StandardController stdController){
        try{
            ten = (Tenancy__c)stdController.getRecord();

            //List of fields assigned for Tenancy__c Object read access
            /*List<String> fieldList = new List<String>{
                'In_Form__Balance_are_up_to_Date__c'
            };*/ 

            //Check read access for Tenancy Object
            //if(InForm_CRUDFLS_Util.getReadAccessCheck(String.valueOf(Schema.SObjectType.Tenancy__c.getSobjectType()),fieldList) ){
                tenUp = [Select Id,Balance_are_up_to_Date__c FROM Tenancy__c where id =: ten.Id];
            //}
            //else{
            //    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'You do not have access. Please contact administrator'));
            //}
        }
        catch(Exception e){ ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage()));
        }
    }
    public PageReference  refreshStatus(){
        try{
            //List of fields assigned for Tenancy__c Object read access
            /*List<String> fieldList = new List<String>{
                'In_Form__Balance_are_up_to_Date__c'
            };*/ 

            //Check read access for Tenancy Object
            //if(InForm_CRUDFLS_Util.getReadAccessCheck(String.valueOf(Schema.SObjectType.Tenancy__c.getSobjectType()),fieldList) ){
                tenUp = [Select Id,Balance_are_up_to_Date__c FROM Tenancy__c where id =: ten.Id];
            //}
            //else{
           //     ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'You do not have access. Please contact administrator'));
           // }
        }
        catch(Exception e){ ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage())); return null;
        }
        return null;
    }
}