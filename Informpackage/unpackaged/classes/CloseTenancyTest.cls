/*------------------------------------------------------------------
/* Created By : 
 * Created date : 18/8/2015
 * Description: Close Tenancy Test
------------------------------------------------------------------*/
@isTest
public class CloseTenancyTest{
    //Test setup data
    @TestSetup
    public static void setupData(){
        try{
            Transaction_Type__c dailytype = new Transaction_Type__c(Daily_Amount__c = true,Is_Active__c=true,Type__c = 'Daily',Name = 'DailyType');
            INSERT dailyType;
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            Map<String,Id> rtMap = new Map<String,Id>();
            if(rtList!= null && rtList.size()>0){
                for(RecordType rt: rtList)
                rtMap.put(rt.Name,rt.Id);
            }
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            INSERT oBedspace;
            
            Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
            INSERT con;
            
            Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Bedspace__c =oBedspace.Id,Total_Rent_Charge__c=100,Start_date__c=Date.Today().addDays(-10),End_date__c=Date.Today().addDays(10),In_Form__Is_default__c =true);
            INSERT oRentSchedule;
            
            Rent_Stream_Charge__c oRentCharge = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=50);
            INSERT oRentCharge;
            
            Rent_Stream_Charge__c oRentCharge1 = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=100);
            INSERT oRentCharge1;
            
            Tenancy__c ten1 = new Tenancy__c();
            ten1.recordTypeid = rtMap.get('Tenancy');
            ten1.Client__c = con.Id;
            ten1.Start_date__c=Date.Today().addDays(-5);
            ten1.Status__c='Current';
            ten1.Bedspace__c=oBedspace.Id;
            Test.startTest();
            INSERT ten1;
            Test.stopTest();
            Transaction__c tran1 = new Transaction__c(Tenancy__c = ten1.Id,Amount__c=100,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-1));
            INSERT tran1;
            Transaction__c tran2 = new Transaction__c(Tenancy__c = ten1.Id,Amount__c=50,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-2));
            INSERT tran2;
            List<Transaction__c> transUpList = [SELECT Id, Name FROM Transaction__c WHERE Tenancy__c =: ten1.Id];
            System.debug('=====transUpList.size()'+transUpList.size());
            System.assertEquals(8, transUpList.size(),'SUCCESS');
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }   
    
    //Test method to close tenancy
    static testmethod void TestCloseTenancy(){
        Test.startTest();
        try{
            Tenancy__c tenancy =[SELECT Id, End_date__c FROM Tenancy__c LIMIT 1];
            tenancy.End_date__c = Date.today().adddays(-5);
            System.assertEquals(tenancy.End_date__c,Date.today().adddays(-5));
            ApexPages.standardController stdCont = new ApexPages.standardController(tenancy);
            CloseTenancy closeTenancy = new CloseTenancy(stdCont);
            closeTenancy.save();
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    }
     static testmethod void TestCloseTenancyExtension(){
         Test.startTest();
        try{
            Tenancy__c tenancy =[SELECT Id, End_date__c FROM Tenancy__c LIMIT 1];
            tenancy.End_date__c = Date.today().adddays(+5);
            System.assertEquals(tenancy.End_date__c,Date.today().adddays(+5));
            ApexPages.standardController stdCont = new ApexPages.standardController(tenancy);
            CloseTenancy closeTenancy = new CloseTenancy(stdCont);
            closeTenancy.save();
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    }
}