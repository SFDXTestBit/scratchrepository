/*------------------------------------------------------------------
/* Created By : 
 * Created date : 17/8/2015
 * Description: Scheduler for Transaction creation on daily/weekly basis.
 * History: v1.1 - Bhargav Shah. - 31/10/2018 - Security Review
------------------------------------------------------------------*/
global class scheduleTransactionCreation implements Schedulable{
   global void execute(SchedulableContext SC) {
	   	try{
	       Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
	       
	       if(!orgDefaults.Turn_off_Rent_Module_Automation__c && !orgDefaults.Turn_off_Scheduled_Transaction_Creation__c){
	           AutomatictransactionCreationBatch batchTrans = new AutomatictransactionCreationBatch();
	           Database.executeBatch(batchTrans,50);
	       }
	    }catch(Exception e){
			System.debug('Exception:'+e.getMessage());
		}   
   }
}