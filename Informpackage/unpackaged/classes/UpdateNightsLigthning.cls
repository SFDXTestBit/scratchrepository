/*
 * History: v1.1 - Sneha U. - 31/10/2018 - Added update access check,Added Try/Catch block 
*/
public  class UpdateNightsLigthning{ 
    
    @AuraEnabled
    public static String getUIThemeDescription() {
        String theme = UserInfo.getUiThemeDisplayed();
        return theme;
    }    
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        List < String > allOpts = new list < String > ();
        
            // Get the object type of the SObject.
            Schema.sObjectType objType = objObject.getSObjectType();
            
            // Describe the SObject using its object type.
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
            
            // Get a map of fields for the SObject
            map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
            
            // Get the list of picklist values for this field.
            list < Schema.PicklistEntry > values =
                fieldMap.get(fld).getDescribe().getPickListValues();
            
            // Add these values to the selectoption list.
            for (Schema.PicklistEntry a: values) {
                allOpts.add(a.getValue());
            }
            allOpts.sort();
       
        return allOpts;
    }
    
    
    
    
    //Method to update Tenancy
    @AuraEnabled
    public static String save(id recordid,string nights,String tenency){
        try{
            In_Form__Night__c night = new In_Form__Night__c();
            night = (In_Form__Night__c)JSON.deserialize(nights, In_Form__Night__c.class);
            In_Form__Tenancy__c ten = new In_Form__Tenancy__c();
            ten = (In_Form__Tenancy__c)JSON.deserialize(tenency, In_Form__Tenancy__c.class); 
            ten.id=recordid;

            //List of fields assigned for In_Form__Tenancy__c Object read access
            /*List<String> fieldList = new List<String>{
            'In_Form__Void_reason__c',
            'In_Form__Void_status__c'
            };*/

            //Check update access for In_Form__Tenancy__c Object
            //&& InForm_CRUDFLS_Util.getUpdateAccessCheck(String.valueOf(Schema.SObjectType.In_Form__Tenancy__c.getSobjectType()),fieldList)
            if(ten.In_Form__End_date__c == Date.Today() ){
                In_Form__Tenancy__c tenant = new In_Form__Tenancy__c();
                tenant.In_Form__Void_reason__c = night.In_Form__Void_reason__c;
                tenant.In_Form__Void_status__c = night.In_Form__Void_status__c;
                tenant.Id = recordid;
                update tenant;
                ten.In_Form__End_date__c = Date.Today()-1;
            }
            
            Database.executeBatch(new UpdateVoidNightBatch(ten,night),1); 
           
        }
        catch(DmlException ex){
            return 'Exception:' + ex.getMessage();
        }
        return 'Success';
        
    }
    
}