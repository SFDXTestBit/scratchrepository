/*------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 15/06/2017 09:13
 * Last Modified by:: Saurabh Gupta ,  26/09/2017 05:21
 * Description: Batch class to create dl field mapping records
 * History: v1.1 - Sneha U. - 26/10/2018 - Enforcing CRUD/FLS
 * History: v1.2 - Sneha U. - 31/10/2018 - Removed commented code, Added Read access check, Added create access check for error logs,Added Try/Catch block
------------------------------------------------------------------*/
global class BatchDLFieldMapping implements Database.Batchable<sObject>{    
    global Id batchDLId; 
    
    public BatchDLFieldMapping(Id batchId){    
        this.batchDLId = batchId;     
    }
    
    //return dl field mapping object records
    global Database.QueryLocator start(Database.BatchableContext BC){    

        //List of fields assigned for In_Form__DL_Field_Mapping__c Object read access
        /*List<String> fieldList = new List<String>{
        'In_Form__Description__c',
        'In_Form__Mapping_Status__c',
        'In_Form__Object_Field_Name__c',
        'In_Form__Object_Field_Type__c',
        'In_Form__Object_Name__c',
        'In_Form__Record_Type__c',
        'In_Form__Record_Type_Selection__c',
        'In_Form__Source_Field__c',
        'In_Form__Source_Field_Label__c',
        'In_Form__Source_Field_Type__c',
        'In_Form__Source_Object__c',
        'In_Form__Visible__c'
        };*/

        //Check read access for Tenancy Object
        //if(InForm_CRUDFLS_Util.getReadAccessCheck(String.valueOf(Schema.SObjectType.DL_Field_Mapping__c.getSobjectType()),fieldList) ){
            return Database.getQueryLocator('SELECT Id,In_Form__Description__c,In_Form__Mapping_Status__c,In_Form__Object_Field_Name__c,In_Form__Object_Field_Type__c,In_Form__Object_Name__c,In_Form__Record_Type__c,In_Form__Record_Type_Selection__c,In_Form__Source_Field__c,In_Form__Source_Field_Label__c,In_Form__Source_Field_Type__c,In_Form__Source_Object__c,In_Form__Visible__c FROM In_Form__DL_Field_Mapping__c');    
        //}
        //return null;
    }
    
    //Excute method to insert error logs and infrm__DL_Batch_Field_Mapping__c records
    global void execute(Database.BatchableContext BC, List<In_Form__DL_Field_Mapping__c> scope){   
        
            List<Error_Logs__c> errorLogList = new List<Error_Logs__c>();
            List<PartnerNetworkRecordConnection> connectionsList =  new  List<PartnerNetworkRecordConnection>();
            id networkId = BatchToCreateDLRecordHelper.getConnectionId(System.Label.ConnectionName);
            List<In_Form__DL_Batch_Field_Mapping__c> dlbatchList =  new  List<In_Form__DL_Batch_Field_Mapping__c>(); 
         
            //List of fields assigned for In_Form__DL_Batch_Field_Mapping__c Object create access
            /*List<String> fieldList = new List<String>{
                'In_Form__DL_Batch_Name__c',
                'In_Form__Description__c',
                'In_Form__Mapping_Status__c',
                'In_Form__Object_Field_Name__c',
                'In_Form__Object_Field_Type__c',
                'In_Form__Object_Name__c',
                'In_Form__Record_Type__c',
                'In_Form__Record_Type_Selection__c',
                'In_Form__Source_Field__c',
                'In_Form__Source_Field_Label__c',
                'In_Form__Source_Field_Type__c',
                'In_Form__Source_Object__c',
                'In_Form__Visible__c'
            };*/ 

            //Check create access for In_Form__DL_Batch_Field_Mapping__c and its fields 
            //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__DL_Batch_Field_Mapping__c',fieldList)){

                 for(In_Form__DL_Field_Mapping__c obj: scope){
                 
                 In_Form__DL_Batch_Field_Mapping__c dlbatchfield = new In_Form__DL_Batch_Field_Mapping__c ();
                     dlbatchfield.In_Form__DL_Batch_Name__c = batchDLId;
                     dlbatchfield.In_Form__Description__c = obj.In_Form__Description__c;
                     dlbatchfield.In_Form__Mapping_Status__c = obj.In_Form__Mapping_Status__c;
                     dlbatchfield.In_Form__Object_Field_Name__c = obj.In_Form__Object_Field_Name__c;
                    
                     dlbatchfield.In_Form__Object_Field_Type__c = obj.In_Form__Object_Field_Type__c;
                     dlbatchfield.In_Form__Object_Name__c    = obj.In_Form__Object_Name__c;
                     dlbatchfield.In_Form__Record_Type__c = obj.In_Form__Record_Type__c;
                     dlbatchfield.In_Form__Record_Type_Selection__c = obj.In_Form__Record_Type_Selection__c;
                    
                     dlbatchfield.In_Form__Source_Field__c  = obj.In_Form__Source_Field__c;
                     dlbatchfield.In_Form__Source_Field_Label__c = obj.In_Form__Source_Field_Label__c;
                     dlbatchfield.In_Form__Source_Field_Type__c = obj.In_Form__Source_Field_Type__c;
                     dlbatchfield.In_Form__Source_Object__c = obj.In_Form__Source_Object__c;
                     dlbatchfield.In_Form__Visible__c = obj.In_Form__Visible__c;
                     
                     dlbatchList.add(dlbatchfield);
                        
               
                }
            //}
            
            //insert infrm__DL_Batch_Field_Mapping__c list 
            if(dlbatchList.isempty()==false){
                insert dlbatchList;     
            
                //List of fields assigned for PartnerNetworkRecordConnection Object create access
                /*fieldList = new List<String>{
                    'ConnectionId',
                    'LocalRecordId',
                    'SendClosedTasks',
                    'SendOpenTasks',
                    'SendEmails'
                };*/ 

                //Check create access for PartnerNetworkRecordConnection and its fields 
                //if(InForm_CRUDFLS_Util.getCreateAccessCheck('PartnerNetworkRecordConnection',fieldList)){

                 //Salesforce to salesforce connect auto functionality
                 for(In_Form__DL_Batch_Field_Mapping__c dbMap: dlbatchList){
                      PartnerNetworkRecordConnection newConnection = new PartnerNetworkRecordConnection(
                    ConnectionId = networkId,
                    LocalRecordId = dbMap.id,
                    SendClosedTasks = false,
                    SendOpenTasks = false,
                    SendEmails = false);
                    
                    connectionsList.add(newConnection);
                 }
               // }
             
                //if Error occur on inserting records creating error records in  Error_Logs__c  object           
                if(!connectionsList.isEmpty()){   
                     
                    List<Database.SaveResult> connectionsListresult = Database.insert(connectionsList,false);
                    for (Database.SaveResult sr : connectionsListresult ) {
                            
                        if (sr.isSuccess()) {
                            // This condition will be executed for successful records and will fetch the ids of successful records
                            System.debug('Successfully updated . Record ID is : ' + sr.getId());
                        }
                        else {
                            errorLogList = new List<Error_Logs__c>();

                            //List of fields assigned for Error_Logs__c Object create access
                            /*fieldList = new List<String>{
                                'In_Form__Description__c'
                            };*/ 

                            //Check create access for Error_Logs__c and its fields 
                            //if(InForm_CRUDFLS_Util.getCreateAccessCheck(String.valueOf(Schema.SObjectType.Error_Logs__c.getSobjectType()),fieldList)){

                                // This condition will be executed for failed records
                                for(Database.Error objErr : sr.getErrors()) {
                                    Error_Logs__c error = new Error_Logs__c(Description__c = objErr.getMessage());
                                                    
                                    //insert error;
                                    errorLogList.add(error);
                                }     
                           // }          
                        }
                    }
                    if(!errorLogList.isEmpty()){
                         insert errorLogList;
                    }
                }  
            } 
       
    
    }
    global void finish(Database.BatchableContext BC){
    
    }
    
    
}