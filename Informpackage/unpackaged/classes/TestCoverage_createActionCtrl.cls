/*********************************************************************
Class   : TestCoverage_createActionCtrl
Author  : AKohakade
Details : Its test class for Create Action for Multiple client "createActionCtrl" controller. It covers all methods written in controller, basic functionality of controller is to fetch project list, session list, timeline events related to project and create actions.
Covered Classes:  createActionCtrl (94%)
                  createActIn_UnInstallScript (97%) 
History : v1.0 - 21/02/2018 - Created
          v1.1 - 30/10/2018 - Modified - Mayur Patil
**********************************************************************/

@isTest
public class TestCoverage_createActionCtrl{
    
    public static String nameSpacePrefix
    {
        get {
            if(nameSpacePrefix == null)
            {
                ApexClass cs = [select NamespacePrefix from ApexClass where Name ='createActionCtrl'];        
                if(String.isNotBlank(cs.NamespacePrefix))
                    nameSpacePrefix = cs.NamespacePrefix+'__';
                else
                    nameSpacePrefix = '';
            }
            return nameSpacePrefix;
        }
        set;
    }
    
    public static void setUpData(){
        try{
            List<Account> lstAccount = new List<Account>();
            List<Session__c> lstSession = new List<Session__c>();
            List<Contact> lstContact = new List<Contact>();
            List<Timeline_Event__c> lstTimeline = new List<Timeline_Event__c>();
            List<Action__c>  lstAction = new List<Action__c>();
            Map<String,String> sessionRecordTypes = new Map<String,String>();
            Map<String,String> ActionRecordTypes = new Map<String,String>();
            
            String objectNameSession = nameSpacePrefix +'Session__c';
            
            List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType=:objectNameSession and isActive=true];
            
            for(RecordType rt: rtypes)
                sessionRecordTypes.put(rt.Name,rt.Id);
            
            String objectNameAction = nameSpacePrefix +'Action__c';
            List<RecordType> rtypesAction = [Select Name, Id From RecordType where sObjectType=:objectNameAction and isActive=true];
            
            for(RecordType rt: rtypesAction)
                ActionRecordTypes.put(rt.Name,rt.Id);
            
            Account acc1 = new Account(Name = 'Base58', Show_in_session__c = true, Service_type__c = 'Accommodation', Accommodation_stage_HL__c = 'Stage 2', Accomodation_type_HL__c = 'High support');        
            lstAccount.add(acc1);
            
            INSERT lstAccount;
            
            Session__c session1 = new Session__c(Project__c = lstAccount[0].id, RecordTypeId = sessionRecordTypes.get('Day Centre'),Start_date_time__c = System.Today(), End_date_time__c = System.Today() + 30);        
            lstSession.add(session1);
            
            Session__c session2 = new Session__c(Project__c = lstAccount[0].id, RecordTypeId = sessionRecordTypes.get('Group Work'),Start_date_time__c = System.Today()-5, End_date_time__c = System.Today() + 35);
            lstSession.add(session2);
            
            INSERT lstSession;
            
            Contact con1 = new Contact(FirstName = 'Alison', LastName = 'Jones', Gender_HL__c = 'Female');
            lstContact.add(con1);
            
            Contact con2 = new Contact(FirstName = 'Mark', LastName = 'James', Gender_HL__c = 'Male');
            lstContact.add(con2);
            
            INSERT lstContact;
            
            Timeline_Event__c te1 = new Timeline_Event__c(Client__c = lstContact[0].id, Project_Lookup__c = lstAccount[0].id, Referral_date__c = System.Today()-10, Start_date__c = System.Today()-5, End_date__c = System.Today()+15);
            lstTimeline.add(te1);
            
            Timeline_Event__c te2 = new Timeline_Event__c(Client__c = lstContact[1].id, Project_Lookup__c = lstAccount[0].id, Referral_date__c = System.Today()-15, Start_date__c = System.Today()-5, End_date__c = System.Today()+15);
            lstTimeline.add(te2);
            
            INSERT lstTimeline;
            
            Action__c act1 = new Action__c(Client__c = lstContact[0].id, Timeline_Event__c = lstTimeline[0].id, Session__c = lstSession[0].id , Start_date_time__c = System.Today(), RecordTypeId = ActionRecordTypes.get('Day Centre'));       
            lstAction.add(act1);
            
            Action__c act2 = new Action__c(Client__c = lstContact[1].id, Timeline_Event__c = lstTimeline[1].id, Session__c = lstSession[1].id , Start_date_time__c = System.Today(), RecordTypeId = ActionRecordTypes.get('Group Work'));       
            lstAction.add(act2);
            
            INSERT lstAction;        
            System.assertEquals(1,lstAccount.size());
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
    
    testmethod static void testCreateAction_Method1(){
        try{
            List<Timeline_Event__c> lstEvents = new List<Timeline_Event__c>();
            Id projId;
            
            setUpData();
            
            String uniqueName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
            
            Profile pf= [Select Id from profile where Name='Force.com - Free User'];
            
            User testUser = new User(firstname = 'ABC', 
                                     lastName = 'XYZ', 
                                     email = uniqueName  + '.org', 
                                     Username = uniqueName  + '.org', 
                                     EmailEncodingKey = 'ISO-8859-1', 
                                     Alias = uniqueName.substring(18, 23), 
                                     TimeZoneSidKey = 'America/Los_Angeles', 
                                     LocaleSidKey = 'en_US', 
                                     LanguageLocaleKey = 'en_US', 
                                     ProfileId = pf.Id);     
            
            Test.startTest();
            //1. getProjectList
            List<Account> lstProj = (List<Account>)createActionCtrl.getProjectList().get('Result');          
            if(lstProj.size() > 0){
                System.assert(lstProj[0].Name == 'Base58', 'Assert failed, Project Base58 not able to access.');
                projId = lstProj[0].id;         
            }
            System.runAs(testUser){
                Map<String, Object> mapOfProjRsl_testUser = createActionCtrl.getProjectList();          
                if(mapOfProjRsl_testUser.size() > 0){
                    System.assert(mapOfProjRsl_testUser.get('Status') == 'false', 'Assert failed to check user access to Project');
                }
            }
            
            //2.getSessionList 
            List<Session__c> lstSession = (List<Session__c>)createActionCtrl.getSessionList(lstProj[0].id).get('Result');
            if(lstSession!=null && lstSession.size() > 0)
                System.assertEquals(lstSession[0].Project__c, projId);
            
            Map<String, Object> mapOfSessionRsl = createActionCtrl.getSessionList(null);          
            if(mapOfSessionRsl.size() > 0){
                System.assert(mapOfSessionRsl.get('Status') == 'false', 'Assert failed to check user access to Project');
            }
            
            System.runAs(testUser){
                Map<String, Object> mapOfSessionRsl_testUser = createActionCtrl.getSessionList(lstProj[0].id);          
                if(mapOfSessionRsl_testUser.size() > 0){
                    System.assert(mapOfSessionRsl_testUser.get('Status') == 'false', 'Assert failed to check user access to Project');
                }
            }
            
            //3.getObjFieldList
            Map<String,Map<String,String>> mapOfResult = createActionCtrl.getObjFieldList('Timeline_Event__c');
            System.assert(mapOfResult.containsKey('MapOfName') && mapOfResult.get('MapOfName').containsKey(nameSpacePrefix+'Client_gender__c'), 'getObjFieldList method failed to retrieve field data ');
            
            List<String> lstFields = new List<String>();
            lstFields.add(nameSpacePrefix+'Client_gender__c');
            
            //4.getTimeLineEventList
            Set<Timeline_Event__c> setTE = (Set<Timeline_Event__c>)createActionCtrl.getTimeLineEventList(lstProj[0].id, lstSession[0].id, lstSession[1].id, nameSpacePrefix+'Status__c', 'Open,Closed', lstFields).get('Result');
            
            System.assert(setTE.size() == 1, 'Assert failed, getTimeLineEventList not able to get actual timeline events.');
            
            Set<Timeline_Event__c> setTE1 = (Set<Timeline_Event__c>)createActionCtrl.getTimeLineEventList(lstProj[0].id, lstSession[0].id, 'showAll', nameSpacePrefix+'Status__c', 'Open,Closed', lstFields).get('Result');
            
            Map<String, Object> mapOfTE_ProjTest = createActionCtrl.getTimeLineEventList(null, lstSession[0].id, lstSession[1].id, nameSpacePrefix+'Status__c', 'Open,Closed', lstFields);
            if(mapOfTE_ProjTest.size() > 0){
                System.assert(mapOfTE_ProjTest.get('Status') == 'false', 'Assert failed to check user access to Timeline event');
            }
            
            Map<String, Object> mapOfTE_SessionTest = createActionCtrl.getTimeLineEventList(lstProj[0].id, null, lstSession[1].id, nameSpacePrefix+'Status__c', 'Open,Closed', lstFields);
            if(mapOfTE_SessionTest.size() > 0){
                System.assert(mapOfTE_SessionTest.get('Status') == 'false', 'Assert failed to check user access to Timeline event');
            }
            
            System.runAs(testUser){             
                Map<String, Object> mapOfTE_testUser = createActionCtrl.getTimeLineEventList(lstProj[0].id, lstSession[0].id, lstSession[1].id, nameSpacePrefix+'Status__c', 'Open,Closed', lstFields);
                if(mapOfTE_testUser.size() > 0){
                    System.assert(mapOfTE_testUser.get('Status') == 'false', 'Assert failed to check user access to Timeline event');
                }
            }
            
            for(Timeline_Event__c event :setTE1){
                lstEvents.add(event);
            }
            
            //5. createAction
            Map<String,Object> mapOfActionResult_Failed = createActionCtrl.createAction(lstEvents,lstSession[1].id,'Day Centre');           
            if(mapOfActionResult_Failed.get('Status') == 'true'){
                Map<String,String> mapOfFailed = (Map<String,String>)mapOfActionResult_Failed.get('Failed');
                System.assert(mapOfFailed.size() > 0, 'Assert failed, createAction failed to get details of errors.');
            }
            
            Map<String,Object> mapOfActionResult_Success = createActionCtrl.createAction(lstEvents,lstSession[0].id,'Day Centre');
            Map<String,String> mapOfSuccess = (Map<String,String>)mapOfActionResult_Success.get('Successful');
            System.assert(mapOfSuccess.size() > 0, 'Assert failed, createAction failed to get details of successfully inserted.');
            
            Map<String,Object> mapOfActionResult_Empty = createActionCtrl.createAction(lstEvents, null,'Day Centre');
            System.assert(mapOfActionResult_Empty.get('Status') == 'false', 'Assert failed, createAction failed.');
            
            System.runAs(testUser){
                Map<String, Object> mapOfAction_testUser = createActionCtrl.createAction(lstEvents,lstSession[0].id,'Day Centre');
                if(mapOfAction_testUser.size() > 0){
                    System.assert(mapOfAction_testUser.get('Status') == 'false', 'Assert failed to check user access to Action object');
                }
                
                Map<String, Object> mapOfCreateCheck_testUser = createActionCtrl.getCreateAccessCheck('Session__c', new List<String>{'Id','Project__c','Start_date_time__c'});
                if(mapOfCreateCheck_testUser.size() > 0){                    
                    System.assert(mapOfCreateCheck_testUser.get('Status') == false, 'Assert failed to check user access');
                }
            }
            
            boolean flag = createActionCtrl.checkRecordType('Day Centre');
            System.assert(flag, 'Assert failed, checkRecordType failed to getrecord type details.');
            
            boolean checkRecordType = createActionCtrl.checkRecordType('');          
            
            Test.stopTest();
        }
        catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
    
    testmethod static void testCreateActionInstallScript_Method2(){
        try{
            Test.startTest();
            createActIn_UnInstallScript installerScript = new createActIn_UnInstallScript();
            
            Test.testInstall(installerScript, null, true);
            
            System.assert(Create_Actions_custom_information__c.getOrgDefaults().Max_Limit_for_Added_Client_table__c == 100, 'Assert failed to update custom setting in Post install script');
            
            Test.testUninstall(new createActIn_UnInstallScript());
            
            Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
}