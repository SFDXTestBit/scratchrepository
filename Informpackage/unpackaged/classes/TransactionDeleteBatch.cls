/* Class Name  : TransactionDeleteBatch
 * Description   : Batch Apex class to create records for Transaction after update in Tenancy start/end date.
 * Created By    : Raushan Anand
 * Created On    : 16-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              16-Nov-2015               Initial version.
 *  * Sneha U.                   26-Oct-2018               Enforcing CRUD/FLS
 *  * Dinesh D.                  31-Oct-2018               Enforcing FLS, Try-Catch 
 *  * Extentia					 10-June-2019			   Pass the Isbulk filed value to TransactionUpdateBatch 
 *****************************************************************************************/
global with sharing class TransactionDeleteBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    Map<Id,List<Date>> tenancyDateMap = new Map<Id,List<Date>>();
     Boolean isBulk;  // added by extentia.
    
    global TransactionDeleteBatch(Map<Id,List<Date>> idDate){
        tenancyDateMap = idDate;
        
    }
        
    global TransactionDeleteBatch(Map<Id,List<Date>> idDate,Boolean isBulk){
        tenancyDateMap = idDate;
        this.isBulk = isBulk;    // added by extentia.
        
    }
    
    
    
    
    /* Method Name : start
     * Description : Batch Class Start Method
     * Return Type : Database.QueryLocator
     * Input Parameter : Database.BatchableContext
     */
    global Database.QueryLocator start(Database.BatchableContext BC){   
        try{
            Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
            
            if(orgDefaults.Turn_off_Rent_Module_Automation__c || orgDefaults.Turn_off_Transaction_on_Tenancy_Change__c){
                return null;
            }
            
            Set<Id> idSet = tenancyDateMap.Keyset();
            Id tranRecType = Schema.SObjectType.Transaction__c.getRecordTypeInfosByName().get('Rent charge').getRecordTypeId();
            
            /*List<String> fieldList = new List<String>{
            'Id',
            'In_Form__Transaction_date__c',
            'In_Form__Tenancy__c',    
            'CreatedDate'
            };*/
            //InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Transaction__c',fieldList) &&
            if( !idSet.isEmpty() && tranRecType != null)
                return Database.getQueryLocator('SELECT Id,Transaction_date__c,Tenancy__c,CreatedDate FROM Transaction__c WHERE Tenancy__c IN: idSet AND RecordType.Id =: tranRecType');        
            //else 
                //return null;
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
            return null;
        }
        return null;
    }
    /* Method Name : execute
     * Description : Batch Class execute Method to delete transaction
     * Return Type : void
     * Input Parameter : Database.BatchableContext, List of Transaction - List<Transaction__c>
     */
    global void execute(Database.BatchableContext BC, List<Transaction__c> scope){
        List<Transaction__c > tranDeleteList = new List<Transaction__c>();
        
        
            for(Transaction__c trans : scope){
                Date tenancyOldEndDate = null;
                if(tenancyDateMap.get(trans.Tenancy__c).get(3) != null){
                    tenancyOldEndDate = tenancyDateMap.get(trans.Tenancy__c).get(3).addDays(-7);
                }
                Date tenancyNewEndDate = null;
                if(tenancyDateMap.get(trans.Tenancy__c).get(1) != null){
                    tenancyNewEndDate = tenancyDateMap.get(trans.Tenancy__c).get(1).addDays(-7);
                }
                if(trans.Transaction_date__c <= tenancyDateMap.get(trans.Tenancy__c).get(0) 
                    || trans.Transaction_date__c >= tenancyNewEndDate
                    || trans.Transaction_date__c.Date() == tenancyDateMap.get(trans.Tenancy__c).get(2)
                    || trans.Transaction_date__c.Date() >= tenancyOldEndDate){
                    tranDeleteList.add(trans);
                }
            }
                systemBatchHelper.isBatchRunning=true;
            //Check delete access for Transaction__c object
            //if(InForm_CRUDFLS_Util.getDeleteRecordsAccessCheck(tranDeleteList)){
                Database.delete(tranDeleteList,false);
            //}
            
    }
    
    global void finish(Database.BatchableContext BC){
        Database.executeBatch(new TransactionUpdateBatch(tenancyDateMap,isBulk));        
    }
}