//Modified for Security Review
@isTest 
private class DLBatchTrigger_Test  {
    
    @TestSetup
    static void CreateTestData(){
        try{
            Date mydate = Date.Today();
            
            In_Form__ClientInformation__c  clientInfo = new In_Form__ClientInformation__c();
            clientInfo.In_Form__Client_Id__c = 'Test_ID';
            clientInfo.In_Form__Client_Secret__c = 'TEST_SECRET';
            clientInfo.In_Form__Connect_URL__c = 'https://login.salesforce.com/services/oauth2/authorize';
            clientInfo.In_Form__Page_Redirect_URL__c ='https://datalab.secure.force.com/DatalabConnect';
            clientInfo.In_Form__Redirect_URL__c ='https://login.salesforce.com/apex/In_Form__SalesforceConnect';
            clientInfo.In_Form__Status_URL__c = 'https://datalab.secure.force.com/services/apexrest/UpdateStatus';
            insert clientInfo;
            
            
            //In_Form__DataBatchCallOrder__mdt databactchcall =[SELECT DeveloperName FROM In_Form__DataBatchCallOrder__mdt WHERE DeveloperName ='Account'];
            
            In_Form__DL_Field_Mapping__c  dlfieldmap = new In_Form__DL_Field_Mapping__c();
            dlfieldmap.In_Form__Object_Field_Name__c = 'name' ;
            dlfieldmap.IN_FORM__OBJECT_FIELD_TYPE__C = 'STRING';
            dlfieldmap.IN_FORM__OBJECT_NAME__C = 'In_Form__DL_Project__c';
            dlfieldmap.In_Form__Record_Type_Selection__c='All Type Selection';
            dlfieldmap.IN_FORM__SOURCE_FIELD_LABEL__C = 'name';
            dlfieldmap.IN_FORM__SOURCE_FIELD_TYPE__C = 'STRING';
            dlfieldmap.IN_FORM__SOURCE_FIELD__C = 'name';
            dlfieldmap.IN_FORM__SOURCE_OBJECT__C = 'Account';
            dlfieldmap.In_Form__Visible__c = True;
            insert dlfieldmap ;

            DL_Consent__c dlconsent = new DL_Consent__c();
            dlconsent.In_Form__Consent_Start_Date__c = Date.Today().addDays(-1);
            dlconsent.In_Form__Consent_Approach__c=false;
            insert dlconsent ;
            
            Account acc = new Account ();
            acc.Name = 'Test Account';
            acc.In_Form__DL_Scope__c = true;
            acc.SP_contact_telephone_number__c = '01403 567890';
            insert acc;
            
            Contact con = new Contact();
            con.FirstName='Test';
            con.LastName='Test';
            con.Accountid= acc.id;
            insert con;
            
            In_Form__DL_Batch__c DLbatch = new In_Form__DL_Batch__c();
            DLbatch.In_Form__Batch_Status__c='Scheduled';
            insert DLbatch; 
            
      
            In_Form__Agency__c  Agency= new In_Form__Agency__c();
            Agency.Name='Test'; 
            Agency.In_Form__DL_Scope__c=true;
            insert Agency;  
            System.AssertNotEquals(Agency.Id,null); 
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
    
    static testMethod void DLBatchTriggerTest() { 
        try{
            In_Form__DL_Field_Mapping__c  dlfieldmap1 = new In_Form__DL_Field_Mapping__c();
            dlfieldmap1.In_Form__Object_Field_Name__c = 'in_form__mail__c' ;
            dlfieldmap1.IN_FORM__OBJECT_FIELD_TYPE__C = 'EMAIL';
            dlfieldmap1.IN_FORM__OBJECT_NAME__C = 'In_Form__DL_Project__c';
            dlfieldmap1.In_Form__Record_Type_Selection__c='All Type Selection';
            dlfieldmap1.IN_FORM__SOURCE_FIELD_LABEL__C = 'in_form__email__c';
            dlfieldmap1.IN_FORM__SOURCE_FIELD_TYPE__C = 'EMAIL';
            dlfieldmap1.IN_FORM__SOURCE_FIELD__C = 'in_form__email__c';
            dlfieldmap1.IN_FORM__SOURCE_OBJECT__C = 'Account';
            dlfieldmap1.In_Form__Visible__c = True;
            insert dlfieldmap1;
          
            System.AssertNotEquals(dlfieldmap1.Id,null);   
             
            In_Form__DL_Batch__c DLbatch = new In_Form__DL_Batch__c();
            DLbatch.In_Form__Batch_Status__c='Scheduled';
            insert DLbatch; 
            
            System.AssertNotEquals(DLbatch.Id,null); 
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());  
            
            List<String> orderList = new List<String>();
            
            for(In_Form__DataBatchCallOrder__mdt obj : [Select DeveloperName,MasterLabel, In_Form__Orders__c 
                                                        FROM In_Form__DataBatchCallOrder__mdt ORDER BY In_Form__Orders__c ]){
                                                            
                                                            
                orderList.add(obj.MasterLabel);                                                                
            }       
            
            BatchToCreateDLRecord batchDL = new BatchToCreateDLRecord(orderList, DLbatch.Id, 'Processing', new Set<Id>());
            
            batchDL.start(null);
            batchDL.execute(null, Database.query('SELECT Id'+batchDL.fields+' FROM '+batchDL.objName+' WHERE In_Form__DL_Scope__c = true'));
            batchDL.finish(null);
            Test.stopTest();
            
            System.AssertEquals(database.countquery('SELECT COUNT()'+' FROM In_Form__DL_Batch__c '), 2);   
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
   
    static testMethod void DLBatchTriggerTest1() { 
        try{
            In_Form__DL_Field_Mapping__c  dlfieldmap2 = new In_Form__DL_Field_Mapping__c();
            dlfieldmap2.In_Form__Object_Field_Name__c = 'in_form__email__c' ;
            dlfieldmap2.IN_FORM__OBJECT_FIELD_TYPE__C = 'EMAIL';
            dlfieldmap2.IN_FORM__OBJECT_NAME__C = 'In_Form__DL_Project__c';
            dlfieldmap2.In_Form__Record_Type_Selection__c='All Type Selection';
            dlfieldmap2.IN_FORM__SOURCE_FIELD_LABEL__C = 'in_form__email__c';
            dlfieldmap2.IN_FORM__SOURCE_FIELD_TYPE__C = 'EMAIL';
            dlfieldmap2.IN_FORM__SOURCE_FIELD__C = 'in_form__email__c';
            dlfieldmap2.IN_FORM__SOURCE_OBJECT__C = 'Account';
            dlfieldmap2.In_Form__Visible__c = True;
            insert dlfieldmap2;
            
            In_Form__DL_Batch__c DLbatch = new In_Form__DL_Batch__c();
            DLbatch.In_Form__Batch_Status__c='Scheduled';
            insert DLbatch; 
             
            System.AssertNotEquals(DLbatch.Id,null); 
            
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());  
            
            Test.startTest();
            
            List<String> orderList = new List<String>();
            
            for(In_Form__DataBatchCallOrder__mdt obj : [Select DeveloperName,MasterLabel, In_Form__Orders__c 
                                                        FROM In_Form__DataBatchCallOrder__mdt ORDER BY In_Form__Orders__c ]){
                                                            
                                                            
                orderList.add(obj.MasterLabel);                                                                
            }       
           
            BatchToCreateDLRecord batchDL = new BatchToCreateDLRecord(orderList, DLbatch.Id, 'Processing', new Set<Id>());
            
            batchDL.start(null);
            batchDL.execute(null, Database.query('SELECT Id'+batchDL.fields+' FROM '+batchDL.objName+' WHERE In_Form__DL_Scope__c = true'));
            
            
            batchDL.finish(null);
            Test.stopTest();
            
            System.AssertEquals(database.countquery('SELECT COUNT()'+' FROM In_Form__DL_Batch__c '), 2); 
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
    static testMethod void DLBatchTriggerTest2() { 
        try{
        
            In_Form__DL_Field_Mapping__c  dlfieldmap3 = new In_Form__DL_Field_Mapping__c();
            dlfieldmap3.In_Form__Object_Field_Name__c = 'In_Form__City__c' ;
            dlfieldmap3.IN_FORM__OBJECT_FIELD_TYPE__C = 'EMAIL';
            dlfieldmap3.IN_FORM__OBJECT_NAME__C = 'In_Form__DL_Project__c';
            dlfieldmap3.In_Form__Record_Type_Selection__c='Specific Record Type';
            dlfieldmap3.IN_FORM__SOURCE_FIELD_LABEL__C = 'in_form__email__c';
            dlfieldmap3.IN_FORM__SOURCE_FIELD_TYPE__C = 'EMAIL';
            dlfieldmap3.IN_FORM__SOURCE_FIELD__C = 'in_form__email__c';
            dlfieldmap3.IN_FORM__SOURCE_OBJECT__C = 'Account';
            dlfieldmap3.In_Form__Record_Type__c = 'Test';
            dlfieldmap3.In_Form__Visible__c = True;
            insert dlfieldmap3;
            
            In_Form__DL_Batch__c DLbatch = new In_Form__DL_Batch__c();
            DLbatch.In_Form__Batch_Status__c='Scheduled';
            insert DLbatch; 
            
            System.AssertNotEquals(DLbatch.Id,null); 
            
            DLbatch.In_Form__Batch_Status__c='Processing';
            Update DLbatch; 
            
            DLbatch.In_Form__Batch_Status__c='Completed';
            Update DLbatch; 
            
            System.AssertEquals(DLbatch.In_Form__Batch_Status__c,'Completed'); 
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());  
            
            Test.startTest();
            
            List<String> orderList = new List<String>();
            
            
           
            for(In_Form__DataBatchCallOrder__mdt obj : [Select DeveloperName,MasterLabel, In_Form__Orders__c 
                                                        FROM In_Form__DataBatchCallOrder__mdt ORDER BY In_Form__Orders__c ]){
                                                           
                                                            
                orderList.add(obj.MasterLabel);                                                                
            }     
            
           
           
            BatchToCreateDLRecord batchDL = new BatchToCreateDLRecord(orderList, DLbatch.Id, 'Processing', new Set<Id>());
            
            batchDL.start(null);
            batchDL.execute(null, Database.query('SELECT Id'+batchDL.fields+' FROM '+batchDL.objName+' WHERE In_Form__DL_Scope__c = true'));
            
            
            batchDL.finish(null);
            Test.stopTest();
            
            System.AssertEquals(database.countquery('SELECT COUNT()'+' FROM In_Form__DL_Batch__c '), 2); 
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
}