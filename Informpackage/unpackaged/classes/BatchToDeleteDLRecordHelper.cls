/*------------------------------------------------------------------
/* Created By : Dev Team Phiz
 * Created date : 12/07/2017 07:12
 * Last Modified by:: Dev Team Phiz ,  26/09/2017 11:47
 * Description: This class is the helper of BatchToDeleteDLRecord
------------------------------------------------------------------*/
global class BatchToDeleteDLRecordHelper Implements Database.Batchable<sObject>,Database.Stateful{  
    global List<String> orderList=new list<String>();
    
    //Return custom metadata 
    global Database.QueryLocator  start(Database.BatchableContext BC) {       
        return Database.getQueryLocator('Select In_Form__Object_name__c,In_Form__Is_Deleted__c FROM In_Form__DataBatchDeleteRecords__mdt');
    }
    
    //Exceute method to add records in list whose isdeleted value is true
    global void execute(Database.BatchableContext BC, List<In_Form__DataBatchDeleteRecords__mdt> scope){    
       for(In_Form__DataBatchDeleteRecords__mdt obj : scope){       
           if(obj.In_Form__Is_Deleted__c == True){            
              orderList.add(obj.In_Form__Object_name__c);                
           }                                                              
        }                     
    }
    
    //Excute BatchToDeleteDLRecord batch with default batch size
    global void finish(Database.BatchableContext BC){
       if(!orderList.isEmpty()){
            
            if(!Test.isRunningTest()){
                Database.executeBatch(new BatchToDeleteDLRecord(orderList));   
            } 
        }
    }    
    
}