/* Class Name  : NightDeleteBatch
 * Description   : Batch Apex class to delete records for Night after update in Tenancy start/end date.
 * Created By    : Raushan Anand
 * Created On    : 09-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              09-Nov-2015               Initial version.
 *  * Sneha U.                   26-Oct-2018               Enforcing CRUD/FLS
 *  * Dinesh D.                  31-Oct-2018               Enforcing FLS & Try-Catch
 *  * Extentia					 10-June-2019			   Pass the Isbulk filed value to NightUpdateBatch 
 
 *****************************************************************************************/
global with sharing class NightDeleteBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    Map<Id,List<Date>> tenancyDateMap = new Map<Id,List<Date>>();
     boolean isbulk ;   // added by extentia. 
    
    global NightDeleteBatch(Map<Id,List<Date>> idDate){
        tenancyDateMap = idDate;
    }
    
     global NightDeleteBatch(Map<Id,List<Date>> idDate,Boolean isbulk){  // added by extentia.
        tenancyDateMap = idDate;
        this.isbulk = isbulk;
        
    }
    /* Method Name : start
     * Description : Batch Class Start Method
     * Return Type : Database.QueryLocator
     * Input Parameter : Database.BatchableContext
     */
    global Database.QueryLocator start(Database.BatchableContext BC){   
        
            Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
            
            if(orgDefaults.Turn_off_Rent_Module_Automation__c || orgDefaults.Turn_off_Nights_on_Tenancy_Change__c){
                return null;
            }
            Set<Id> idSet = tenancyDateMap.Keyset();
            //List of fields assigned for Night__c Object read access
            /*List<String> fieldList = new List<String>{
                'Id',
                'In_Form__Date__c',
                'In_Form__Tenancy__c'
            };*/ 
                        
            //Check read access for Night__c Object
            //InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Night__c',fieldList) &&
           // if(!idSet.isEmpty())
                return Database.getQueryLocator('SELECT Id,Date__c,Tenancy__c FROM Night__c WHERE Tenancy__c IN: idSet');       
            //else
                //return null;                                      
    }
    /* Method Name : execute
     * Description : Batch Class execute Method to delete Nights
     * Return Type : void
     * Input Parameter : Database.BatchableContext, List of Nights - List<Night__c>
     */
    global void execute(Database.BatchableContext BC, List<Night__c> scope){
        List<Night__c> nightDeleteList = new List<Night__c>();
        
            for(Night__c night : scope){
                Date endDateTenancy = tenancyDateMap.get(night.Tenancy__c).get(1) == null?Date.Today().addDays(1):tenancyDateMap.get(night.Tenancy__c).get(1);
                if(night.Date__c < tenancyDateMap.get(night.Tenancy__c).get(0) || night.Date__c >= endDateTenancy){
                    nightDeleteList.add(night);
                }
            }

            //Check delete access for Night object
            //InForm_CRUDFLS_Util.getDeleteRecordsAccessCheck(nightDeleteList) && 
            if(!nightDeleteList.isempty()){
                systemBatchHelper.isBatchRunning=true;
                Database.delete(nightDeleteList,false);
            }            
    }
    
    global void finish(Database.BatchableContext BC){
        try{
            if(!Test.isRunningTest()){
                Database.executeBatch(new NightUpdateBatch(tenancyDateMap,isbulk));
            }
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
}