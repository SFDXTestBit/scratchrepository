/*------------------------------------------------------------------
* Created By : Saurabh Gupta
* Created date :  14/02/2017 06:19
* Last Modified by:: Saurabh Gupta ,  26/09/2017 11:58
* Description: Class to call page Redirect Code
* History: v1.1 - Dinesh D. - 31/10/2018 - Enforcing EscapeSignleQuotes 
------------------------------------------------------------------*/
public class CreateParticipantObject{
    
    //Page Redirect Code
    public PageReference submitData() {
        PageReference pageRef=null;
            ClientInformation__c clientInfo = ClientInformation__c.getOrgDefaults();        
        String authCode = ApexPages.CurrentPage().getParameters().get('code');
        if(authCode != null && clientInfo != null && clientInfo.Page_Redirect_URL__c != null){
            pageRef = new PageReference(clientInfo.Page_Redirect_URL__c+'?org_id='+ UserInfo.getOrganizationId() + '&code=' + String.escapeSingleQuotes(authCode));              
        }        
        return pageRef;    
    }
}