/*------------------------------------------------------------------
 * Created By : Saurabh Gupta 
 * Created date : 23/03/2017 13:13
 * Last Modified by:: Saurabh Gupta,  26/09/2017 11:22
 * Description: Batch class to call BatchToCreateDLRecordHelper class
 * History: v1.1 - Dinesh D. - 31/10/2018 - Enforcing FLS check & Try-Catch block
------------------------------------------------------------------*/
global with sharing class BatchContactDLScope implements Database.Batchable<sObject>{   
    //Initialization
    global Id batchDLId;
    
    //Constructor
    global BatchContactDLScope(Id batchId){       
        this.batchDLId = batchId;
    } 
    
    //Start method to return contact records
    global Database.QueryLocator start(Database.BatchableContext BC){
        try{
        //List of fields assigned for Contact Object read access
        /*List<String> fieldList = new List<String>{
            'Id',
            'In_Form__DL_Scope__c'                         
        };*/         
        
        //Check read access for Contact Object
        //if(InForm_CRUDFLS_Util.getReadAccessCheck('Contact',fieldList) )
            return Database.getQueryLocator('SELECT Id,In_Form__DL_Scope__c FROM Contact');
        //else                 
            //return null;
        }catch(Exception e){            
            System.debug('Exception:'+e.getMessage());            
            return null;
        }    
    }
    
    //Execute method to update contact dl Scope field
    global void execute(Database.BatchableContext BC, List<sObject> scope){   
    
        set<Id> conIds = new set<Id>();  
        Map<Id,Contact> conMap = new Map<Id,Contact>();
        
            for(Contact con : (List<Contact>)Scope){       
                con.In_Form__DL_Scope__c = False;
                conIds.add(con.Id);
                conMap.put(con.Id,con);    
            }
            
            List<String> fieldList = new List<String>{
                'Id',
                'In_Form__Client__c',
                'In_Form__DL_Scope__c'
            };    
            //InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Timeline_Event__c',fieldList) && 
            if(!conIds.isEmpty()){
            
                for(In_Form__Timeline_Event__c obj : [SELECT Id,Client__c,DL_Scope__c FROM Timeline_Event__c WHERE DL_Scope__c = true AND Client__c IN:conIds]){
            
                Contact con = conMap.get(obj.Client__c);
                con.DL_Scope__c = true;
                conMap.put(con.Id,con);
                
                }
       
                if(!conMap.isEmpty()){            
                    List<Database.SaveResult> resultsList = Database.update(conMap.values(), false);
                    for (Database.SaveResult result : resultsList ) { if (!result.isSuccess()){ for (Database.Error err : result.getErrors()){
                            System.debug('Error: '+ err.getStatusCode() + ' ' + err.getMessage());
                        }
                      }
                    }
               }
            }           
    }   
    
    //Finish Method to execute BatchAgencyDLScope class with batch size 200
    global void finish(Database.BatchableContext BC){
        try{
            Database.executeBatch(new BatchAgencyDLScope(batchDLId),200);
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }                        
    }   
}