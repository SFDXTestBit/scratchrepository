/* Class Name  : NightCreationBatch
 * Description   : Apex class to create a night record everyday at 3.00 AM
 * Created By    : Raushan Anand
 * Created On    : 03-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              03-Nov-2015               Initial version.
 *  * Sneha U.                   26-Nov-2018               Enforcing CRUD/FLS
 *  * Dinesh D.                  31-Oct-2018               Enforcing FLS/Try-Catch in Start method  
 *****************************************************************************************/
global with sharing class NightCreationBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC){
        try{
            //List of fields assigned for Tenancy__c Object read access
            /*List<String> fieldList = new List<String>{
                'Id',
                'Name',
                'In_Form__Bedspace__c',
                'RecordType.Name',    
                'In_Form__Void_Status__c',
                'In_Form__Void_Reason__c'                
            };*/ 
            //Check read access for Tenancy__c Object
           // if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',fieldList) )
                Date today = System.Today();
                return Database.getQueryLocator('SELECT Id, Name, Bedspace__c, RecordType.Name, Void_Status__c, Void_Reason__c,(SELECT Id FROM Nights__r WHERE Date__c=: today) FROM Tenancy__c where Bedspace__c != null And Status__c = \'Current\'');
            //else                 
                //return null;
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
            return null;
        }        
    }

    global void execute(Database.BatchableContext BC, List<Tenancy__c> scope){
        List<Night__c> nightList = new List<Night__c>();
        try
        {
            //List of fields assigned for In_Form__Night__c Object create access
            /*List<String> fieldList = new List<String>{
                'In_Form__Date__c',
                'In_Form__Tenancy__c',    
                'In_Form__Void_Status__c',
                'In_Form__Void_Reason__c'
            };*/ 

            //Check create access for In_Form__Night__c and its fields 
            //if(InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Night__c',fieldList)){   
                
                for(Tenancy__c otenancy : scope){
                    if(otenancy.Nights__r.size() ==0){
                        Night__c night = new Night__c(Date__c=date.today(),Tenancy__c = otenancy.Id);
    
                        if(otenancy.RecordType.Name == 'Void'){
                            night.Void_Status__c = otenancy.Void_Status__c;
                            night.Void_Reason__c = otenancy.Void_Reason__c;
                        }
    
                        nightList.add(night);
                    }
                }
            //}
            
            if(!nightList.isEmpty()){
                systemBatchHelper.isBatchRunning=true;
                INSERT nightList;
            }
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC){
        
    }
}