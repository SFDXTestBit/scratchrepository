/*------------------------------------------------------------------
/* Created By : 
 * Created date : 12/8/2015
 * Description: Utility class to update status on Bedspace and deletion of Transaction on Tenancy update.
------------------------------------------------------------------*/ 
public with sharing class TenancyTriggerUtility {
    
    public static Boolean hasRun = false;
    public static Boolean hasRunTransaction = false;
    public static Boolean hasRunRentStreamCharge = false;
    
    public static void updateNightsOnVoidReasonChange(List<Tenancy__c> tenList){
        try{
            Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
            
            if(orgDefaults.Turn_off_Rent_Module_Automation__c || orgDefaults.Turn_off_Bedspace_Status_Update__c){
                return;
            }
            
            Id voidRecordTypeId = Schema.SObjectType.Tenancy__c.getRecordTypeInfosByName().get('Void').getRecordTypeId();
            
            Map<Id, Tenancy__c> voidTenancies = new Map<Id, Tenancy__c>();
            
            for(Tenancy__c ten : tenList){
                if(ten.RecordTypeId == voidRecordTypeId && ten.Status__c == 'Current'){
                    voidTenancies.put(ten.Id, ten);
                }
            }
            //List<String> nightFieldList = new List<String>{'In_Form__Tenancy__c'};
            //if(InForm_CRUDFLS_Util.getUpdateAccessCheck('In_Form__Night__c',nightFieldList)){
                List<Night__c> nights = [SELECT Id, Tenancy__c FROM Night__c WHERE Tenancy__c =: voidTenancies.keySet() AND Date__c = TODAY];
            
                for(Night__c night : nights){
                    if(voidTenancies.containsKey(night.Tenancy__c)){
                        night.Void_Status__c = voidTenancies.get(night.Tenancy__c).Void_Status__c;
                        night.Void_Reason__c = voidTenancies.get(night.Tenancy__c).Void_Reason__c;
                    }
                }
                
                if(!nights.isEmpty()){
                    update nights;
                }
            //}
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    // Update Status whenever Tenacy Status or Void Status update
    public static void updateStatusonBedspace(List<Tenancy__c> tennUpList){
        try{
            Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
            
            if(orgDefaults.Turn_off_Rent_Module_Automation__c || orgDefaults.Turn_off_Bedspace_Status_Update__c){
                return;
            }
            
            Set<Id> bedspaceIdSet = new Set<Id>();
            for(Tenancy__c infTen : tennUpList){
                bedspaceIdSet.add(infTen.Bedspace__c);   
            }
            //List<String> dedspaceFieldList = new List<String>{'In_From__Status__c'};
            //if(InForm_CRUDFLS_Util.getUpdateAccessCheck('In_From__Bedspace__c',dedspaceFieldList)){
                Map<Id,Bedspace__c> bedMap = new Map<Id,Bedspace__c>([Select Id, Status__c FROM Bedspace__c where Id IN: bedspaceIdSet]);
                List<Bedspace__c> updateList = new List<Bedspace__c>();
                Map<Id,List<Tenancy__c>> maptenancy = new Map<Id,List<Tenancy__c>>();
                //List<String> tenancyFieldList = new List<String>{'In_From__Status__c','In_From__Bedspace__c','In_From__Void_status__c'};
                //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',tenancyFieldList)){
                    List<Tenancy__c> tenancyList = [SELECT Id,recordType.Name,Name,Status__c,Bedspace__c,Void_status__c FROM Tenancy__c WHERE Bedspace__c IN: bedspaceIdSet];
                    for(Tenancy__c itl : tenancyList){
                        if(maptenancy.containsKey(itl.Bedspace__c)){
                            List<Tenancy__c> tempList = maptenancy.get(itl.Bedspace__c);
                            tempList.add(itl);
                            maptenancy.put(itl.Bedspace__c,tempList);
                        }
                        else{
                            List<Tenancy__c> tempList = new List<Tenancy__c>();
                            tempList.add(itl);
                            maptenancy.put(itl.Bedspace__c,tempList);
                        }
                        
                    }  
                    for(Id bedspaceId : maptenancy.keySet()){
                        List<Tenancy__c> bedSpaceList = maptenancy.get(bedspaceId);
                        boolean status=false,voidStatus=false;
                        Map<Id,String> bedStatusMap = new map<Id,String>();
                        for(Tenancy__c it: bedSpaceList){
                            bedStatusMap.put(it.Id,it.Status__c);
                            if(it.Status__c != null && it.recordType.Name == 'Tenancy' && it.Status__c == 'Current'){
                                status=true;
                            }
                            
                            else{
                                if(!(it.recordType.Name == 'Tenancy' && it.Status__c == 'Current')){
                                    if(!(it.recordType.Name == 'Void' && it.Status__c == 'Current' && it.Void_Status__c == 'Available To let')){
                                        if(it.recordType.Name == 'Void' && it.Status__c == 'Current' && it.Void_Status__c == 'Unavailable'){
                                            if(status==false && voidStatus == false){
                                                Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                                                bedSpaceUpdate.STATUS__c = 'Void - unavailable';
                                                bedMap.put(bedspaceId,bedSpaceUpdate);
                                            }
                                            continue;
                                        }
                                    }
                                    else{
                                        if(status==false){
                                            Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                                            bedSpaceUpdate.STATUS__c = 'Void - available to let';
                                            bedMap.put(bedspaceId,bedSpaceUpdate);
                                            voidStatus = true;
                                        }
                                        continue;
                                    }
                                }
                            }
                                            
                        }
                       
                        if(status==true){
                            Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                            bedSpaceUpdate.STATUS__c = 'Let';
                            bedMap.put(bedspaceId,bedSpaceUpdate);
                            continue;
                        }
                        Integer statusMapSize = bedStatusMap.size();
                        List<String> lstStr = new List<String>();
                        for(Id bId:bedStatusMap.keySet()){
                            if(bedStatusMap.get(bId) != 'Current')
                               lstStr.add(bedStatusMap.get(bId)) ;
                        } 
                        if(statusMapSize == lstStr.size()){
                            Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                            bedSpaceUpdate.STATUS__c = 'Void - available to let';
                            bedMap.put(bedspaceId,bedSpaceUpdate);
                        }
                    }   
                    if(bedMap.size()>0){
                        UPDATE bedMap.values(); 
                    }
                //}
            //}
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    // Delete Transaction whenever Tenancy is closed by populating End date
    public static void closeTenancy(List<Tenancy__c> tenUpList){
        try{
            Map<Id,DateTime> tenIdDateMap =new Map<Id,DateTime>();
            for(Tenancy__c infTen : tenUpList){
                if(infTen.End_date__c!=null){
                    tenIdDateMap.put(infTen.Id,infTen.End_date__c);
                }      
            }

            List<Transaction__c> listTrans = [SELECT Id,Transaction_date__c,Tenancy__c FROM Transaction__c WHERE Tenancy__c IN: tenIdDateMap.keySet()];
            List<Transaction__c> listTransDel = new List<Transaction__c>();
            for(Transaction__c tran: listTrans){
                if((tran.Transaction_date__c > DateTime.now() && tran.Transaction_date__c <= tenIdDateMap.get(tran.Tenancy__c))
                  ||(tran.Transaction_date__c <= DateTime.now() && tran.Transaction_date__c > tenIdDateMap.get(tran.Tenancy__c))){
                    listTransDel.add(tran);
                }
            }
            //&& InForm_CRUDFLS_Util.getDeleteRecordsAccessCheck(listTransDel)
            if(!listTransDel.isEmpty() ){
                Delete listTransDel;
            }
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    //Update Status on Bedspace based upon deletion of any tenancy
    Public static void updateonDelStatusonBedspace(List<Tenancy__c> tennUpList){
        try{
            Set<Id> bedspaceIdSet = new Set<Id>();
            for(Tenancy__c infTen : tennUpList){
                bedspaceIdSet.add(infTen.Bedspace__c);   
            }
            //List<String> dedspaceFieldList = new List<String>{'In_From__Status__c'};
            //if(InForm_CRUDFLS_Util.getUpdateAccessCheck('In_From__Bedspace__c',dedspaceFieldList)){
                Map<Id,Bedspace__c> bedMap = new Map<Id,Bedspace__c>([Select Id, Status__c FROM Bedspace__c where Id IN: bedspaceIdSet]);
                List<Bedspace__c> updateList = new List<Bedspace__c>();
                Map<Id,List<Tenancy__c>> maptenancy = new Map<Id,List<Tenancy__c>>();
                List<Tenancy__c> tenancyList = new List<Tenancy__c>();
                List<String> tenancyFieldsList = new List<String>{'In_Form__Status__c','In_Form__Bedspace__c','In_Form__Void_status__c'};
                //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',tenancyFieldsList)){
                    tenancyList = [SELECT Id,recordType.Name,Name,Status__c,Bedspace__c,Void_status__c FROM Tenancy__c WHERE Bedspace__c IN: bedspaceIdSet];
                //}
                if(tenancyList == null || tenancyList.size()==0){
                    for(Id bsId : bedMap.keySet()){
                        Bedspace__c bedSpaceUpdate = bedMap.get(bsId);
                        bedSpaceUpdate.STATUS__c = 'Void - available to let';
                        bedMap.put(bsId,bedSpaceUpdate);
                    }
                }else{
                    for(Tenancy__c itl : tenancyList){
                        if(maptenancy.containsKey(itl.Bedspace__c)){
                            List<Tenancy__c> tempList = maptenancy.get(itl.Bedspace__c);
                            tempList.add(itl);
                            maptenancy.put(itl.Bedspace__c,tempList);
                        }
                        else{
                            List<Tenancy__c> tempList = new List<Tenancy__c>();
                            tempList.add(itl);
                            maptenancy.put(itl.Bedspace__c,tempList);
                        }
                        
                    }  
                    for(Id bedspaceId : maptenancy.keySet()){
                        List<Tenancy__c> bedSpaceList = maptenancy.get(bedspaceId);
                        boolean status=false,voidStatus=false;
                        Map<Id,String> bedStatusMap = new map<Id,String>();      
                        for(Tenancy__c it: bedSpaceList){
                            bedStatusMap.put(it.Id,it.Status__c);
                            if(it.Status__c != null && it.recordType.Name == 'Tenancy' && it.Status__c == 'Current'){
                                status=true;
                            }
                            else{
                                if(!(it.recordType.Name == 'Tenancy' && it.Status__c == 'Current')){
                                    if(!(it.recordType.Name == 'Void' && it.Status__c == 'Current' && it.Void_Status__c == 'Available To let')){
                                        if(it.recordType.Name == 'Void' && it.Status__c == 'Current' && it.Void_Status__c == 'Unavailable'){
                                            if(status==false && voidStatus == false){
                                                Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                                                bedSpaceUpdate.STATUS__c = 'Void - unavailable';
                                                bedMap.put(bedspaceId,bedSpaceUpdate);
                                            }
                                            continue;
                                        }
                                    }
                                    else{
                                        if(status==false){
                                            Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                                            bedSpaceUpdate.STATUS__c = 'Void - available to let';
                                            bedMap.put(bedspaceId,bedSpaceUpdate);
                                            voidStatus = true;
                                        }
                                        continue;
                                    }
                                }
                                else{
                                    if(status==false){
                                        Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                                        bedSpaceUpdate.STATUS__c = 'Let';
                                        bedMap.put(bedspaceId,bedSpaceUpdate);
                                    }
                                    continue;
                                }
                            }
                            
                        }
                        if(status==true){
                            Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                            bedSpaceUpdate.STATUS__c = 'Let';
                            bedMap.put(bedspaceId,bedSpaceUpdate);
                            continue;
                        }
                        Integer statusMapSize = bedStatusMap.size();
                        List<String> lstStr = new List<String>();
                        for(Id bId:bedStatusMap.keySet()){
                            if(bedStatusMap.get(bId) != 'Current')
                               lstStr.add(bedStatusMap.get(bId)) ;
                        } 
                        if(statusMapSize == lstStr.size()){
                            Bedspace__c bedSpaceUpdate = bedMap.get(bedspaceId);
                            bedSpaceUpdate.STATUS__c = 'Void - available to let';
                            bedMap.put(bedspaceId,bedSpaceUpdate);
                        }
                    }   
                }
                if(bedMap.size()>0){
                    UPDATE bedMap.values(); 
                }
           // }    
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    public static void validateTenancy(List<Tenancy__c> tenList){
       
            Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
            
            if(orgDefaults.Turn_off_Rent_Module_Automation__c){
                return;
            }
            
            Map<Id,Set<Date>> clientDateMap = new Map<Id,Set<Date>>();
            Map<Id,Set<Date>> bedSpaceDateMap = new Map<Id,Set<Date>>();
            Map<Id,Tenancy__c> clientTenancyMap = new Map<Id, Tenancy__c>();
            Map<Id,Tenancy__c> bedspaceTenancyMap = new Map<Id, Tenancy__c>();
            
            for(Tenancy__c ten : tenList){
                
                Set<Date> dtSet = new Set<Date>();
                
                Date endDate = ten.End_Date__c == null?Date.Today():(ten.End_Date__c-1);
                for(Date dt = ten.Start_Date__c; dt <= endDate; dt = dt.addDays(1)){
                    dtSet.add(dt);
                }
                
                if(ten.Client__c != null){
                    clientTenancyMap.put(ten.Client__c, ten);
                    clientDateMap.put(ten.Client__c, dtSet);
                }
                bedSpaceDateMap.put(ten.Bedspace__c, dtSet);
                bedspaceTenancyMap.put(ten.Bedspace__c, ten);
            }
            
            if(!orgDefaults.Turn_off_Client_Tenancy_Overlap__c){
                //List<String> tenancyFieldsList = new List<String>{'In_Form__Start_Date__c','In_Form__End_Date__c','In_Form__Client__c'};
                //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',tenancyFieldsList)){
                    for(Tenancy__c ten : [SELECT Id, Start_Date__c, End_Date__c, Client__c FROM Tenancy__c WHERE Client__c IN : clientDateMap.keySet() and id not in:tenList]){
                        
                        Date endDate = ten.End_Date__c == null?Date.Today():(ten.End_Date__c-1);
                        if(clientDateMap.get(ten.Client__c).contains(ten.Start_date__c) || clientDateMap.get(ten.Client__c).contains(endDate)){
                            clientTenancyMap.get(ten.Client__c).addError('This Client is already in a Tenancy for this period. Please check the details you\'ve entered.');
                        }
                    }
                //}
            }
        
    }
    public static void validateNights(List<In_Form__Tenancy__c> tenList,Map<id,In_Form__Tenancy__c> mapOldTenancy){
       
            set<id> setBedSpace=new set<id>();
            set<date> setDate=new set<date>();
            set<id> setTenancy=new set<id>();
            for(In_Form__Tenancy__c ten: tenList){
                
                if(mapOldTenancy==null){
                    if(ten.In_Form__Bedspace__c<>null){
                        setBedSpace.add(ten.In_Form__Bedspace__c);
                    }
                    Date endDate;
                
                    if(ten.End_date__c == null){
                        endDate = Date.today();
                    }else{
                        endDate = ten.End_date__c.addDays(-1);
                    }
                    
                    for(Date dt = ten.Start_date__c ; dt<= endDate ; dt = dt.addDays(1)){
                        setDate.add(dt);
                    }
                }else{
                    if(mapOldTenancy.get(ten.id).Start_date__c <> ten.Start_date__c || mapOldTenancy.get(ten.id).End_date__c<> ten.End_date__c){
                        if(ten.In_Form__Bedspace__c<>null){
                            setBedSpace.add(ten.In_Form__Bedspace__c);
                        }
                        Date endDate;
                    
                        if(ten.End_date__c == null){
                            endDate = Date.today();
                        }else{
                            endDate = ten.End_date__c.addDays(-1);
                        }
                        
                        for(Date dt = ten.Start_date__c ; dt<= endDate ; dt = dt.addDays(1)){
                            setDate.add(dt);
                        }
                        setTenancy.add(ten.id);
                    }
                
                }
            }
            set<id> setTenancyId=new set<id>();
            //List<String> tenancyFieldsList = new List<String>{'In_Form__Void_reason__c','In_Form__Void_status__c','In_Form__Client__c','In_Form__Bedspace__c'};
            //if(InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',tenancyFieldsList)){
                for(In_Form__Tenancy__c tenancy: [Select id,In_Form__Void_reason__c,In_Form__Void_status__c,name from In_Form__Tenancy__c where In_Form__Bedspace__c in:setBedSpace]){
                    setTenancyId.add(tenancy.id);
                }
                integer i=1;
                String sBreak = '</br>';
                String Error='Affected dates:'+sBreak;
                for(In_Form__Night__c night: [SELECT id,In_Form__Void_reason__c,In_Form__Void_status__c,In_Form__Date__c,In_Form__Tenancy__c FROM In_Form__Night__c WHERE In_Form__Tenancy__c IN:setTenancyId AND In_Form__Tenancy__c not in:setTenancy AND In_Form__Date__c IN:setDate order by In_Form__Date__c desc]){
                    Datetime dt = datetime.newInstance(night.In_Form__Date__c.year(), night.In_Form__Date__c.month(),night.In_Form__Date__c.day());
                    String formattedDate = dt.format('dd/MM/YYYY');
                    Error=Error+' ['+formattedDate+']' +sBreak ;
                }
                
                if(Error!='Affected dates:'+sBreak){
                    map<id,String> mapStringRecordtype=new map<id,String>();
                    for(RecordType rt:[Select id,name from Recordtype where sobjecttype ='In_Form__Tenancy__c']){
                        mapStringRecordtype.put(rt.id,rt.Name);
                    }
                    for(In_Form__Tenancy__c ten: tenList){
                        Error='The following Nights covered by this '+mapStringRecordtype.get(ten.Recordtypeid)+' record have already been accounted for on a different Tenancy / Void record. Please either change the Start date and / or the End date to avoid these dates or edit the existing Tenancy / Void record(s) before attempting to create this one again. Otherwise, please contact your System Administrator for assistance.'+sBreak+sBreak  +Error;
                        ten.adderror(Error,false);
                    }
                }
            //}
       
    }
   
}