@isTest
public class SplitRentCharge_Test {
    //Test data setup
    @testSetup
    public static void setupData(){
        try{
            Transaction_Type__c dailytype = new Transaction_Type__c(Daily_Amount__c = false,Is_Active__c=true,Type__c = 'Daily',Name = 'DailyType');
            INSERT dailyType;
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            List<RecordType> rtTransList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Transaction__c'];
            Map<String,Id> rtMap = new Map<String,Id>();
            Map<String,Id> rtTransMap = new Map<String,Id>();
            if(!rtTransList.isEmpty()){
                for(RecordType rt: rtTransList)
                    rtTransMap.put(rt.Name,rt.Id);
            }
            if(!rtList.isEmpty()){
                for(RecordType rt: rtList)
                    rtMap.put(rt.Name,rt.Id);
            }
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace');
            INSERT oBedspace;
            
            Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
            INSERT con;
            
            Tenancy__c ten1 = new Tenancy__c();
            ten1.recordTypeid = rtMap.get('Tenancy');
            ten1.Client__c = con.Id;
            ten1.Start_date__c=Date.Today().addDays(-364);
            ten1.End_date__c=Date.Today();
            ten1.Status__c='Current';
            ten1.Bedspace__c=oBedspace.Id;
            INSERT ten1;
            List<Transaction__c> tranList = new List<Transaction__c>();
            Transaction__c tran1 = new Transaction__c(Tenancy__c = ten1.Id,RecordTypeId = rtTransMap.get('Rent charge'),Amount__c=-100,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-1));
            tranList.add(tran1);
            Transaction__c tran2 = new Transaction__c(Tenancy__c = ten1.Id,RecordTypeId = rtTransMap.get('Rent charge'),Amount__c=-50,Type__c='Housing benefit',Transaction_Date__c = DateTime.Now().adddays(-2));
            tranList.add(tran2);
            INSERT tranList;
            List<Transaction__c> rsList = [SELECT Id,Name FROM Transaction__c WHERE Tenancy__c=:ten1.Id];
            System.assertEquals(2, rsList.size(), '');
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    }
    
    //Test method when Amount sum doesn't matches total amount
    public static testmethod void splitRentChargeAmountMismatchTest(){
        try{
            List<Bedspace__c> bedList = [SELECT Id,Name FROM Bedspace__c WHERE Name='testBedSpace'];
            if(!bedList.isEmpty()){
                Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Bedspace__c =bedList[0].Id, Start_date__c =Date.today().adddays(-4),End_date__c =Date.today().adddays(-1));
                Test.StartTest();
                PageReference myVfPage = Page.SplitRentCharge;
                Test.setCurrentPage(myVfPage);
                    ApexPages.StandardController sc = new ApexPages.StandardController(oRentSchedule);
                    SplitRentCharge testController = new SplitRentCharge(sc);
                    testController.save();
                Test.StopTest();
                List<Rent_Schedule__c> rsList = [SELECT Id,Name FROM Rent_Schedule__c WHERE Bedspace__c=:bedList[0].Id];
                System.assertEquals(0, rsList.size(), 'Amount Split does not match');
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }     
    }
    public static testmethod void splitRentCharge(){
        try{
            List<Bedspace__c> bedList = [SELECT Id,Name FROM Bedspace__c WHERE Name='testBedSpace'];
            if(!bedList.isEmpty()){
                Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Bedspace__c =bedList[0].Id, Start_date__c =Date.today().adddays(-4),End_date__c =Date.today(),In_Form__Is_default__c=true);
                insert oRentSchedule;
                Test.StartTest();
                    PageReference myVfPage = Page.SplitRentCharge;
                    Test.setCurrentPage(myVfPage);
                    ApexPages.currentPage().getParameters().put('bid',bedList[0].Id);
                    ApexPages.StandardController sc = new ApexPages.StandardController(oRentSchedule);
                    SplitRentCharge testController = new SplitRentCharge(sc);
                    testController.save();
                Test.StopTest();
                List<Rent_Schedule__c> rsList = [SELECT Id,Name FROM Rent_Schedule__c WHERE Bedspace__c=:bedList[0].Id];
                System.assertEquals(1, rsList.size(), 'SplitRentCharge');
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }       
    }
    
    
    
    // Test method when Start date is a future date
    public static testmethod void splitRentChargeFutureStartDateTest(){
        try{
            List<Bedspace__c> bedList = [SELECT Id,Name FROM Bedspace__c WHERE Name='testBedSpace'];
            if(!bedList.isEmpty()){
                Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Bedspace__c =bedList[0].Id, Start_date__c =Date.today().adddays(4),Total_Rent_Charge__c=100,End_date__c =Date.today().adddays(+1));
                Test.StartTest();
                    PageReference myVfPage = Page.SplitRentCharge;
                    Test.setCurrentPage(myVfPage);
                    ApexPages.StandardController sc = new ApexPages.StandardController(oRentSchedule);
                    SplitRentCharge testController = new SplitRentCharge(sc);
                    testController.amountMap.put('Housing benefit',50);
                    testController.amountMap.put('Residential contribution',50);
                    testController.save();
                Test.StopTest();    
                List<Rent_Schedule__c> rsList = [SELECT Id,Name FROM Rent_Schedule__c WHERE Bedspace__c=:bedList[0].Id];
                System.assertEquals(0, rsList.size(), 'Future Start Date');
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    }
    
    //Test method for Success scenario
    public static testmethod void splitRentChargeSucccessTest(){
        try{
            List<Bedspace__c> bedList = [SELECT Id,Name FROM Bedspace__c WHERE Name='testBedSpace'];
            Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Start_date__c =Date.today().adddays(-4),Total_Rent_Charge__c=100,End_date__c =Date.today().adddays(+1),In_Form__Is_default__c=true);
            if(!bedList.isEmpty()){
            Test.StartTest();
            PageReference myVfPage = Page.SplitRentCharge;
            Test.setCurrentPage(myVfPage);
                ApexPages.currentPage().getParameters().put('bid',bedList[0].Id);
                ApexPages.StandardController sc = new ApexPages.StandardController(oRentSchedule);
                SplitRentCharge testController = new SplitRentCharge(sc);
                testController.amountMap.put('Housing benefit',50);
                testController.amountMap.put('Residential contribution',50);
                testController.save();
            Test.StopTest();
            List<Rent_Schedule__c> rsList = [SELECT Id,Name FROM Rent_Schedule__c WHERE Bedspace__c=:bedList[0].Id];
            System.assertEquals(0, rsList.size(), 'SUCCESS');
            }
        }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    }
    
    //Test method for failure scenario
    public static testmethod void splitRentChargeFailureTest(){
        try{
            List<Bedspace__c> bedList = [SELECT Id,Name FROM Bedspace__c WHERE Name='testBedSpace'];
            Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Start_date__c =Date.today().adddays(-4),Total_Rent_Charge__c=100);
            if(!bedList.isEmpty()){
                Test.StartTest();
                    PageReference myVfPage = Page.SplitRentCharge;
                    Test.setCurrentPage(myVfPage);
                    ApexPages.StandardController sc = new ApexPages.StandardController(oRentSchedule);
                    SplitRentCharge testController = new SplitRentCharge(sc);
                    testController.amountMap.put('Housing benefit',50);
                    testController.amountMap.put('Residential contribution',50);
                    testController.save();
                Test.StopTest();    
                List<Rent_Schedule__c> rsList = [SELECT Id,Name FROM Rent_Schedule__c WHERE Bedspace__c=:bedList[0].Id];
                System.assertEquals(0, rsList.size(), 'Failure');
            }
         }catch(Exception ex){
            System.debug('Exception:'+ex.getMessage());
        }    
    }
    
}