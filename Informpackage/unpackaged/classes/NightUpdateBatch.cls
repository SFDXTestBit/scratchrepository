/* Class Name  : NightUpdateBatch
 * Description   : Batch Apex class to create records for Night after update in Tenancy start/end date.
 * Created By    : Raushan Anand
 * Created On    : 03-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              09-Nov-2015               Initial version.
 *  * Dinesh D.                  31-oct-2018               Enforcing FLS/Null Check added
 *  * Extentia					 10-June-2019			   Pass the Isbulk filed value to TransactionDeleteBatch   
 *****************************************************************************************/
global with sharing class NightUpdateBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    Map<Id,List<Date>> tenancyDateMap = new Map<Id,List<Date>>();
    Map<ID, Date> tenancyStartMap = new Map<Id,Date>();
    
     boolean isBulk;  // added by extentia
    
    global NightUpdateBatch(Map<Id,List<Date>> idList){
        tenancyDateMap=idList;
    }
    
     global NightUpdateBatch(Map<Id,List<Date>> idList,boolean isBulk){   // added by extentia.
        tenancyDateMap=idList;
        This.isBulk = isBulk;  
    }
    
    
    /* Method Name : start
     * Description : Batch Class Start Method
     * Return Type : List of Night Records - List<Night__c>
     * Input Parameter : Database.BatchableContext
     */
    global List<Night__c> start(Database.BatchableContext BC){
        try{
            Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
            
            if(orgDefaults.Turn_off_Rent_Module_Automation__c || orgDefaults.Turn_off_Nights_on_Tenancy_Change__c){
                return null;
            }
            Set<Id> idSet = tenancyDateMap.Keyset();
            Map<Id,String> newMap = new Map<Id,String>();
            List<Night__c> nightList = new List<Night__c>();
            Map<Id,Set<Date>> mapDate = new Map<Id,Set<Date>>();
            Set<date> deletableDate = new Set<Date>();
            for(Id idNight : tenancyDateMap.Keyset()){
                List<Date> dateList = tenancyDateMap.get(idNight);
                Set<Date> dateSet = new Set<Date>();
                Date endDateTenancy = dateList[1] == null?Date.Today().addDays(1):dateList[1];
                for(date d = dateList[0];d < endDateTenancy;d=d.addDays(1)){
                    dateSet.add(d);
                }
                mapDate.put(idNight,dateSet);
            }
            
            //List of fields assigned for Tenancy__c Object read access
            /*List<String> fieldList = new List<String>{
                'Id',
                'RecordType.Name',
                'In_Form__Void_Status__c',
                'In_Form__Void_Reason__c'               
            };*/ 
            
                //Check read access for Tenancy Object
                //InForm_CRUDFLS_Util.getReadAccessCheck('In_Form__Tenancy__c',fieldList) &&
            if( !idSet.isEmpty())  {       
                Map<Id,Tenancy__c> tenList =  new Map<Id,Tenancy__c>([SELECT Id, RecordType.Name, Void_Status__c, Void_Reason__c FROM Tenancy__c WHERE Id IN: idSet]);
                if(!tenList.isEmpty()){    
                   for(Night__c nigh: [SELECT Id,Tenancy__c,Date__c FROM Night__c WHERE Tenancy__c IN : tenList.keySet()]){
                        if(mapDate.containsKey(nigh.Tenancy__c)){
                            Set<Date> dateSet = new Set<Date>();
                            dateSet = mapDate.get(nigh.Tenancy__c);
                            if(dateSet.contains(nigh.Date__c)){
                                dateSet.remove(nigh.Date__c);
                                mapDate.put(nigh.Tenancy__c,dateSet);
                            }
                        }
                    }
                }
                            
                /*List<String> fieldList_Night = new List<String>{
                'In_Form__Tenancy__c',
                'In_Form__Date__c',
                'In_Form__Void_Status__c',
                'In_Form__Void_Reason__c'               
                 };*/ 
                 //&& InForm_CRUDFLS_Util.getCreateAccessCheck('In_Form__Night__c',fieldList_Night)                  
                if(!mapDate.isEmpty() ){ 
                    for(Id mId : mapDate.Keyset()){
                        for(Date dat : mapDate.get(mId)){
                            Night__c night = new Night__c();
                            night.Tenancy__c = mId;
                            night.Date__c = dat;
                            
                            if(tenList.get(mId).RecordType.Name == 'Void'){
                                night.Void_Status__c = tenList.get(mId).Void_Status__c;
                                night.Void_Reason__c = tenList.get(mId).Void_Reason__c;
                            }
                            
                            nightList.add(night);
                        }
                    }
                }    
            }    
            return nightList;
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
            return null;
        }
        return null;
    }
    /* Method Name : execute
     * Description : Batch Class execute Method to insert Nights
     * Return Type : void
     * Input Parameter : Database.BatchableContext, List of Nights - List<Night__c>
     */
    global void execute(Database.BatchableContext BC, List<Night__c> scope){
        try{
            if(!scope.isEmpty() && scope != null)    {        
                
                systemBatchHelper.isBatchRunning=true;
                INSERT scope; 
            }
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
                      
    }    
    global void finish(Database.BatchableContext BC){
        try{
            Database.executeBatch(new TransactionDeleteBatch(tenancyDateMap,isBulk)); 
        }catch(Exception e){
            System.debug('Exception:'+e.getMessage());
        }
    }
}