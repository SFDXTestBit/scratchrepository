/* Class Name  : ScheduleNightCreation
 * Description   : Apex class to schedule a night record creation everyday at 3.00 AM
 * Created By    : Raushan Anand
 * Created On    : 03-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              03-Nov-2015               Initial version.
 *  * Bhargav Shah				 10-OCT-2018			   Enforcing FLS
 *****************************************************************************************/
global class ScheduleNightCreation implements Schedulable{
    global void execute(SchedulableContext SC) {
    	try{
	       Org_Defaults__c orgDefaults = Org_Defaults__c.getInstance();
	       
	       if(!orgDefaults.Turn_off_Rent_Module_Automation__c && !orgDefaults.Turn_off_Scheduled_Night_Creation__c){
	           Database.executeBatch(new NightCreationBatch());
	       }
        }catch(Exception e){
			System.debug('Exception:'+e.getMessage());
		}
   }
}