@istest
Public class BatchToDeleteDLRecordTest{ 
    Static Testmethod void recordTest(){
        Test.startTest();
        List<String> orderList = new List<String>();
        for(In_Form__DataBatchDeleteRecords__mdt obj : [Select DeveloperName,MasterLabel,In_Form__Is_Deleted__c  FROM In_Form__DataBatchDeleteRecords__mdt 
                                                      Where In_Form__Is_Deleted__c = true]){                                                     
                                                          orderList.add(obj.MasterLabel);                                                                
                                                      }   
        String objName = orderList.get(0);
        
        BatchToDeleteDLRecord batchDL = new BatchToDeleteDLRecord(orderList);
        batchDL.start(null);
        batchDL.execute(null, Database.query('SELECT Id'+' FROM '+objName));
        
        batchDL.finish(null); 
        Test.stopTest();
        
    }
}