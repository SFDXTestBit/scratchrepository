/***************************************************************************************************
Class   : createActIn_UnInstallScript
Author  : AKohakade
Details : Its controller contains install and uninstall scripts for package create action for multiple clients
Coverage Classes: createActIn_UnInstallScript(97%) 
History : v1.0 - 28/03/2018 - Created
          v1.1 - 25/04/2018 - Initialize Enable lightning page value to false  
          v1.2 - 31/10/2018 - Sneha U. - Removed debug statements
****************************************************************************************************/
global without sharing class createActIn_UnInstallScript implements InstallHandler,UninstallHandler{
    public createActIn_UnInstallScript(){
        
    }
    
    global void onInstall(InstallContext context){
        try{    
            //Mayur Patil[26/10/2018]:v1.1 - Extentia - Enforcing CRUD/FLS
            String objectName='In_Form__Create_Actions_custom_information__c';
            /*List<String> fieldList=new List<String>{'In_Form__Added_client_table_header__c','In_Form__Client_Table_Columns__c','In_Form__Client_table_header__c','In_Form__Enable_lightning_page__c',
                'In_Form__Error_message_for_max_added_clients__c','In_Form__Max_Limit_for_Added_Client_table__c','In_Form__Max_records_to_show_in_tables__c','In_Form__Page_header__c',
                'In_Form__Successfully_created_action_table_header__c','In_Form__Timeline_field_matching_values__c','In_Form__Timeline_object_field_API_name__c','In_Form__Unsuccessful_action_table_header__c'};*/
                    
                    //if(InForm_CRUDFLS_Util.getCreateAccessCheck(objectName, fieldList) && InForm_CRUDFLS_Util.getUpdateAccessCheck(objectName, fieldList)){
                        Create_Actions_custom_information__c CustRec = Create_Actions_custom_information__c.getOrgDefaults();
                        if(String.isBlank(CustRec.Added_client_table_header__c)){
                            CustRec.Added_client_table_header__c = 'Clients Added';
                        }
                        
                        if(String.isBlank(CustRec.Client_Table_Columns__c)){
                            CustRec.Client_Table_Columns__c = 'In_Form__Client_gender__c,In_Form__Status__c,In_Form__Referral_date__c';
                        }
                        
                        if(String.isBlank(CustRec.Client_table_header__c)){
                            CustRec.Client_table_header__c = 'Clients';
                        }
                        
                        CustRec.Enable_lightning_page__c = false;
                        
                        if(String.isBlank(CustRec.Error_message_for_max_added_clients__c)){
                            CustRec.Error_message_for_max_added_clients__c = 'Cannot add any more Clients as the maximum has been reached';
                        }
                        
                        if(CustRec.Max_Limit_for_Added_Client_table__c == null){
                            CustRec.Max_Limit_for_Added_Client_table__c = 100;
                        }
                        
                        if(CustRec.Max_records_to_show_in_tables__c == null){
                            CustRec.Max_records_to_show_in_tables__c = 10;
                        }
                        
                        if(String.isBlank(CustRec.Page_header__c)){
                            CustRec.Page_header__c = 'Create Actions for Multiple Clients';
                        }
                        
                        if(String.isBlank(CustRec.Successfully_created_action_table_header__c)){
                            CustRec.Successfully_created_action_table_header__c = 'Successfully Created Actions';
                        }
                        
                        if(String.isBlank(CustRec.Timeline_field_matching_values__c)){
                            CustRec.Timeline_field_matching_values__c = 'Open,Referral,Referral accepted';
                        }
                        
                        if(String.isBlank(CustRec.Timeline_object_field_API_name__c)){
                            CustRec.Timeline_object_field_API_name__c = 'In_Form__Status__c';
                        }
                        
                        if(String.isBlank(CustRec.Unsuccessful_action_table_header__c)){
                            CustRec.Unsuccessful_action_table_header__c = 'Unable To Create These Actions';
                        }
                        UPSERT CustRec;
                    //}
        }catch(Exception e){  
            try{                
                if (Schema.sObjectType.Account.isCreateable() && Schema.sObjectType.Account.fields.Name.isCreateable() && Schema.sObjectType.Account.fields.Description.isCreateable()){
                    INSERT new Account(Name='InForm-PostInstall', Description='Exception occurred==>'+e.getMessage()+' : stackTrace==>'+e.getStackTraceString());
                }
            }catch(Exception ee){
                System.debug('Exception:'+ee.getMessage());
            }           
        }

}
    
    
    global void onUninstall(UninstallContext context){
        
    }   
    
}