/*------------------------------------------------------------------
 * Created By : Saurabh Gupta
 * Created date : 12/01/2016 13:43
 * Last Modified by : Saurabh Gupta ,  13/02/2016 18:48
 * Description: Close the tenancy and delete invalid transaction on end date population
 * History: v1.1 - Sneha U. - 31/10/2018 - Added Update Access Check
------------------------------------------------------------------*/
public with sharing class CloseTenancy{ 
    
    public Tenancy__c tenancy{get;set;}
    public Boolean isSuccess{get;set;}
    public CloseTenancy(ApexPages.StandardController sc){
        if(!Test.isRunningTest()){
            sc.addFields(new List<String>{'End_Date__c'});
        }
        tenancy = (Tenancy__c)sc.getRecord();
    }

    //Method to update Tenancy
    public void save(){
        try{
            //if(InForm_CRUDFLS_Util.getUpdateAccessCheck(String.valueOf(Schema.SObjectType.Tenancy__c.getSobjectType()), new List<String>() ) ){
                update tenancy;
                isSuccess = true;
            //}
           // else{
              //  isSuccess = false;
              //  ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'There was a problem updating the record') );
          //  }
        }
        catch(DmlException ex){
            isSuccess = false;
            ApexPages.addMessages(ex);
        }

    }
}