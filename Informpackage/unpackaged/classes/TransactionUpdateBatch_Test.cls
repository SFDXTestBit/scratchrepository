/* Class Name  : ScheduleNightCreation_Test
 * Description   : Apex Test class to create a night record everyday at 3.00 AM
 * Created By    : Raushan Anand
 * Created On    : 03-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              03-Nov-2015               Initial version.
 *  * Mayur Patil                30-Oct-2018               Security Review Changes.
 *****************************************************************************************/
@isTest
public class TransactionUpdateBatch_Test {
    @TestSetup
    public static void setupData(){
        Test.startTest();
        try{
            Transaction_Type__c dailytype = new Transaction_Type__c(Day_of_week__c = Datetime.now().format('EEEE'),Daily_Amount__c = false,Is_Active__c=true,Type__c = 'Weekly',Name = 'DailyType');
            INSERT dailyType;
            List<Tenancy__c> tenList = new List<Tenancy__c>();
            List<RecordType> rtList = [SELECT Name,Id FROM RecordType where sObjectType='In_Form__Tenancy__c'];
            Map<String,Id> rtMap = new Map<String,Id>();
            if(rtList!= null && rtList.size()>0){
                for(RecordType rt: rtList)
                rtMap.put(rt.Name,rt.Id);
            }
            Bedspace__c oBedspace = new Bedspace__c(Name='testBedSpace', In_Form__Basic_Rent__c=100);
            INSERT oBedspace;
            
            
            Contact con = new Contact(Lastname='TestName',FirstName='TestFname');
            INSERT con;
            
            Rent_Schedule__c oRentSchedule = new Rent_Schedule__c(Bedspace__c =oBedspace.Id,Total_Rent_Charge__c=100,Start_date__c=Date.Today().addDays(-10),End_date__c=Date.Today().addDays(4),In_Form__Is_default__c=true);
            INSERT oRentSchedule;
            
            Rent_Stream_Charge__c oRentCharge = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=50);
            INSERT oRentCharge;
            
            Rent_Stream_Charge__c oRentCharge1 = new Rent_Stream_Charge__c(Rent_Schedule__c=oRentSchedule.Id,Type__c='Housing benefit',Amount__c=100);
            INSERT oRentCharge1;        
            
            Tenancy__c ten1 = new Tenancy__c();
            ten1.recordTypeid = rtMap.get('Tenancy');
            ten1.Client__c = con.Id;
            ten1.Start_date__c=Date.Today().addDays(-34);
            ten1.Status__c='Current';
            ten1.Bedspace__c=oBedspace.Id;
            tenList.add(ten1);
            
            Tenancy__c ten2 = new Tenancy__c();
            ten2.recordTypeid = rtMap.get('Tenancy');
            ten2.Client__c = con.Id;
            ten2.Start_date__c=Date.Today().addDays(-27);
            ten2.Status__c='Current';
            ten2.Bedspace__c=oBedspace.Id;
            tenList.add(ten2);
            INSERT tenList;


            System.assertEquals(2, tenList.size(),'SUCCESS');
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    } 
    
    public static testmethod void TestScheduleNightCreation(){
        Test.startTest();
        try{
            String CRON_EXP = '0 0 0 1 1 ? 2025';  
            String jobId = System.schedule('ScheduledApex', CRON_EXP, new scheduleNightCreation() );
            CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression); 
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    }
    public static testmethod void TestNightDelete(){
        Test.startTest();
        try{
            List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34) ];
            Tenancy__c ten = tenList[0];
            ten.Start_date__c = Date.Today().addDays(-2);
            ten.End_date__c = Date.Today();
            Night__c nigh = new Night__c(Date__c = Date.today().addDays(-4),Tenancy__c = ten.Id);
            INSERT nigh;
            try{
                Update ten;
            }catch(Exception e){
            
            }
            System.assertEquals(Date.Today(), ten.End_Date__c);
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
        Test.stopTest();
    }
     public static testmethod void TestNightUpdate(){
        try{
             Map<Id,List<Date>> mapDate = new map<Id, List<Date>>();
             List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34) ];
             Tenancy__c ten = tenList[0];
             Night__c nigh = new Night__c(Date__c = Date.today(),Tenancy__c = ten.Id);
             INSERT nigh;
             date startDate = date.Today().adddays(-2);
             date endDate = date.Today().adddays(2);
             List<Date> dtList = new List<Date>();
             dtList.add(startDate);
             dtList.add(endDate);
             dtList.add(date.Today().adddays(3));
             dtList.add(date.Today().adddays(4));
             mapDate.put(ten.Id, dtList);
             Test.startTest();
             Database.executeBatch(new NightUpdateBatch(mapDate,systemBatchHelper.isBulk));
             Test.stopTest();
             List<Night__c> nighList = [SELECT Id FROM Night__c where Tenancy__c =: ten.Id];
             System.assertEquals(4,nighList.size());
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }

    public static testmethod void TestNightUpdateDaily(){
        try{
            delete [SELECT Id FROM Transaction_Type__c];
            Transaction_Type__c dailytype = new Transaction_Type__c(Day_of_week__c = Datetime.now().format('EEEE'),Daily_Amount__c = false,Is_Active__c=true,Type__c = 'Daily',Name = 'DailyType');
            INSERT dailyType;
            Map<Id,List<Date>> mapDate = new map<Id, List<Date>>();
            List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34) ];
            Tenancy__c ten = tenList[0];
            Night__c nigh = new Night__c(Date__c = Date.today(),Tenancy__c = ten.Id);
            INSERT nigh;
            date startDate = date.Today().adddays(-2);
            date endDate = date.Today().adddays(2);
            List<Date> dtList = new List<Date>();
            dtList.add(startDate);
            dtList.add(endDate);
            dtList.add(date.Today().adddays(3));
            dtList.add(date.Today().adddays(4));
            mapDate.put(ten.Id, dtList);
            Test.startTest();
            Database.executeBatch(new NightUpdateBatch(mapDate));
            Test.stopTest();
            List<Night__c> nighList = [SELECT Id FROM Night__c where Tenancy__c =: ten.Id];
            System.assertEquals(4,nighList.size());
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
    public static testmethod void TestNightUpdateWeeklyProRate(){
        try{
            delete [SELECT Id FROM Transaction_Type__c];
            Transaction_Type__c weeklyProRatetype = new Transaction_Type__c(Day_of_week__c = Datetime.now().format('EEEE'),Daily_Amount__c = false,Is_Active__c=true,Type__c = 'Weekly - pro rata',Name = 'WeeklyType');
            INSERT weeklyProRatetype;
            Map<Id,List<Date>> mapDate = new map<Id, List<Date>>();
            List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34) ];
            Tenancy__c ten = tenList[0];
            Night__c nigh = new Night__c(Date__c = Date.today(),Tenancy__c = ten.Id);
            INSERT nigh;
            date startDate = date.Today().adddays(-2);
            date endDate = date.Today().adddays(2);
            List<Date> dtList = new List<Date>();
            dtList.add(startDate);
            dtList.add(endDate);
            dtList.add(date.Today().adddays(3));
            dtList.add(date.Today().adddays(4));
            mapDate.put(ten.Id, dtList);
            Test.startTest();
            Database.executeBatch(new NightUpdateBatch(mapDate));
            Test.stopTest();
            List<Night__c> nighList = [SELECT Id FROM Night__c where Tenancy__c =: ten.Id];
            System.assertEquals(4,nighList.size());
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
    public static testmethod void TestNightUpdateWeeklyProRateEndDatUpdated(){
        try{
            Map<Id,List<Date>> mapDate = new map<Id, List<Date>>();
            List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34) ];
            Tenancy__c ten = tenList[0];
            ten.Id=null;
            ten.End_Date__c=System.today().addDays(90);
            insert ten;
            Night__c nigh = new Night__c(Date__c = Date.today(),Tenancy__c = ten.Id);
            INSERT nigh;
            date startDate = date.Today().adddays(-2);
            date endDate = date.Today().adddays(2);
            List<Date> dtList = new List<Date>();
            dtList.add(startDate);
            dtList.add(endDate);
            dtList.add(date.Today().adddays(3));
            dtList.add(date.Today().adddays(4));
            mapDate.put(ten.Id, dtList);
            Test.startTest();
            Database.executeBatch(new NightUpdateBatch(mapDate,systemBatchHelper.isBulk));  // added by extentia.
            Test.stopTest();
            List<Night__c> nighList = [SELECT Id FROM Night__c where Tenancy__c =: ten.Id];
            System.assertEquals(4,nighList.size());
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
    public static testmethod void TestTransactcionUpdateDelete(){
        try{
            Map<Id,List<Date>> mapDate = new map<Id, List<Date>>();
            Test.startTest();
            List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-27) ];
            Tenancy__c ten = tenList[0];
            date startDate = date.Today().adddays(-2);
            date endDate = date.Today().adddays(2);
            List<Date> dtList = new List<Date>();
            dtList.add(startDate);
            dtList.add(endDate);
            dtList.add(date.Today().adddays(3));
            dtList.add(date.Today().adddays(4));
            mapDate.put(ten.Id, dtList);
            
            Database.executeBatch(new TransactionDeleteBatch(mapDate,systemBatchHelper.isBulk)); // added by Extentia.
            Test.stopTest();
            List<Transaction__c> tranList = [SELECT Id, Transaction_date__c, Tenancy__c FROM Transaction__c where Tenancy__c =: ten.Id];
            System.debug(tranList);
            System.assertEquals(2,tranList.size());
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
    public static testmethod void TestNightCreationonTenancy(){
        try{
            List<Tenancy__c> tenList = [SELECT Id, Start_Date__c FROM Tenancy__c WHERE Start_date__c >=:Date.Today().addDays(-34) ];
            List<Id> ids = new List<Id>();
            for(Tenancy__c tenId : tenList){
                ids.add(tenId.Id);
            }
            Test.startTest();
                Database.executeBatch(new NightCreationOnTenancyInsertBatch(ids),365);
            Test.stopTest();
            List<Night__c> nighList = [SELECT Id FROM Night__c where Tenancy__c =: tenList[0].Id];
            System.assertEquals(35,nighList.size());
        }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
        }
    }
}